﻿using ENTIDADES.Login;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO.Seguridad
{
    public class EmpleadoConDAO: IEmpleadoConDAO
    {

        public UsuarioViewModel EmpleadoLoginCon(Empleado usuario)
        {
            UsuarioViewModel usuarioLogin = null;
            try
            {
                using (DBDINEntities  conn = new DBDINEntities())
                {
                    conn.Database.Connection.Open();
                    usuarioLogin = (from emp in conn.Empleado
                                    where emp.Em_Num_IMSS == usuario.Em_Num_IMSS
                                          && emp.Em_Password == usuario.Em_Password
                                    join de in conn.Departamento_Empleado
                                    on emp.De_Cve_Departamento_Empleado equals de.De_Cve_Departamento_Empleado
                                    join suc in conn.Sucursal
                                    on emp.Sc_Cve_Sucursal equals suc.Sc_Cve_Sucursal
                                    join pe in conn.Puesto_Empleado
                                    on emp.Pe_Cve_Puesto_Empleado equals pe.Pe_Cve_Puesto_Empleado
                                    select new UsuarioViewModel()
                                    {
                                        CveEmpleado = emp.Em_Cve_Empleado,
                                        CodigoAlterno = emp.Em_Codigo_Alterno,
                                        NombreEmp = emp.Em_Nombre +" "+emp.Em_Apellido_Paterno ,
                                        CveDepartamentoEmp= emp.De_Cve_Departamento_Empleado,
                                        NomDepartamdentoEmp = de.De_Descripcion,
                                        CvePuestoEmpleado = emp.Pe_Cve_Puesto_Empleado,
                                        NomPuestoEmp = pe.Pe_Descripcion,
                                        CveSucursal = emp.Sc_Cve_Sucursal,
                                        NomSucursal= suc.Sc_Descripcion,
                                        EmReporta = emp.Em_Reporta ,
                                        NumIMSS= emp.Em_Num_IMSS

                                    }
                                    ).FirstOrDefault();


                }
                if (usuarioLogin == null)
                {
                    usuarioLogin = new UsuarioViewModel();
                    usuarioLogin.codigoRespuesta = "D-000002";
                    usuarioLogin.mensajeRespuesta = "Usuario / Contraseña incorrectos, favor de volver a intentar";

                }
                else
                {
                    usuarioLogin.codigoRespuesta = "000000";
                    usuarioLogin.mensajeRespuesta = "Operacion realizada con exito";
                }


            }
            catch (Exception ex)
            {

                if (usuarioLogin == null)
                {
                    usuarioLogin = new UsuarioViewModel();
                    usuarioLogin.codigoRespuesta = "D-000001";
                    usuarioLogin.mensajeRespuesta = "Ocurrió un error al recuperar el usuario de la Base de Datos " + ex.Message;
                }
            }
            return usuarioLogin;

        }




    }
}
