﻿using ENTIDADES.Login;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO.Seguridad
{
    public interface IEmpleadoConDAO
    {
        UsuarioViewModel EmpleadoLoginCon(Empleado usuario);
    }
}
