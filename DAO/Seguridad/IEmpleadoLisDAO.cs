﻿using ENTIDADES.Permisos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO.Seguridad
{
    public interface IEmpleadoLisDAO
    {
        List<Modulo> PermisoModuloEmpleado(PUESTOOPERACION puestoOperacion);
    }
}
