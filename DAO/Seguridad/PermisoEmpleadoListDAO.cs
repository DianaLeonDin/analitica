﻿using DAO.Base;
using ENTIDADES.Base;
using ENTIDADES.Login;
using ENTIDADES.Seguridad;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO.Seguridad
{
    public class PermisoEmpleadoListDAO
    {

        public PermisoEmpleadoViewModel ConsultaPermisoEmpleadoDAO(int valor, string Cvepuesto)
        {
            PermisoEmpleadoViewModel PermisoEmpleado = null;

            SqlDataReader dr = null;
            SqlTransaction tran = null;
            try
            {
                using (var conn = new SqlConnection(BaseDAO.analiticabd))
                {
                    conn.Open();
                    tran = conn.BeginTransaction();
                    SqlCommand cmd = new SqlCommand("PERMISOEMPLEADOLIST", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@Par_PuestoID", Cvepuesto);
                    cmd.Parameters.AddWithValue("@Par_PermisoID", valor);
                    cmd.Parameters.AddWithValue("@Par_NumLis", 1);

                    cmd.Parameters.AddWithValue("@Par_Anio", "");
                    cmd.Parameters.AddWithValue("@Par_Mes", "");
                    cmd.Parameters.AddWithValue("@Par_Sucursal", "");

                    cmd.Parameters.AddWithValue("@Par_Salida", "S");
                    cmd.Parameters.AddWithValue("@Par_NumErr", "");
                    cmd.Parameters.AddWithValue("@Par_ErrMenDev", "");

                    cmd.Transaction = tran;
                    try
                    {
                        dr = cmd.ExecuteReader();

                        PermisoEmpleado = new PermisoEmpleadoViewModel();
                        if (dr.Read())
                        {
                            PermisoEmpleado.RolID = int.Parse(dr["RolID"].ToString());
                            PermisoEmpleado.PermisoID = int.Parse(dr["PermisoID"].ToString());
                            PermisoEmpleado.Nombre = dr["Nombre"].ToString();
                            PermisoEmpleado.Modulo = dr["Modulo"].ToString();
                            PermisoEmpleado.Descripcion = dr["Descripcion"].ToString();
                            // PermisoEmpleado.Pe_Cve_Puesto_Empleado = dr["Pe_Cve_Puesto_Empleado"].ToString();

                        }
                        dr.Close();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                }
            }
            catch (Exception)
            {
                throw;
            }
            return PermisoEmpleado;
        }
        
        //LISTADO DE LOS MODULOS PARA CREAR ACCESO Y PERMISO 
        //LISTADO PARA LA VISTA DE ACCESO Y PARA AGREGAR UN NUEVO REGISTRO
        public List<PermisoEmpleadoViewModel> ListaAccesoDAO(int valor)
        {
            List<PermisoEmpleadoViewModel> lista = new List<PermisoEmpleadoViewModel>();
            PermisoEmpleadoViewModel listaDao;

            SqlDataReader drd = null;

            using (var conn = new SqlConnection(BaseDAO.analiticabd))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("PERMISOEMPLEADOLIST", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Par_PuestoID", "");
                    cmd.Parameters.AddWithValue("@Par_PermisoID", "");
                    cmd.Parameters.AddWithValue("@Par_NumLis", valor);

                    cmd.Parameters.AddWithValue("@Par_Anio", "");
                    cmd.Parameters.AddWithValue("@Par_Mes", "");
                    cmd.Parameters.AddWithValue("@Par_Sucursal", "");

                    cmd.Parameters.AddWithValue("@Par_Salida", "S");
                    cmd.Parameters.AddWithValue("@Par_NumErr", "");
                    cmd.Parameters.AddWithValue("@Par_ErrMenDev", "");

                    drd = cmd.ExecuteReader();

                    while (drd.Read())
                    {
                        listaDao = new PermisoEmpleadoViewModel()
                        {
                            PermisoID = int.Parse(drd["Folio"].ToString()),
                            Modulo = drd["Nombre"].ToString(),
                    };
                        lista.Add(listaDao);
                    }
                    drd.Close();
                }

                catch (Exception ex)
                {
                    throw ex;
                }
                return lista;
            }
        }

        //LISTADO DE CONTROL DE ACCESO DE INICIO SESION(lISTA DE USUARIOS QUE INICIARON SESION)
        //LISTA CONTROL DE QUIEN VISUALIZA LOS INDICADORES DE OPERACIONES DIARIOS
        public List<PermisoEmpleadoViewModel> ListaControlAccesoDAO(PermisoEmpleadoViewModel permiso, int valor)
        {
            List<PermisoEmpleadoViewModel> listaLogin = new List<PermisoEmpleadoViewModel>();
            PermisoEmpleadoViewModel listaDao;

            SqlDataReader drd = null;
            //int valorMes = Convert.ToInt32(permiso.mes);

            using (var conn = new SqlConnection(BaseDAO.analiticabd))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("PERMISOEMPLEADOLIST", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Par_PuestoID", "");
                    cmd.Parameters.AddWithValue("@Par_PermisoID", "");
                    cmd.Parameters.AddWithValue("@Par_NumLis", valor);
                    if (valor==5)
                    {
                        cmd.Parameters.AddWithValue("@Par_Anio", permiso.anio);
                        cmd.Parameters.AddWithValue("@Par_Mes", Convert.ToInt32(permiso.mes));
                        cmd.Parameters.AddWithValue("@Par_Sucursal", permiso.Sucursal);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@Par_Anio", "");
                        cmd.Parameters.AddWithValue("@Par_Mes", "");
                        cmd.Parameters.AddWithValue("@Par_Sucursal", "");
                    }


                    cmd.Parameters.AddWithValue("@Par_Salida", "S");
                    cmd.Parameters.AddWithValue("@Par_NumErr", "");
                    cmd.Parameters.AddWithValue("@Par_ErrMenDev", "");

                    drd = cmd.ExecuteReader();

                    while (drd.Read())
                    {
                        listaDao = new PermisoEmpleadoViewModel()
                        {
                            //Usuario = drd["Usuario"].ToString(),
                            visitas = int.Parse(drd["Visitas"].ToString()),
                            //FechaVar = drd["Fecha"].ToString(),
                            Sucursal = drd["SucursalID"].ToString(),
                            comentarios = int.Parse(drd["Comentarios"].ToString()),
                        };
                        listaLogin.Add(listaDao);
                    }
                    drd.Close();
                }

                catch (Exception ex)
                {
                    throw ex;
                }
                return listaLogin;
            }
        }

        //Alta permisos dependido del puesto
        public MensajeTransaccion AltaPermiso(PermisoEmpleadoViewModel permiso)
        {
            MensajeTransaccion mensaje = null;
            SqlDataReader dr = null;
            SqlTransaction tran = null;

            try
            {
                using (var conn = new SqlConnection(BaseDAO.analiticabd))
                {
                    conn.Open();

                    tran = conn.BeginTransaction();
                    SqlCommand cmd = new SqlCommand("PERMISOALT", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Par_RolID", permiso.RolID);
                    cmd.Parameters.AddWithValue("@Par_PermisoID", permiso.Modulo);

                    cmd.Parameters.AddWithValue("@Par_Salida", "S");
                    cmd.Parameters.AddWithValue("@Par_NumErr", "");
                    cmd.Parameters.AddWithValue("@Par_ErrMenDev", "");
                    // cmd.Parameters.AddWithValue("@Par_Consecutivo", "");
                    cmd.Transaction = tran;

                    try
                    {
                        dr = cmd.ExecuteReader();
                        while (dr.Read())
                        {
                            mensaje = new MensajeTransaccion();
                            mensaje.CodigoRespuesta = dr["NumErr"].ToString();
                            mensaje.MensajeRespuesta = dr["ErrMenDev"].ToString();
                            //   mensaje.Consecutivo = dr["Consecutivo"].ToString();
                        }
                        dr.Close();
                        if (!mensaje.CodigoRespuesta.Equals("000000"))
                        {
                            throw new Exception();
                        }
                        tran.Commit();
                    }
                    catch (Exception ex)
                    {
                        tran.Rollback();
                        if (mensaje == null)
                        {
                            mensaje = new MensajeTransaccion();
                            mensaje.CodigoRespuesta = "CD-000001";
                            mensaje.MensajeRespuesta = "-CAPA DAO- \n OCURRIÓ UN ERROR AL CREAR EL PERMISO, EN EL PROCESO " +
                                " PERMISOALT \n Error técnico: " + ex.Message;
                        }
                    }

                }
            }
            catch (Exception)
            {
                throw;
            }
            return mensaje;
        }
        
        //LISTADO PARA LA VISTA DE ACCESO 
        public List<PermisoEmpleadoViewModel> ListaTreeRolDAO(int valor)
        {
            List<PermisoEmpleadoViewModel> lista = new List<PermisoEmpleadoViewModel>();
            PermisoEmpleadoViewModel listaDao;

            SqlDataReader drd = null;

            using (var conn = new SqlConnection(BaseDAO.analiticabd))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("PERMISOEMPLEADOLIST", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Par_PuestoID", "");
                    cmd.Parameters.AddWithValue("@Par_PermisoID", "");
                    cmd.Parameters.AddWithValue("@Par_NumLis", valor);

                    cmd.Parameters.AddWithValue("@Par_Anio", "");
                    cmd.Parameters.AddWithValue("@Par_Mes", "");
                    cmd.Parameters.AddWithValue("@Par_Sucursal", "");

                    cmd.Parameters.AddWithValue("@Par_Salida", "S");
                    cmd.Parameters.AddWithValue("@Par_NumErr", "");
                    cmd.Parameters.AddWithValue("@Par_ErrMenDev", "");


                    drd = cmd.ExecuteReader();

                    while (drd.Read())
                    {
                        listaDao = new PermisoEmpleadoViewModel()
                        {
                            PermisoID = int.Parse(drd["Folio"].ToString()),
                            Modulo = drd["Nombre"].ToString(),
                        };
                        lista.Add(listaDao);
                    }
                    drd.Close();
                }

                catch (Exception ex)
                {
                    throw ex;
                }
                return lista;
            }
        }
        
        public List<PermisoEmpleadoViewModel> ListaTreeModuloDAO(int valor)
        {
            List<PermisoEmpleadoViewModel> lista = new List<PermisoEmpleadoViewModel>();
            PermisoEmpleadoViewModel listaDao;

            SqlDataReader drd = null;

            using (var conn = new SqlConnection(BaseDAO.analiticabd))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("PERMISOEMPLEADOLIST", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Par_PuestoID", "");
                    cmd.Parameters.AddWithValue("@Par_PermisoID", "");
                    cmd.Parameters.AddWithValue("@Par_NumLis", valor);

                    cmd.Parameters.AddWithValue("@Par_Anio", "");
                    cmd.Parameters.AddWithValue("@Par_Mes", "");
                    cmd.Parameters.AddWithValue("@Par_Sucursal", "");

                    cmd.Parameters.AddWithValue("@Par_Salida", "S");
                    cmd.Parameters.AddWithValue("@Par_NumErr", "");
                    cmd.Parameters.AddWithValue("@Par_ErrMenDev", "");

                    drd = cmd.ExecuteReader();

                    while (drd.Read())
                    {
                        listaDao = new PermisoEmpleadoViewModel()
                        {
                            PermisoID = int.Parse(drd["Folio"].ToString()),
                            Modulo = drd["Nombre"].ToString(),
                            RolID = int.Parse(drd["PadreFolio"].ToString()),
                            Nombre = drd["PadreNombre"].ToString(),
                        };
                        lista.Add(listaDao);
                    }
                    drd.Close();
                }

                catch (Exception ex)
                {
                    throw ex;
                }
                return lista;
            }
        }
        
        //AÑO DE ACCESO
        public List<PermisoEmpleadoViewModel> ListaAccesoAnioDAO(int valor)
        {
            List<PermisoEmpleadoViewModel> lista = new List<PermisoEmpleadoViewModel>();
            PermisoEmpleadoViewModel listaDao;

            SqlDataReader drd = null;

            using (var conn = new SqlConnection(BaseDAO.analiticabd))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("PERMISOEMPLEADOLIST", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Par_PuestoID", "");
                    cmd.Parameters.AddWithValue("@Par_PermisoID", "");
                    cmd.Parameters.AddWithValue("@Par_NumLis", valor);

                    cmd.Parameters.AddWithValue("@Par_Anio", "");
                    cmd.Parameters.AddWithValue("@Par_Mes", "");
                    cmd.Parameters.AddWithValue("@Par_Sucursal", "");

                    cmd.Parameters.AddWithValue("@Par_Salida", "S");
                    cmd.Parameters.AddWithValue("@Par_NumErr", "");
                    cmd.Parameters.AddWithValue("@Par_ErrMenDev", "");

                    drd = cmd.ExecuteReader();

                    while (drd.Read())
                    {
                        listaDao = new PermisoEmpleadoViewModel()
                        {
                            anio = int.Parse(drd["Anio"].ToString()),
                        };
                        lista.Add(listaDao);
                    }
                    drd.Close();
                }

                catch (Exception ex)
                {
                    throw ex;
                }
                return lista;
            }
        }

        //MES DE ACCESO 
        public List<PermisoEmpleadoViewModel> ListaAccesoMesDAO(int valor)
        {
            List<PermisoEmpleadoViewModel> lista = new List<PermisoEmpleadoViewModel>();
            PermisoEmpleadoViewModel listaDao;

            SqlDataReader drd = null;

            using (var conn = new SqlConnection(BaseDAO.analiticabd))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("PERMISOEMPLEADOLIST", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Par_PuestoID", "");
                    cmd.Parameters.AddWithValue("@Par_PermisoID", "");
                    cmd.Parameters.AddWithValue("@Par_NumLis", valor);

                    cmd.Parameters.AddWithValue("@Par_Anio", "");
                    cmd.Parameters.AddWithValue("@Par_Mes", "");
                    cmd.Parameters.AddWithValue("@Par_Sucursal", "");

                    cmd.Parameters.AddWithValue("@Par_Salida", "S");
                    cmd.Parameters.AddWithValue("@Par_NumErr", "");
                    cmd.Parameters.AddWithValue("@Par_ErrMenDev", "");

                    drd = cmd.ExecuteReader();

                    while (drd.Read())
                    {
                        listaDao = new PermisoEmpleadoViewModel()
                        {
                            Total = int.Parse(drd["Fecha"].ToString()),
                            mes = drd["Mes"].ToString(),
                        };
                        lista.Add(listaDao);
                    }
                    drd.Close();
                }

                catch (Exception ex)
                {
                    throw ex;
                }
                return lista;
            }
        }

        //SUURSALES CON ACCESO 
        public List<PermisoEmpleadoViewModel> ListaAccesoSucursalDAO(int valor)
        {
            List<PermisoEmpleadoViewModel> lista = new List<PermisoEmpleadoViewModel>();
            PermisoEmpleadoViewModel listaDao;

            SqlDataReader drd = null;

            using (var conn = new SqlConnection(BaseDAO.analiticabd))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("PERMISOEMPLEADOLIST", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Par_PuestoID", "");
                    cmd.Parameters.AddWithValue("@Par_PermisoID", "");
                    cmd.Parameters.AddWithValue("@Par_NumLis", valor);

                    cmd.Parameters.AddWithValue("@Par_Anio", "");
                    cmd.Parameters.AddWithValue("@Par_Mes", "");
                    cmd.Parameters.AddWithValue("@Par_Sucursal", "");

                    cmd.Parameters.AddWithValue("@Par_Salida", "S");
                    cmd.Parameters.AddWithValue("@Par_NumErr", "");
                    cmd.Parameters.AddWithValue("@Par_ErrMenDev", "");

                    drd = cmd.ExecuteReader();

                    while (drd.Read())
                    {
                        listaDao = new PermisoEmpleadoViewModel()
                        {
                            Sucursal = drd["Sucursal"].ToString(),
                        };
                        lista.Add(listaDao);
                    }
                    drd.Close();
                }

                catch (Exception ex)
                {
                    throw ex;
                }
                return lista;
            }
        }

        //PUESTO SERVIDOR 4
        public List<PermisoEmpleadoViewModel> ListaPuestoDAO(int valor)
        {
            List<PermisoEmpleadoViewModel> lista = new List<PermisoEmpleadoViewModel>();
            PermisoEmpleadoViewModel listaDao;

            SqlDataReader drd = null;

            using (var conn = new SqlConnection(BaseDAO.dbdinbd))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("PERMISOEMPLEADOLIST", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Par_PuestoID", "");
                    cmd.Parameters.AddWithValue("@Par_PermisoID", "");
                    cmd.Parameters.AddWithValue("@Par_NumLis", valor);

                    cmd.Parameters.AddWithValue("@Par_Anio", "");
                    cmd.Parameters.AddWithValue("@Par_Mes", "");
                    cmd.Parameters.AddWithValue("@Par_Sucursal", "");

                    drd = cmd.ExecuteReader();

                    while (drd.Read())
                    {
                        listaDao = new PermisoEmpleadoViewModel()
                        {
                            Pe_Cve_Puesto_Empleado = drd["Folio"].ToString(),
                            Nombre = drd["Nombre"].ToString(),
                        };
                        lista.Add(listaDao);
                    }
                    drd.Close();
                }

                catch (Exception ex)
                {
                    throw ex;
                }
                return lista;
            }
        }
        
        //EMPLEADO SERVIDOR 4
        public List<PermisoEmpleadoViewModel> ListaEmpleadoDAO(int valorEdo, string puesto)
        {
            List<PermisoEmpleadoViewModel> lista = new List<PermisoEmpleadoViewModel>();
            PermisoEmpleadoViewModel listaDao;

            SqlDataReader drd = null;

            using (var conn = new SqlConnection(BaseDAO.dbdinbd))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("PERMISOEMPLEADOLIST", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Par_PuestoID", puesto);
                    cmd.Parameters.AddWithValue("@Par_PermisoID", "");
                    cmd.Parameters.AddWithValue("@Par_NumLis", valorEdo);

                    cmd.Parameters.AddWithValue("@Par_Anio", "");
                    cmd.Parameters.AddWithValue("@Par_Mes", "");
                    cmd.Parameters.AddWithValue("@Par_Sucursal", "");

                    drd = cmd.ExecuteReader();

                    while (drd.Read())
                    {
                        listaDao = new PermisoEmpleadoViewModel()
                        {
                            Usuario = drd["Folio"].ToString(),
                            Nombre = drd["Nombre"].ToString(),
                        };
                        lista.Add(listaDao);
                    }
                    drd.Close();
                }

                catch (Exception ex)
                {
                    throw ex;
                }
                return lista;
            }
        }


        //Alta de permisos , empleado con sus sucursales
        public MensajeTransaccion AltaPermisoSucursal(PermisoEmpleadoViewModel permiso)
        {
            MensajeTransaccion mensaje = null;
            SqlDataReader dr = null;
            SqlTransaction tran = null;

            try
            {
                using (var conn = new SqlConnection(BaseDAO.analiticabd))
                {
                    conn.Open();

                    tran = conn.BeginTransaction();
                    SqlCommand cmd = new SqlCommand("EMPLEADOSUCURSALALT", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Par_Sucursal", permiso.Sucursal);
                    cmd.Parameters.AddWithValue("@Par_SucursalID", permiso.SucursalID);
                    cmd.Parameters.AddWithValue("@Par_UsuarioID", permiso.Usuario);
                    cmd.Parameters.AddWithValue("@Par_Salida", "S");
                    cmd.Parameters.AddWithValue("@Par_NumErr", "");
                    cmd.Parameters.AddWithValue("@Par_ErrMenDev", "");
                    cmd.Transaction = tran;

                    try
                    {
                        dr = cmd.ExecuteReader();
                        while (dr.Read())
                        {
                            mensaje = new MensajeTransaccion();
                            mensaje.CodigoRespuesta = dr["NumErr"].ToString();
                            mensaje.MensajeRespuesta = dr["ErrMenDev"].ToString();
                            //   mensaje.Consecutivo = dr["Consecutivo"].ToString();
                        }
                        dr.Close();
                        if (!mensaje.CodigoRespuesta.Equals("000000"))
                        {
                            throw new Exception();
                        }
                        tran.Commit();
                    }
                    catch (Exception ex)
                    {
                        tran.Rollback();
                        if (mensaje == null)
                        {
                            mensaje = new MensajeTransaccion();
                            mensaje.CodigoRespuesta = "CD-000002";
                            mensaje.MensajeRespuesta = "-CAPA DAO- \n OCURRIÓ UN ERROR AL CREAR EL PERMISO, EN EL PROCESO " +
                                " EMPLEADOSUCURSALALT \n Error técnico: " + ex.Message;
                        }
                    }

                }
            }
            catch (Exception)
            {
                throw;
            }
            return mensaje;
        }

        //Listado de arbol para los permisos por sucursales
        public List<PermisoEmpleadoViewModel> ListaTreeEmpSucDAO(int valor)
        {
            List<PermisoEmpleadoViewModel> lista = new List<PermisoEmpleadoViewModel>();
            PermisoEmpleadoViewModel listaDao;

            SqlDataReader drd = null;

            using (var conn = new SqlConnection(BaseDAO.analiticabd))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("PERMISOEMPLEADOLIST", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Par_PuestoID", "");
                    cmd.Parameters.AddWithValue("@Par_PermisoID", "");
                    cmd.Parameters.AddWithValue("@Par_NumLis", valor);

                    cmd.Parameters.AddWithValue("@Par_Anio", "");
                    cmd.Parameters.AddWithValue("@Par_Mes", "");
                    cmd.Parameters.AddWithValue("@Par_Sucursal", "");

                    cmd.Parameters.AddWithValue("@Par_Salida", "S");
                    cmd.Parameters.AddWithValue("@Par_NumErr", "");
                    cmd.Parameters.AddWithValue("@Par_ErrMenDev", "");


                    drd = cmd.ExecuteReader();

                    while (drd.Read())
                    {
                        listaDao = new PermisoEmpleadoViewModel()
                        {
                            //SucursalID, Sucursal, UsuarioID 
                            SucursalID = drd["SucursalID"].ToString(),
                            Sucursal = drd["Sucursal"].ToString(),
                            Usuario = drd["UsuarioID"].ToString(),
                        };
                        lista.Add(listaDao);
                    }
                    drd.Close();
                }

                catch (Exception ex)
                {
                    throw ex;
                }
                return lista;
            }
        }

        //arbiol para permisos
        public List<PermisoEmpleadoViewModel> ListaTreeEmpDAO(int valor)
        {
            List<PermisoEmpleadoViewModel> lista = new List<PermisoEmpleadoViewModel>();
            PermisoEmpleadoViewModel listaDao;

            SqlDataReader drd = null;

            using (var conn = new SqlConnection(BaseDAO.analiticabd))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("PERMISOEMPLEADOLIST", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Par_PuestoID", "");
                    cmd.Parameters.AddWithValue("@Par_PermisoID", "");
                    cmd.Parameters.AddWithValue("@Par_NumLis", valor);

                    cmd.Parameters.AddWithValue("@Par_Anio", "");
                    cmd.Parameters.AddWithValue("@Par_Mes", "");
                    cmd.Parameters.AddWithValue("@Par_Sucursal", "");

                    cmd.Parameters.AddWithValue("@Par_Salida", "S");
                    cmd.Parameters.AddWithValue("@Par_NumErr", "");
                    cmd.Parameters.AddWithValue("@Par_ErrMenDev", "");


                    drd = cmd.ExecuteReader();

                    while (drd.Read())
                    {
                        listaDao = new PermisoEmpleadoViewModel()
                        {
                            Usuario = drd["UsuarioID"].ToString(),
                        };
                        lista.Add(listaDao);
                    }
                    drd.Close();
                }

                catch (Exception ex)
                {
                    throw ex;
                }
                return lista;
            }
        }

        //Delete permisos(permisos para visualizar la sucursal en la pantalla principal)
        public MensajeTransaccion DeletePermiso(UsuarioViewModel usuarioLogin, int valor)
        {
            MensajeTransaccion mensaje = null;
            SqlDataReader dr = null;
            SqlTransaction tran = null;

            try
            {
                using (var conn = new SqlConnection(BaseDAO.analiticabd))
                {
                    conn.Open();

                    tran = conn.BeginTransaction();
                    SqlCommand cmd = new SqlCommand("PERMISOEMPLEADOLIST", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Par_PuestoID", "");
                    cmd.Parameters.AddWithValue("@Par_PermisoID", "");
                    cmd.Parameters.AddWithValue("@Par_NumLis", valor);
                    cmd.Parameters.AddWithValue("@Par_Anio", "");

                    cmd.Parameters.AddWithValue("@Par_Mes", "");
                    cmd.Parameters.AddWithValue("@Par_Sucursal", usuarioLogin.CodigoAlterno);
                    // cmd.Parameters.AddWithValue("@Par_Consecutivo", "");
                    cmd.Parameters.AddWithValue("@Par_Salida", "S");
                    cmd.Parameters.AddWithValue("@Par_NumErr", "");
                    cmd.Parameters.AddWithValue("@Par_ErrMenDev", "");


                    cmd.Transaction = tran;

                    try
                    {
                        dr = cmd.ExecuteReader();
                        while (dr.Read())
                        {
                            mensaje = new MensajeTransaccion();
                            mensaje.CodigoRespuesta = dr["NumErr"].ToString();
                            mensaje.MensajeRespuesta = dr["ErrMenDev"].ToString();
                            //   mensaje.Consecutivo = dr["Consecutivo"].ToString();
                        }
                        dr.Close();
                        if (!mensaje.CodigoRespuesta.Equals("000000"))
                        {
                            throw new Exception();
                        }
                        tran.Commit();
                    }
                    catch (Exception ex)
                    {
                        tran.Rollback();
                        if (mensaje == null)
                        {
                            mensaje = new MensajeTransaccion();
                            mensaje.CodigoRespuesta = "CD-000002";
                            mensaje.MensajeRespuesta = "-CAPA DAO- \n OCURRIÓ UN ERROR AL ELIMINAR EL PERMISO, EN EL PROCESO " +
                                " PERMISOEMPLEADOLIST \n Error técnico: " + ex.Message;
                        }
                    }

                }
            }
            catch (Exception)
            {
                throw;
            }
            return mensaje;
        }


        //Delete permisos(permisos para visualizar las vistas del proyecto)
        public MensajeTransaccion DeletePermisoLog(UsuarioViewModel usuarioLogin, int valor)
        {
            MensajeTransaccion mensaje = null;
            SqlDataReader dr = null;
            SqlTransaction tran = null;

            try
            {
                using (var conn = new SqlConnection(BaseDAO.analiticabd))
                {
                    conn.Open();

                    tran = conn.BeginTransaction();
                    SqlCommand cmd = new SqlCommand("PERMISOEMPLEADOLIST", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Par_PuestoID", "");
                    cmd.Parameters.AddWithValue("@Par_PermisoID", usuarioLogin.Em_Puesto);
                    cmd.Parameters.AddWithValue("@Par_NumLis", valor);
                    cmd.Parameters.AddWithValue("@Par_Anio", "");

                    cmd.Parameters.AddWithValue("@Par_Mes", "");
                    cmd.Parameters.AddWithValue("@Par_Sucursal", "");
                    // cmd.Parameters.AddWithValue("@Par_Consecutivo", "");
                    cmd.Parameters.AddWithValue("@Par_Salida", "S");
                    cmd.Parameters.AddWithValue("@Par_NumErr", "");
                    cmd.Parameters.AddWithValue("@Par_ErrMenDev", "");


                    cmd.Transaction = tran;

                    try
                    {
                        dr = cmd.ExecuteReader();
                        while (dr.Read())
                        {
                            mensaje = new MensajeTransaccion();
                            mensaje.CodigoRespuesta = dr["NumErr"].ToString();
                            mensaje.MensajeRespuesta = dr["ErrMenDev"].ToString();
                            //   mensaje.Consecutivo = dr["Consecutivo"].ToString();
                        }
                        dr.Close();
                        if (!mensaje.CodigoRespuesta.Equals("000000"))
                        {
                            throw new Exception();
                        }
                        tran.Commit();
                    }
                    catch (Exception ex)
                    {
                        tran.Rollback();
                        if (mensaje == null)
                        {
                            mensaje = new MensajeTransaccion();
                            mensaje.CodigoRespuesta = "CD-000002";
                            mensaje.MensajeRespuesta = "-CAPA DAO- \n OCURRIÓ UN ERROR AL ELIMINAR EL PERMISO, EN EL PROCESO " +
                                " PERMISOEMPLEADOLIST \n Error técnico: " + ex.Message;
                        }
                    }

                }
            }
            catch (Exception)
            {
                throw;
            }
            return mensaje;
        }




    }
}