﻿using ENTIDADES.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ENTIDADES.Mantenimiento;
using System.Data.SqlClient;
using DAO.Base;

namespace DAO.Mantenimiento
{
    public class OperacionesAltDAO
    {


        //ALTA DE LA BASE DE LA MATRIZ DE INDICADORES DE GESTION (OPERACIONES)
        public MensajeTransaccion AltaBase(OperacionesViewModel operaciones)
        {
            MensajeTransaccion mensaje = null;
            SqlDataReader dr = null;
            SqlTransaction tran = null;

            try
            {
                using (var conn = new SqlConnection(BaseDAO.analiticabd))
                {
                    conn.Open();

                    tran = conn.BeginTransaction();
                    SqlCommand cmd = new SqlCommand("BASEALT", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Par_Nombre", operaciones.Nombre);
                    cmd.Parameters.AddWithValue("@Par_Valor", operaciones.Valor);
                    //cmd.Parameters.AddWithValue("@Par_Frecuencia", operaciones.Frecuencia);
                    cmd.Parameters.AddWithValue("@Par_SucursalID", operaciones.SucursalID);
                    if(operaciones.AliadoID==null)
                    {
                        cmd.Parameters.AddWithValue("@Par_AliadoID", "Todos");
                    }
                    else {
                        cmd.Parameters.AddWithValue("@Par_AliadoID", operaciones.AliadoID);
                    }
                    
                    cmd.Parameters.AddWithValue("@Par_MesAnio", operaciones.mesAnio);

                    cmd.Parameters.AddWithValue("@Par_Salida", "S");
                    cmd.Parameters.AddWithValue("@Par_NumErr", "");
                    cmd.Parameters.AddWithValue("@Par_ErrMenDev", "");
                   // cmd.Parameters.AddWithValue("@Par_Consecutivo", "");
                    cmd.Transaction = tran;
                   
                    try
                    {
                        dr = cmd.ExecuteReader();
                        while (dr.Read())
                        {
                            mensaje = new MensajeTransaccion();
                            mensaje.CodigoRespuesta = dr["NumErr"].ToString();
                            mensaje.MensajeRespuesta = dr["ErrMenDev"].ToString();
                         //   mensaje.Consecutivo = dr["Consecutivo"].ToString();
                        }
                        dr.Close();
                        if (!mensaje.CodigoRespuesta.Equals("000000"))
                        {
                            throw new Exception();
                        }
                        tran.Commit();
                    }
                    catch (Exception ex)
                     {
                        tran.Rollback();
                        if (mensaje == null)
                        {
                            mensaje = new MensajeTransaccion();
                            mensaje.CodigoRespuesta = "CD-000001";
                            mensaje.MensajeRespuesta = "-CAPA DAO- \n OCURRIÓ UN ERROR AL CREAR LA BASE, EN EL PROCESO " +
                                " BASEALT \n Error técnico: " + ex.Message;
                        }
                    }

                }
            }
            catch (Exception)
            {
                throw;
            }
            return mensaje;
        }

        
        //ALTA DEL DIA HABIL DE LA MATRIZ DE INDICADORES DE GESTION (OPERACIONES)
        public MensajeTransaccion AltaDiaHabil(OperacionesViewModel operaciones, string DiaInhabil)
        {
            MensajeTransaccion mensaje = null;
            SqlDataReader dr = null;
            SqlTransaction tran = null;

            try
            {
                using (var conn = new SqlConnection(BaseDAO.analiticabd))
                {
                    conn.Open();

                    tran = conn.BeginTransaction();
                    SqlCommand cmd = new SqlCommand("DIAHABILALT", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Par_SucursalID", operaciones.SucursalID);
                    cmd.Parameters.AddWithValue("@Par_MesAnio", operaciones.mesAnio);
                    cmd.Parameters.AddWithValue("@Par_FechaInhabil", DiaInhabil);
                    cmd.Parameters.AddWithValue("@Par_Salida", "S");
                    cmd.Parameters.AddWithValue("@Par_NumErr", "");
                    cmd.Parameters.AddWithValue("@Par_ErrMenDev", "");

                    cmd.Transaction = tran;

                    try
                    {
                        dr = cmd.ExecuteReader();
                        while (dr.Read())
                        {
                            mensaje = new MensajeTransaccion();
                            mensaje.CodigoRespuesta = dr["NumErr"].ToString();
                            mensaje.MensajeRespuesta = dr["ErrMenDev"].ToString();
                            //   mensaje.Consecutivo = dr["Consecutivo"].ToString();
                        }
                        dr.Close();
                        if (!mensaje.CodigoRespuesta.Equals("000000"))
                        {
                            throw new Exception();
                        }
                        tran.Commit();
                    }
                    catch (Exception ex)
                    {
                        tran.Rollback();
                        if (mensaje == null)
                        {
                            mensaje = new MensajeTransaccion();
                            mensaje.CodigoRespuesta = "CD-000002";
                            mensaje.MensajeRespuesta = "-CAPA DAO- \n OCURRIÓ UN ERROR AL CREAR LOS DIAS HABILES, EN EL PROCESO " +
                                " DIAHABILALT \n Error técnico: " + ex.Message;
                        }
                    }

                }
            }
            catch (Exception)
            {
                throw;
            }
            return mensaje;
        }



    }
}
