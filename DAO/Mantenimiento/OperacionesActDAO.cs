﻿using DAO.Base;
using ENTIDADES.Base;
using ENTIDADES.Mantenimiento;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO.Mantenimiento
{
    public class OperacionesActDAO
    {

        //Actualizar la base/Meta del indicador de operaciones(MAtriz de indicador de gestion)
        public MensajeTransaccion ActualizarMantenimientoOperaciones(OperacionesViewModel Mantenimiento, int valorAct)
        {
            MensajeTransaccion mensaje = null;
            SqlDataReader dr = null;
            SqlTransaction tran = null;
            try
            {
                using (var conn = new SqlConnection(BaseDAO.analiticabd))
                {

                    conn.Open();
                    tran = conn.BeginTransaction();
                    SqlCommand cmd = new SqlCommand("MANOPERACIONESACT", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Par_FolioID", Mantenimiento.BaseID);
                    cmd.Parameters.AddWithValue("@Par_Nombre", Mantenimiento.Nombre);
                    cmd.Parameters.AddWithValue("@Par_Meta", Mantenimiento.Valor);
                    cmd.Parameters.AddWithValue("@Par_SucursalID", Mantenimiento.SucursalID);
                    cmd.Parameters.AddWithValue("@Par_Aliado", Mantenimiento.AliadoID);
                    cmd.Parameters.AddWithValue("@Par_MesAnio", Mantenimiento.mesAnio);    
                    cmd.Parameters.AddWithValue("@Par_NumAct", valorAct);
                    cmd.Parameters.AddWithValue("@Par_Salida", "S");
                    cmd.Parameters.AddWithValue("@Par_NumErr", "");
                    cmd.Parameters.AddWithValue("@Par_ErrMenDev", "");
                    cmd.Parameters.AddWithValue("@Par_Consecutivo", "");
                    
                    cmd.Transaction = tran;

                    try
                    {
                        dr = cmd.ExecuteReader();
                        while (dr.Read())
                        {
                            mensaje = new MensajeTransaccion();
                            mensaje.CodigoRespuesta = dr["NumErr"].ToString();
                            mensaje.MensajeRespuesta = dr["ErrMenDev"].ToString();
                        }
                        dr.Close();
                        if (!mensaje.CodigoRespuesta.Equals("000000"))
                        {
                            throw new Exception();
                        }
                        tran.Commit();
                    }
                    catch (Exception ex)
                    {
                        tran.Rollback();
                        if (mensaje == null)
                        {
                            mensaje = new MensajeTransaccion();
                            mensaje.CodigoRespuesta = "CD-000001";
                            mensaje.MensajeRespuesta = "-CAPA DAO- \n OCURRIÓ UN ERROR AL MODIFICAR LA BASE, EN EL PROCESO " +
                                " MANOPERACIONESACT \n Error técnico: " + ex.Message;
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return mensaje;
        }

        //Actualizar el dia habil del indicador de operaciones(MAtriz de indicador de gestion)
        public MensajeTransaccion ActualizarDiaHabil(OperacionesViewModel Mantenimiento, int valorAct)
        {
            MensajeTransaccion mensaje = null;
            SqlDataReader dr = null;
            SqlTransaction tran = null;
            try
            {
                using (var conn = new SqlConnection(BaseDAO.analiticabd))
                {

                    conn.Open();
                    tran = conn.BeginTransaction();
                    SqlCommand cmd = new SqlCommand("MANOPERACIONESACT", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Par_FolioID", "");
                    cmd.Parameters.AddWithValue("@Par_Nombre", "");
                    cmd.Parameters.AddWithValue("@Par_Meta",0.0);
                    cmd.Parameters.AddWithValue("@Par_SucursalID", Mantenimiento.SucursalID.Trim());
                    cmd.Parameters.AddWithValue("@Par_Aliado", "");
                    cmd.Parameters.AddWithValue("@Par_MesAnio", Mantenimiento.mesAnio);
                    cmd.Parameters.AddWithValue("@Par_NumAct", valorAct);
                    cmd.Parameters.AddWithValue("@Par_Salida", "S");
                    cmd.Parameters.AddWithValue("@Par_NumErr", "");
                    cmd.Parameters.AddWithValue("@Par_ErrMenDev", "");
                    cmd.Parameters.AddWithValue("@Par_Consecutivo", "");

                    cmd.Transaction = tran;

                    try
                    {
                        dr = cmd.ExecuteReader();
                        while (dr.Read())
                        {
                            mensaje = new MensajeTransaccion();
                            mensaje.CodigoRespuesta = dr["NumErr"].ToString();
                            mensaje.MensajeRespuesta = dr["ErrMenDev"].ToString();
                        }
                        dr.Close();
                        if (!mensaje.CodigoRespuesta.Equals("000000"))
                        {
                            throw new Exception();
                        }
                        tran.Commit();
                    }
                    catch (Exception ex)
                    {
                        tran.Rollback();
                        if (mensaje == null)
                        {
                            mensaje = new MensajeTransaccion();
                            mensaje.CodigoRespuesta = "CD-000002";
                            mensaje.MensajeRespuesta = "-CAPA DAO- \n OCURRIÓ UN ERROR AL ELIMINAR EL DIA INHABILES, EN EL PROCESO " +
                                " MANOPERACIONESACT \n Error técnico: " + ex.Message;
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return mensaje;
        }


    }
}
