﻿using ENTIDADES.Mantenimiento;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO.Mantenimiento
{
    public class OperacionConDAO
    {
        //Consulta de la base de la matriz de operaciones 

        //Consulta de mantenimiento de la base de la matriz de indicadores de gestion
        public OperacionesViewModel ConsultaBase(int valor, int bases)
        {
            OperacionesViewModel BaseView = null;

            SqlDataReader dr = null;
            SqlTransaction tran = null;

            try
            {
                using (var conn = new SqlConnection(Base.BaseDAO.analiticabd))
                {
                    conn.Open();
                    tran = conn.BeginTransaction();
                    SqlCommand cmd = new SqlCommand("MANTOPERACIONESCON", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Par_NumCon", valor);
                    cmd.Parameters.AddWithValue("@Par_FolioID", bases);
                    cmd.Parameters.AddWithValue("@Par_SucursalID", "");

                    cmd.Parameters.AddWithValue("@Par_Nombre", "");
                    
                    cmd.Parameters.AddWithValue("@Par_Anio", "");
                    cmd.Parameters.AddWithValue("@Par_Mes", "");

                    cmd.Transaction = tran;
                    try
                    {
                        dr = cmd.ExecuteReader();

                        BaseView = new OperacionesViewModel();
                        if (dr.Read())
                        {

                            BaseView.BaseID = int.Parse(dr["BaseID"].ToString().Trim());
                            BaseView.Nombre = dr["NomIndicador"].ToString().Trim();
                            BaseView.Valor = decimal.Parse(dr["ValBase"].ToString().Trim());
                            BaseView.SucursalID = dr["SucursalID"].ToString().Trim();
                            BaseView.AliadoID = dr["AliadoID"].ToString().Trim();
                            BaseView.NomMes = dr["NomMes"].ToString().Trim();

                        }
                        dr.Close();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return BaseView;
        }


        //Consulta de Matriz de indicadores de gestion --DIAS HABILES con base al folioID(Mantenimiento)
        public OperacionesViewModel ConsultaDiaHabil(OperacionesViewModel operacion,int valor)
        {
            OperacionesViewModel metadiasHAbiles = null;

            SqlDataReader dr = null;
            SqlTransaction tran = null;

            try
            {
                using (var conn = new SqlConnection(Base.BaseDAO.analiticabd))
                {
                    conn.Open();
                    tran = conn.BeginTransaction();
                    SqlCommand cmd = new SqlCommand("MANTOPERACIONESCON", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Par_NumCon", valor);
                    cmd.Parameters.AddWithValue("@Par_FolioID", 0);
                    cmd.Parameters.AddWithValue("@Par_SucursalID", operacion.SucursalID);

                    cmd.Parameters.AddWithValue("@Par_Anio", operacion.Anio);
                    cmd.Parameters.AddWithValue("@Par_Mes", operacion.Mes);


                    cmd.Transaction = tran;
                    try
                    {
                        dr = cmd.ExecuteReader();

                        metadiasHAbiles = new OperacionesViewModel();
                        if (dr.Read())
                        {
                            
                            //metadiasHAbiles.Folio = int.Parse(dr["FolioID"].ToString().Trim());
                            metadiasHAbiles.SucursalID = dr["SucursalID"].ToString().Trim();
                            //metadiasHAbiles.Cantidad = int.Parse(dr["Cantidad"].ToString().Trim());
                            metadiasHAbiles.mesAnio = dr["Fecha"].ToString().Trim();

                        }
                        dr.Close();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return metadiasHAbiles;
        }


        //Consulta de dia habil por medio de sucursal
        public OperacionesViewModel ConsultaDiaHabilSuc(OperacionesViewModel operacion, int valor)
        {
            OperacionesViewModel metadiasHAbiles = null;

            SqlDataReader dr = null;
            SqlTransaction tran = null;

            try
            {
                using (var conn = new SqlConnection(Base.BaseDAO.analiticabd))
                {
                    conn.Open();
                    tran = conn.BeginTransaction();
                    SqlCommand cmd = new SqlCommand("MANTOPERACIONESCON", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Par_NumCon", valor);
                    cmd.Parameters.AddWithValue("@Par_FolioID","");
                    cmd.Parameters.AddWithValue("@Par_SucursalID", operacion.SucursalID);
                    cmd.Parameters.AddWithValue("@Par_Nombre", "");

                    if (!operacion.Anio.Equals(0) & !operacion.Mes.Equals(0))
                    {
                        cmd.Parameters.AddWithValue("@Par_Anio", operacion.Anio);
                        cmd.Parameters.AddWithValue("@Par_Mes", operacion.Mes);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@Par_Anio", "");
                        cmd.Parameters.AddWithValue("@Par_Mes", "");
                    }

                    cmd.Transaction = tran;
                    try
                    {
                        dr = cmd.ExecuteReader();

                        metadiasHAbiles = new OperacionesViewModel();
                        if (dr.Read())
                        {
                            
                            metadiasHAbiles.DiasHabiles = int.Parse(dr["DiasHabiles"].ToString().Trim());
                            metadiasHAbiles.DiasTranscurridos = int.Parse(dr["DiasTranscurridos"].ToString().Trim());

                        }
                        dr.Close();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return metadiasHAbiles;
        }

    }
}
