﻿using ENTIDADES.Mantenimiento;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO.Mantenimiento
{
    public class OperacionesConDAO
    {

        //Consulta la base para el mantenimiento de catalogos
        public OperacionesViewModel ConsultaBase(int valor)
        {
            OperacionesViewModel metaCartera = null;

            SqlDataReader dr = null;
            SqlTransaction tran = null;
            string DBCadena;

            try
            {

                using (var conn = new SqlConnection(Base.BaseDAO.analiticabd))
                {
                    conn.Open();
                    tran = conn.BeginTransaction();
                    SqlCommand cmd = new SqlCommand("MANTOPERACIONESCON", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Par_NumCon", valor);
                    cmd.Parameters.AddWithValue("@Par_FolioID", valor);
                    //cmd.Parameters.AddWithValue("@Par_Sucursal", 1);
                    cmd.Transaction = tran;
                    try
                    {
                        dr = cmd.ExecuteReader();

                        metaCartera = new OperacionesViewModel();
                        if (dr.Read())
                        {
                            //aqui puse los archivos del ticket
                            metaCartera.BaseID = int.Parse(dr["BaseID"].ToString().Trim());
                            metaCartera.Nombre = dr["NomIndicador"].ToString().Trim();
                            metaCartera.Valor = int.Parse(dr["ValBase"].ToString().Trim());
                            metaCartera.SucursalID = dr["SucursalID"].ToString().Trim();
                            metaCartera.AliadoID = dr["Aliado"].ToString().Trim();
                            metaCartera.NomMes = dr["NomMes"].ToString().Trim();

                        }
                        dr.Close();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return metaCartera;
        }


    }
}
