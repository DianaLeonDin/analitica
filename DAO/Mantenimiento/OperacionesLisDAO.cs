﻿using DAO.Base;
using ENTIDADES.Mantenimiento;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO.Mantenimiento
{
    public class OperacionesLisDAO
    {
        //Base de la matriz de indicadores de gestion de operaciones
        public List<OperacionesViewModel> ListaBaseDAO(OperacionesViewModel operaciones,int valorLista)
        {
            List<OperacionesViewModel> listaBase = new List<OperacionesViewModel>();
            OperacionesViewModel listaDao;

            SqlDataReader drd = null;

            using (var conn = new SqlConnection(BaseDAO.analiticabd))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("MATRIZGESTIONLIS", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Par_NumLis", valorLista);
                    //hasta poner el mes anterior para que se busque 
                    
                        cmd.Parameters.AddWithValue("@Par_AliadoID", "");

                    if (valorLista.Equals(2))
                    {
                        cmd.Parameters.AddWithValue("@Par_SucursalID", "");
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@Par_SucursalID", operaciones.SucursalID);
                    }

                    if (!operaciones.Anio.Equals(0) & !operaciones.Mes.Equals(0))
                    {
                        cmd.Parameters.AddWithValue("@Par_Anio", operaciones.Anio);
                        cmd.Parameters.AddWithValue("@Par_Mes", operaciones.Mes);
                        //cmd.Parameters.AddWithValue("@Par_SucursalID", operaciones.SucursalID);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@Par_Anio", "");
                        cmd.Parameters.AddWithValue("@Par_Mes", "");
                    }

                    drd = cmd.ExecuteReader();

                    while (drd.Read())
                    {
                        listaDao = new OperacionesViewModel()
                        {
                            BaseID = int.Parse(drd["BaseID"].ToString()),
                            Nombre = drd["NomIndicador"].ToString(),
                            Valor = Decimal.Parse(drd["ValBase"].ToString()),
                            SucursalID = drd["SucursalID"].ToString(),
                            AliadoID = drd["AliadoID"].ToString(),
                            Mes = int.Parse(drd["Mes"].ToString()),
                            NomMes = drd["NomMes"].ToString(),
                            Anio = int.Parse(drd["Anio"].ToString()),
                            Descripcion = drd["Descripcion"].ToString(),
                        };
                        listaBase.Add(listaDao);
                    }
                    drd.Close();
                }

                catch (Exception ex)
                {
                    throw ex;
                }
                return listaBase;
            }
        }

        //dias habiles de la matriz de indicadores de gestion de operaciones
        public List<OperacionesViewModel> ListaDiaHabilDAO(int valorLista)
        {
            List<OperacionesViewModel> listaDiaHabil = new List<OperacionesViewModel>();
            OperacionesViewModel listaDao;

            SqlDataReader drd = null;

            using (var conn = new SqlConnection(BaseDAO.analiticabd))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("MATRIZGESTIONLIS", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Par_NumLis", valorLista);
                    cmd.Parameters.AddWithValue("@Par_SucursalID", "");

                    cmd.Parameters.AddWithValue("@Par_AliadoID", "");

                    //hasta poner el mes anterior para que se busque 
                    cmd.Parameters.AddWithValue("@Par_Anio", "");
                    cmd.Parameters.AddWithValue("@Par_Mes", "");


                    drd = cmd.ExecuteReader();

                    while (drd.Read())
                    {
                        listaDao = new OperacionesViewModel()
                        {
                            SucursalID = drd["SucursalID"].ToString(),
                            NomMes = drd["NomMes"].ToString(),
                            DiasHabiles = int.Parse(drd["DiaInhabil"].ToString()),
                        };
                        listaDiaHabil.Add(listaDao);
                    }
                    drd.Close();
                }

                catch (Exception ex)
                {
                    throw ex;
                }
                return listaDiaHabil;
            }
        }

    }
}
