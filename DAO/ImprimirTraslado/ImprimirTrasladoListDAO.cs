﻿using ENTIDADES.ArticuloBodega;
using ENTIDADES.ArticuloSucursal;
using ENTIDADES.Conexion;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO.ImprimirTraslado
{
    public class ImprimirTrasladoListDAO
    {

        //SP_ListarImprimirTraslado SUCURSAL
        public List<ArticuloSucursalViewModel> ListArtTrasSucDAO(ConexionSucursalViewModel sucursalResponse)
        {
            List<ArticuloSucursalViewModel> lista = new List<ArticuloSucursalViewModel>();
            ArticuloSucursalViewModel listaDao;

            SqlDataReader drd = null;

            // string cadena;
            //cadena = System.Web.HttpContext.Current.Session["DbConexion"].ToString();

            using (var conn = new SqlConnection(sucursalResponse.DbSucursal))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("SP_ListarImprimirTraslado", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Par_numLis", 1);
                    cmd.Parameters.AddWithValue("@Par_Sucursal", sucursalResponse.Cve_Sucursal);

                    drd = cmd.ExecuteReader();

                    while (drd.Read())
                    {
                        listaDao = new ArticuloSucursalViewModel()
                        {
                            FolioMovOriginal = int.Parse(drd["FolioMovOriginal"].ToString()),
                            Descripcion = drd["Descripcion"].ToString(),
                            Prestamo = drd["Prestamo"].ToString(),
                            ClaveSucursal = int.Parse(drd["ClaveSucursal"].ToString()),
                            NombreSucursal = drd["NombreSucursal"].ToString(),
                            Ubicacion = drd["Ubicacion"].ToString(),

                        };
                        lista.Add(listaDao);
                    }
                    drd.Close();
                }

                catch (Exception ex)
                {
                    throw ex;
                }
                return lista;
            }
        }

        //SP_ListarImprimirTraslado BC
        public List<ArticuloBodegaViewModel> ListArtTrasBCDAO(ConexionSucursalViewModel sucursalResponse)
        {
            List<ArticuloBodegaViewModel> lista = new List<ArticuloBodegaViewModel>();
            ArticuloBodegaViewModel listaDao;
            string DbBode;

            SqlDataReader drd = null;


            //cadena = System.Web.HttpContext.Current.Session["DbConexion"].ToString();
            DbBode = System.Web.HttpContext.Current.Session["DbConexionBod"].ToString();
            using (var conn = new SqlConnection(DbBode))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("SP_ListarImprimirTraslado", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Par_numLis", 2);
                    cmd.Parameters.AddWithValue("@Par_Sucursal", sucursalResponse.Cve_Sucursal);
                    


                    drd = cmd.ExecuteReader();

                    while (drd.Read())
                    {
                        listaDao = new ArticuloBodegaViewModel()
                        {
                            FolMov = int.Parse(drd["FolMov"].ToString()),
                            Em_Descripcion = drd["Em_Descripcion"].ToString(),
                            Em_Importe = drd["Em_Importe"].ToString(),
                            Sucursal = drd["Sucursal"].ToString(),                            
                            Sc_Descripcion = drd["Sc_Descripcion"].ToString(),
                                               
                        };
                        lista.Add(listaDao);
                    }
                    drd.Close();
                }

                catch (Exception ex)
                {
                    throw ex;
                }
                return lista;
            }
        }


    }
}
