﻿using DAO.Base;
using ENTIDADES.Base;
using ENTIDADES.Login;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO.EmpleadoERP
{
    public class EmpleadoActDAO: IEmpleadoActDAO
    {
        public enum Act_Em{
            act_contraseña = 1

        }


        public MensajeTransaccion empleadoAct_01(UsuarioViewModel usuarioResponse) {
            MensajeTransaccion mensaje = null;
            try
            {
                SqlTransaction tran;
                SqlDataReader dr;

                using (var conn = new SqlConnection(BaseDAO.dbdinbd))
                {
                    conn.Open();

                    tran = conn.BeginTransaction();
                    SqlCommand cmd = new SqlCommand("USREMPLEADOACT", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Par_Em_Cve_Empleado", usuarioResponse.CveEmpleado);
                    cmd.Parameters.AddWithValue("@Par_Em_Password", usuarioResponse.Password);
                    cmd.Parameters.AddWithValue("@Par_numAct", Act_Em.act_contraseña);
                    cmd.Parameters.AddWithValue("@Par_Salida", "S");
                    cmd.Parameters.AddWithValue("@Par_NumErr", "");
                    cmd.Parameters.AddWithValue("@Par_ErrMenDev", "");

                    cmd.Transaction = tran;

                    try
                    {
                        dr = cmd.ExecuteReader();
                        while (dr.Read())
                        {
                            mensaje = new MensajeTransaccion();
                            mensaje.CodigoRespuesta = dr["NumErr"].ToString();
                            mensaje.MensajeRespuesta = dr["ErrMenDev"].ToString();

                        }
                        dr.Close();
                        if (!mensaje.CodigoRespuesta.Equals("000000"))
                        {
                            throw new Exception();
                        }
                        tran.Commit();
                    }
                    catch (Exception ex)
                    {

                        tran.Rollback();
                        if (mensaje == null)
                        {
                            mensaje = new MensajeTransaccion();
                            mensaje.CodigoRespuesta = "000001";
                            mensaje.MensajeRespuesta = "-CAPA DAO- \n OCURRIÓ UN ERROR AL CAMBIAR LA CONTRASEÑA, EN EL PROCESO " +
                                " USREMPLEADOACT \n Error técnico: " + ex.Message;
                        }
                    }




                }



            }
            catch (Exception)
            {

                throw;
            }

            return mensaje;
        }


    }
}
