﻿using ENTIDADES.Base;
using ENTIDADES.Login;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO.EmpleadoERP
{
    public interface IEmpleadoActDAO
    {
        MensajeTransaccion empleadoAct_01(UsuarioViewModel usuarioResponse);
    }
}
