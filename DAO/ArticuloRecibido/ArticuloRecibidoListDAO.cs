﻿using ENTIDADES.ArticuloBodega;
using ENTIDADES.Conexion;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO.ArticuloRecibido
{
    public class ArticuloRecibidoListDAO
    {
        //LISTADO DE LOS ARTICULOS QUE SE ENVIAN ALA BODEGA CENTRAL
        public List<ArticuloBodegaViewModel> ListadoArticuloTrasladoABodegaDAO(ConexionSucursalViewModel sucursalResponse)
        {
            List<ArticuloBodegaViewModel> lista = new List<ArticuloBodegaViewModel>();
            ArticuloBodegaViewModel listaDao;
            string DbBode;

            SqlDataReader drd = null;


            //cadena = System.Web.HttpContext.Current.Session["DbConexion"].ToString();
            DbBode = System.Web.HttpContext.Current.Session["DbConexionBod"].ToString();
            using (var conn = new SqlConnection(DbBode))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("SP_ZADD_LISTA_EMPEÑO", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Par_numLis", 2);
                    cmd.Parameters.AddWithValue("@Par_FechaIni", "");
                    cmd.Parameters.AddWithValue("@Par_FechaFin", "");

                    cmd.Parameters.AddWithValue("@Par_SucursalID", sucursalResponse.Cve_Sucursal);


                    drd = cmd.ExecuteReader();

                    while (drd.Read())
                    {
                        listaDao = new ArticuloBodegaViewModel()
                        {
                            Me_folio = drd["Me_folio"].ToString(),
                            Em_Descripcion = drd["Em_Descripcion"].ToString(),
                            Em_Importe = drd["Em_Importe"].ToString(),
                            Sc_Descripcion = drd["Sc_Descripcion"].ToString(),                          
                        };
                        lista.Add(listaDao);
                    }
                    drd.Close();
                }

                catch (Exception ex)
                {
                    throw ex;
                }
                return lista;
            }
        }


    }
}
