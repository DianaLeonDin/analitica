﻿using DAO.Base;
using ENTIDADES.Base;
using ENTIDADES.Login;
using ENTIDADES.Operaciones;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO.Operaciones
{
    public class MatrizGestionAltDAO
    {

        //ALTA DE LOS COMENTARIOS DE LA MATRIZ DE INDICADORES DE GESTION
        public MensajeTransaccion AltaComentario(ComentarioViewModel comentario, UsuarioViewModel usuarioLogin)
        {
            MensajeTransaccion mensaje = null;
            SqlDataReader dr = null;
            SqlTransaction tran = null;

            try
            {
                using (var conn = new SqlConnection(BaseDAO.analiticabd))
                {
                    conn.Open();

                    tran = conn.BeginTransaction();
                    SqlCommand cmd = new SqlCommand("COMENTARIOALT", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Par_Descripcion", comentario.Descripcion);
                    cmd.Parameters.AddWithValue("@Par_BaseID", comentario.BaseID);
                    cmd.Parameters.AddWithValue("@Par_UsuarioID", usuarioLogin.CodigoAlterno);
                    cmd.Parameters.AddWithValue("@Par_SucursalID", comentario.SucursalID);

                    cmd.Parameters.AddWithValue("@Par_Salida", "S");
                    cmd.Parameters.AddWithValue("@Par_NumErr", "");
                    cmd.Parameters.AddWithValue("@Par_ErrMenDev", "");
                    // cmd.Parameters.AddWithValue("@Par_Consecutivo", "");
                    cmd.Transaction = tran;

                    try
                    {
                        dr = cmd.ExecuteReader();
                        while (dr.Read())
                        {
                            mensaje = new MensajeTransaccion();
                            mensaje.CodigoRespuesta = dr["NumErr"].ToString();
                            mensaje.MensajeRespuesta = dr["ErrMenDev"].ToString();
                            //   mensaje.Consecutivo = dr["Consecutivo"].ToString();
                        }
                        dr.Close();
                        if (!mensaje.CodigoRespuesta.Equals("000000"))
                        {
                            throw new Exception();
                        }
                        tran.Commit();
                    }
                    catch (Exception ex)
                    {
                        tran.Rollback();
                        if (mensaje == null)
                        {
                            mensaje = new MensajeTransaccion();
                            mensaje.CodigoRespuesta = "CD-000001";
                            mensaje.MensajeRespuesta = "-CAPA DAO- \n OCURRIÓ UN ERROR AL CREAR EL COMENTARIO, EN EL PROCESO " +
                                " COMENTARIOALT \n Error técnico: " + ex.Message;
                        }
                    }

                }
            }
            catch (Exception)
            {
                throw;
            }
            return mensaje;
        }

       

    }
}
