﻿using DAO.Base;
using ENTIDADES.Operaciones;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO.Operaciones
{
    public class MatrizGestionConDAO
    {

        //----------------------------------    INDICADOR DIARIO -------------------------

        //Consulta de  Matriz de indicadores de gestion  --REAL  (%META CARTERA)
        public MatrizGestionViewModel ConsultaMetaCartera(int valor, ConexionSucursalViewModel cadenaConexion, MatrizGestionViewModel SucMesAnio)
        {
            MatrizGestionViewModel metaCartera = null;
      
            SqlDataReader dr = null;
            SqlTransaction tran = null;
            string DBCadena;
            //ConexionSucursalViewModel cadenaConexion = null;

            try
            {
                 //cadenaConexion = new ConexionSucursalViewModel();

                //cadenaConexion = System.Web.HttpContext.Current.Session["conexionDB"];
                //cadenaConexion.cadena = System.Web.HttpContext.Current.Session["conexionDB"].ToString();

                using (var conn = new SqlConnection(cadenaConexion.cadena))
                {
                    conn.Open();
                    tran = conn.BeginTransaction();
                    SqlCommand cmd = new SqlCommand("MATRIZGESTIONCON", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandTimeout = 300;
                    cmd.Parameters.AddWithValue("@Par_NumCon", valor);
                    cmd.Parameters.AddWithValue("@Par_Sucursal", "");

                    if (!SucMesAnio.Anio.Equals(0) & !SucMesAnio.Mes.Equals(0))
                    {
                        cmd.Parameters.AddWithValue("@Par_Anio", SucMesAnio.Anio);
                        cmd.Parameters.AddWithValue("@Par_Mes", SucMesAnio.Mes);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@Par_Anio", "");
                        cmd.Parameters.AddWithValue("@Par_Mes", "");
                    }


                    cmd.Transaction = tran;
                    try
                    {
                        dr = cmd.ExecuteReader();

                         metaCartera = new MatrizGestionViewModel();
                        if (dr.Read())
                        {
                            //aqui puse los archivos del ticket
                            metaCartera.Cartera = decimal.Parse(dr["Cartera"].ToString().Trim());
                            metaCartera.Producto = dr["Producto"].ToString().Trim();

                        }
                        dr.Close();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return metaCartera;
        }

        //Consulta de  Matriz de indicadores de gestion  --REAL  (%META INGRESO)
        public MatrizGestionViewModel ConsultaMetaIngreso(int valor, ConexionSucursalViewModel cadenaConexion, MatrizGestionViewModel SucMesAnio)
        {
            MatrizGestionViewModel metaIngreso = null;

            SqlDataReader dr = null;
            SqlTransaction tran = null;

            try
            {
                using (var conn = new SqlConnection(cadenaConexion.cadena))
                {
                    conn.Open();
                    tran = conn.BeginTransaction();
                    SqlCommand cmd = new SqlCommand("MATRIZGESTIONCON", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Par_NumCon", valor);
                    cmd.Parameters.AddWithValue("@Par_Sucursal", "");

                    if (!SucMesAnio.Anio.Equals(0) & !SucMesAnio.Mes.Equals(0))
                    {
                        cmd.Parameters.AddWithValue("@Par_Anio", SucMesAnio.Anio);
                        cmd.Parameters.AddWithValue("@Par_Mes", SucMesAnio.Mes);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@Par_Anio", "");
                        cmd.Parameters.AddWithValue("@Par_Mes", "");
                    }

                    cmd.Transaction = tran;
                    try
                    {
                        dr = cmd.ExecuteReader();

                        metaIngreso = new MatrizGestionViewModel();
                        if (dr.Read())
                        {

                            metaIngreso.ingreso = decimal.Parse(dr["Ingreso"].ToString().Trim());

                        }
                        dr.Close();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return metaIngreso;
        }

        //Consulta de Matriz de indicadores de gestion --REAL (Cliente nuevo empeño)
        public MatrizGestionViewModel ConsultaCltNvo(int valor, ConexionSucursalViewModel cadenaConexion, MatrizGestionViewModel SucMesAnio)
        {
            MatrizGestionViewModel metaCTNVO = null;

            SqlDataReader dr = null;
            SqlTransaction tran = null;

            try
            {
                using (var conn = new SqlConnection(cadenaConexion.cadena))
                {
                    conn.Open();
                    tran = conn.BeginTransaction();
                    SqlCommand cmd = new SqlCommand("MATRIZGESTIONCON", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandTimeout = 300;
                    cmd.Parameters.AddWithValue("@Par_NumCon", valor);
                    cmd.Parameters.AddWithValue("@Par_Sucursal", "");

                    if (!SucMesAnio.Anio.Equals(0) & !SucMesAnio.Mes.Equals(0))
                    {
                        cmd.Parameters.AddWithValue("@Par_Anio", SucMesAnio.Anio);
                        cmd.Parameters.AddWithValue("@Par_Mes", SucMesAnio.Mes);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@Par_Anio", "");
                        cmd.Parameters.AddWithValue("@Par_Mes", "");
                    }

                    cmd.Transaction = tran;
                    try
                    {
                        dr = cmd.ExecuteReader();

                        metaCTNVO = new MatrizGestionViewModel();
                        if (dr.Read())
                        {

                            metaCTNVO.TotalCteNvo = decimal.Parse(dr["TotalCteNvo"].ToString().Trim());
                            metaCTNVO.NumClientes = int.Parse(dr["NumClietes"].ToString().Trim());
                            //NumClietes

                        }
                        dr.Close();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return metaCTNVO;
        }

        //Consulta de Matriz de indicadores de gestion --REAL (Promedio monto empeño)
        public MatrizGestionViewModel ConsultaPromMontoEMpeño(int valor, ConexionSucursalViewModel cadenaConexion, MatrizGestionViewModel SucMesAnio)
        {
            MatrizGestionViewModel metaIngreso = null;

            SqlDataReader dr = null;
            SqlTransaction tran = null;

            try
            {
                using (var conn = new SqlConnection(cadenaConexion.cadena))
                {
                    conn.Open();
                    tran = conn.BeginTransaction();
                    SqlCommand cmd = new SqlCommand("MATRIZGESTIONCON", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                   
                    cmd.CommandTimeout = 300; // Timeout en hardcode :( 

                    cmd.Parameters.AddWithValue("@Par_NumCon", valor);
                    cmd.Parameters.AddWithValue("@Par_Sucursal", "");

                    if (!SucMesAnio.Anio.Equals(0) & !SucMesAnio.Mes.Equals(0))
                    {
                        cmd.Parameters.AddWithValue("@Par_Anio", SucMesAnio.Anio);
                        cmd.Parameters.AddWithValue("@Par_Mes", SucMesAnio.Mes);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@Par_Anio", "");
                        cmd.Parameters.AddWithValue("@Par_Mes", "");
                    }
                    

                    cmd.Transaction = tran;
                    try
                    {
                        dr = cmd.ExecuteReader();

                        metaIngreso = new MatrizGestionViewModel();
                        if (dr.Read())
                        {
                            metaIngreso.FolioCapPrest = decimal.Parse(dr["Folios"].ToString().Trim());
                            metaIngreso.PromedioSem = decimal.Parse(dr["PromedioSem"].ToString().Trim());
                            
                        }
                        dr.Close();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return metaIngreso;
        }
        
        //Consulta de Matriz de indicadores de gestion --REAL (Porcentaje Maximo de enajenacion)
        public MatrizGestionViewModel ConsultaPorcMaxEna(int valor, ConexionSucursalViewModel cadenaConexion, MatrizGestionViewModel SucMesAnio)
        {
            MatrizGestionViewModel metaIngreso = null;

            SqlDataReader dr = null;
            SqlTransaction tran = null;

            try
            {
                using (var conn = new SqlConnection(cadenaConexion.cadena))
                {
                    conn.Open();
                    tran = conn.BeginTransaction();
                    SqlCommand cmd = new SqlCommand("MATRIZGESTIONCON", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandTimeout = 300;
                    cmd.Parameters.AddWithValue("@Par_NumCon", valor);
                    cmd.Parameters.AddWithValue("@Par_Sucursal", "");
                    if (!SucMesAnio.Anio.Equals(0) & !SucMesAnio.Mes.Equals(0))
                    {
                        cmd.Parameters.AddWithValue("@Par_Anio", SucMesAnio.Anio);
                        cmd.Parameters.AddWithValue("@Par_Mes", SucMesAnio.Mes);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@Par_Anio", "");
                        cmd.Parameters.AddWithValue("@Par_Mes", "");
                    }

                    cmd.Transaction = tran;
                    try
                    {
                        dr = cmd.ExecuteReader();

                        metaIngreso = new MatrizGestionViewModel();
                        if (dr.Read())
                        {

                            metaIngreso.Folios = decimal.Parse(dr["Folios"].ToString().Trim());

                        }
                        dr.Close();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return metaIngreso;
        }

        //Consulta de Matriz de indicadores de gestion --REAL(Ingreso total)
        public MatrizGestionViewModel ConsultaIngTotal(int valor, ConexionSucursalViewModel cadenaConexion)
        {
            MatrizGestionViewModel ingTotal = null;

            SqlDataReader dread = null;
            SqlTransaction tran = null;

            try
            {
                using (var conn = new SqlConnection(Base.BaseDAO.sicavibd))
                {
                    conn.Open();
                    tran = conn.BeginTransaction();
                    SqlCommand cmd = new SqlCommand("MATRIZGESTIONCON", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Par_NumCon", valor);
                    cmd.Parameters.AddWithValue("@Par_Sucursal", Convert.ToInt32(cadenaConexion.sucursal));
                    //cmd.Parameters.AddWithValue("@Par_Sucursal", cadenaConexion.sucursal);
                    cmd.Parameters.AddWithValue("@Par_Anio", "");
                    cmd.Parameters.AddWithValue("@Par_Mes", "");
                    

                    cmd.Transaction = tran;
                    try
                    {
                        dread = cmd.ExecuteReader();

                        ingTotal = new MatrizGestionViewModel();
                        if (dread.Read())
                        {

                            ingTotal.IngTotal = decimal.Parse(dread["IngTotal"].ToString().Trim());

                        }
                        dread.Close();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ingTotal;
        }

        //Consulta de Matriz de indicadores de gestion --REAL (Cliente nuevo ventas)
        public MatrizGestionViewModel ConsultaCltNvoVta(int valor, ConexionSucursalViewModel cadenaConexion, MatrizGestionViewModel SucMesAnio)
        {
            MatrizGestionViewModel metaCTNVOVta = null;

            SqlDataReader dr = null;
            SqlTransaction tran = null;

            try
            {
                using (var conn = new SqlConnection(BaseDAO.sicavibd))
                {
                    conn.Open();
                    tran = conn.BeginTransaction();
                    SqlCommand cmd = new SqlCommand("MATRIZGESTIONCON", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandTimeout = 300;
                    cmd.Parameters.AddWithValue("@Par_NumCon", valor);
                    cmd.Parameters.AddWithValue("@Par_Sucursal", cadenaConexion.sucursal);

                    if (!SucMesAnio.Anio.Equals(0) & !SucMesAnio.Mes.Equals(0))
                    {
                        cmd.Parameters.AddWithValue("@Par_Anio", SucMesAnio.Anio);
                        cmd.Parameters.AddWithValue("@Par_Mes", SucMesAnio.Mes);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@Par_Anio", "");
                        cmd.Parameters.AddWithValue("@Par_Mes", "");
                    }

                    cmd.Transaction = tran;
                    try
                    {
                        dr = cmd.ExecuteReader();

                        metaCTNVOVta = new MatrizGestionViewModel();
                        if (dr.Read())
                        {

                            metaCTNVOVta.NumClientesVta = int.Parse(dr["NumClientes"].ToString().Trim());

                        }
                        dr.Close();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return metaCTNVOVta;
        }


        //----------------------------------    INDICADOR POR ALIADO -------------------------

        //Consulta de Matriz de indicadores de gestion --REAL (Promedio monto empeño)
        public MatrizGestionViewModel PromEmpAliado(ConexionSucursalViewModel cadenaConexion, MatrizGestionViewModel metaCartera, int valor)
        {
            MatrizGestionViewModel metaIngreso = null;

            SqlDataReader dr = null;
            SqlTransaction tran = null;

            try
            {
                using (var conn = new SqlConnection(cadenaConexion.cadena))
                {
                    conn.Open();
                    tran = conn.BeginTransaction();
                    SqlCommand cmd = new SqlCommand("MATRIZGESTIONCON", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Par_NumCon", valor);
                    cmd.Parameters.AddWithValue("@Par_Sucursal",metaCartera.Cusr);
                    

                    if (!metaCartera.Anio.Equals(0) & !metaCartera.Mes.Equals(0))
                    {
                        cmd.Parameters.AddWithValue("@Par_Anio", metaCartera.Anio);
                        cmd.Parameters.AddWithValue("@Par_Mes", metaCartera.Mes);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@Par_Anio", "");
                        cmd.Parameters.AddWithValue("@Par_Mes", "");
                    }


                    cmd.Transaction = tran;
                    try
                    {
                        dr = cmd.ExecuteReader();

                        metaIngreso = new MatrizGestionViewModel();
                        if (dr.Read())
                        {

                            metaIngreso.PromedioSem = decimal.Parse(dr["PromedioSem"].ToString().Trim());

                        }
                        dr.Close();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return metaIngreso;
        }

        //Consulta de  Matriz de indicadores de gestion  --REAL  (%META CARTERA)
        public MatrizGestionViewModel ConsultaMetaAliadoCartera(ConexionSucursalViewModel cadenaConexion, MatrizGestionViewModel mmetaCartera, int valorMCAliado)
        {
            MatrizGestionViewModel metaCartera = null;

            SqlDataReader dr = null;
            SqlTransaction tran = null;
            //ConexionSucursalViewModel cadenaConexion = null;

            try
            {
                
                using (var conn = new SqlConnection(cadenaConexion.cadena))
                {
                    conn.Open();
                    tran = conn.BeginTransaction();
                    SqlCommand cmd = new SqlCommand("MATRIZGESTIONCON", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Par_NumCon", valorMCAliado);
                    cmd.Parameters.AddWithValue("@Par_Sucursal", mmetaCartera.Cusr);

                    if (!mmetaCartera.Anio.Equals(0) & !mmetaCartera.Mes.Equals(0))
                    {
                        cmd.Parameters.AddWithValue("@Par_Anio", mmetaCartera.Anio);
                        cmd.Parameters.AddWithValue("@Par_Mes", mmetaCartera.Mes);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@Par_Anio", "");
                        cmd.Parameters.AddWithValue("@Par_Mes", "");
                    }


                    cmd.Transaction = tran;
                    try
                    {
                        dr = cmd.ExecuteReader();

                        metaCartera = new MatrizGestionViewModel();
                        if (dr.Read())
                        {
                            //aqui puse los archivos del ticket
                            metaCartera.Cartera = decimal.Parse(dr["Cartera"].ToString().Trim());

                        }
                        dr.Close();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return metaCartera;
        }

        //Consulta de Matriz de indicadores de gestion --REAL (Cliente nuevo)
        public MatrizGestionViewModel ConsultaCltNvoAliado(ConexionSucursalViewModel cadenaConexion, MatrizGestionViewModel matriz, int valorCNAliado)
        {
            MatrizGestionViewModel metaCTNVO = null;

            SqlDataReader dr = null;
            SqlTransaction tran = null;

            try
            {
                using (var conn = new SqlConnection(cadenaConexion.cadena))
                {
                    conn.Open();
                    tran = conn.BeginTransaction();
                    SqlCommand cmd = new SqlCommand("MATRIZGESTIONCON", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Par_NumCon", valorCNAliado);
                    cmd.Parameters.AddWithValue("@Par_Sucursal", matriz.Cusr);

                    if (!matriz.Anio.Equals(0) & !matriz.Mes.Equals(0))
                    {
                        cmd.Parameters.AddWithValue("@Par_Anio", matriz.Anio);
                        cmd.Parameters.AddWithValue("@Par_Mes", matriz.Mes);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@Par_Anio", "");
                        cmd.Parameters.AddWithValue("@Par_Mes", "");
                    }

                    cmd.Transaction = tran;
                    try
                    {
                        dr = cmd.ExecuteReader();

                        metaCTNVO = new MatrizGestionViewModel();
                        if (dr.Read())
                        {

                            metaCTNVO.TotalCteNvo = int.Parse(dr["TotalCteNvo"].ToString());

                        }
                        dr.Close();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return metaCTNVO;
        }

        //Consulta de  Matriz de indicadores de gestion  --REAL  (META INGRESO (EMPEÑO))---INGRESO(EMPEÑO)
        public MatrizGestionViewModel ConsultaMIAliado(ConexionSucursalViewModel cadenaConexion, MatrizGestionViewModel matriz, int valorIEAliado)
        {
            MatrizGestionViewModel metaIngreso = null;

            SqlDataReader dr = null;
            SqlTransaction tran = null;

            try
            {
                using (var conn = new SqlConnection(cadenaConexion.cadena))
                {
                    conn.Open();
                    tran = conn.BeginTransaction();
                    SqlCommand cmd = new SqlCommand("MATRIZGESTIONCON", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Par_NumCon", valorIEAliado);
                    cmd.Parameters.AddWithValue("@Par_Sucursal", matriz.Cusr);

                    if (!matriz.Anio.Equals(0) & !matriz.Mes.Equals(0))
                    {
                        cmd.Parameters.AddWithValue("@Par_Anio", matriz.Anio);
                        cmd.Parameters.AddWithValue("@Par_Mes", matriz.Mes);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@Par_Anio", "");
                        cmd.Parameters.AddWithValue("@Par_Mes", "");
                    }

                    cmd.Transaction = tran;
                    try
                    {
                        dr = cmd.ExecuteReader();

                        metaIngreso = new MatrizGestionViewModel();
                        if (dr.Read())
                        {

                            metaIngreso.ingreso = decimal.Parse(dr["Ingreso"].ToString().Trim());

                        }
                        dr.Close();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return metaIngreso;
        }

        //Consulta de Matriz de indicadores de gestion --REAL (Cartera total)
        public MatrizGestionViewModel ConsultaCarteraTotal(ConexionSucursalViewModel cadenaConexion, int valorCTAliado)
        {
            MatrizGestionViewModel metaCartTotal = null;

            SqlDataReader dr = null;
            SqlTransaction tran = null;

            try
            {
                using (var conn = new SqlConnection(cadenaConexion.cadena))
                {
                    conn.Open();
                    tran = conn.BeginTransaction();
                    SqlCommand cmd = new SqlCommand("MATRIZGESTIONCON", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandTimeout = 300;
                    cmd.Parameters.AddWithValue("@Par_NumCon", valorCTAliado);
                    cmd.Parameters.AddWithValue("@Par_Sucursal", "");
                    cmd.Parameters.AddWithValue("@Par_Anio", "");

                    cmd.Parameters.AddWithValue("@Par_Mes", "");

                    cmd.Transaction = tran;
                    try
                    {
                        dr = cmd.ExecuteReader();

                        metaCartTotal = new MatrizGestionViewModel();
                        if (dr.Read())
                        {

                            metaCartTotal.CarteraTotal = int.Parse(dr["CarteraTotal"].ToString().Trim());

                        }
                        dr.Close();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return metaCartTotal;
        }


        //----------------------------------    INDICADOR POR PRODUCTO -------------------------

        //Consulta de Matriz de indicadores de gestion --REAL (Promedio monto empeño)
        public MatrizGestionViewModel ConsultaPromEmpProducto(ConexionSucursalViewModel cadenaConexion, MatrizGestionViewModel metaCartera, int valor)
        {
            MatrizGestionViewModel metaIngreso = null;

            SqlDataReader dr = null;
            SqlTransaction tran = null;

            try
            {
                using (var conn = new SqlConnection(cadenaConexion.cadena))
                {
                    conn.Open();
                    tran = conn.BeginTransaction();
                    SqlCommand cmd = new SqlCommand("MATRIZGESTIONCON", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Par_NumCon", valor);
                    cmd.Parameters.AddWithValue("@Par_Sucursal", metaCartera.Producto.Trim());
                    cmd.Parameters.AddWithValue("@Par_Anio", "");
                    cmd.Parameters.AddWithValue("@Par_Mes", "");

                    cmd.Transaction = tran;
                    try
                    {
                        dr = cmd.ExecuteReader();

                        metaIngreso = new MatrizGestionViewModel();
                        if (dr.Read())
                        {

                            metaIngreso.PromedioSem = decimal.Parse(dr["PromedioSem"].ToString().Trim());

                        }
                        dr.Close();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return metaIngreso;
        }

        //Consulta de  Matriz de indicadores de gestion  --REAL  (%META CARTERA)
        public MatrizGestionViewModel ConsultaMetaCarteraProducto(ConexionSucursalViewModel cadenaConexion, MatrizGestionViewModel mmetaCartera, int valorMCProducto)
        {
            MatrizGestionViewModel metaCartera = null;

            SqlDataReader dr = null;
            SqlTransaction tran = null;
            //ConexionSucursalViewModel cadenaConexion = null;

            try
            {

                using (var conn = new SqlConnection(cadenaConexion.cadena))
                {
                    conn.Open();
                    tran = conn.BeginTransaction();
                    SqlCommand cmd = new SqlCommand("MATRIZGESTIONCON", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Par_NumCon", valorMCProducto);
                    cmd.Parameters.AddWithValue("@Par_Sucursal", mmetaCartera.Producto.Trim());

                    if (!mmetaCartera.Anio.Equals(0) & !mmetaCartera.Mes.Equals(0))
                    {
                        cmd.Parameters.AddWithValue("@Par_Anio", mmetaCartera.Anio);
                        cmd.Parameters.AddWithValue("@Par_Mes", mmetaCartera.Mes);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@Par_Anio", "");
                        cmd.Parameters.AddWithValue("@Par_Mes", "");
                    }


                    cmd.Transaction = tran;
                    try
                    {
                        dr = cmd.ExecuteReader();

                        metaCartera = new MatrizGestionViewModel();
                        if (dr.Read())
                        {
                            //aqui puse los archivos del ticket
                            metaCartera.Cartera = decimal.Parse(dr["Cartera"].ToString().Trim());

                        }
                        dr.Close();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return metaCartera;
        }

        //Consulta de Matriz de indicadores de gestion --REAL (Cliente nuevo)
        public MatrizGestionViewModel ConsultaCltNvoProdcuto(ConexionSucursalViewModel cadenaConexion, MatrizGestionViewModel matriz, int valorCNProducto)
        {
            MatrizGestionViewModel metaCTNVO = null;

            SqlDataReader dr = null;
            SqlTransaction tran = null;

            try
            {
                using (var conn = new SqlConnection(cadenaConexion.cadena))
                {
                    conn.Open();
                    tran = conn.BeginTransaction();
                    SqlCommand cmd = new SqlCommand("MATRIZGESTIONCON", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Par_NumCon", valorCNProducto);
                    cmd.Parameters.AddWithValue("@Par_Sucursal", matriz.Producto.Trim());

                    if (!matriz.Anio.Equals(0) & !matriz.Mes.Equals(0))
                    {
                        cmd.Parameters.AddWithValue("@Par_Anio", matriz.Anio);
                        cmd.Parameters.AddWithValue("@Par_Mes", matriz.Mes);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@Par_Anio", "");
                        cmd.Parameters.AddWithValue("@Par_Mes", "");
                    }

                    cmd.Transaction = tran;
                    try
                    {
                        dr = cmd.ExecuteReader();

                        metaCTNVO = new MatrizGestionViewModel();
                        if (dr.Read())
                        {

                            metaCTNVO.TotalCteNvo = int.Parse(dr["TotalCteNvo"].ToString().Trim());

                        }
                        dr.Close();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return metaCTNVO;
        }


        //Consulta de  Matriz de indicadores de gestion  --REAL  (META INGRESO (EMPEÑO))---INGRESO(EMPEÑO)
        public MatrizGestionViewModel ConsultaMetIngProducto(ConexionSucursalViewModel cadenaConexion, MatrizGestionViewModel matriz, int valorIEPoducto)
        {
            MatrizGestionViewModel metaIngreso = null;

            SqlDataReader dr = null;
            SqlTransaction tran = null;

            try
            {
                using (var conn = new SqlConnection(cadenaConexion.cadena))
                {
                    conn.Open();
                    tran = conn.BeginTransaction();
                    SqlCommand cmd = new SqlCommand("MATRIZGESTIONCON", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Par_NumCon", valorIEPoducto);
                    cmd.Parameters.AddWithValue("@Par_Sucursal", matriz.Producto.Trim());

                    if (!matriz.Anio.Equals(0) & !matriz.Mes.Equals(0))
                    {
                        cmd.Parameters.AddWithValue("@Par_Anio", matriz.Anio);
                        cmd.Parameters.AddWithValue("@Par_Mes", matriz.Mes);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@Par_Anio", "");
                        cmd.Parameters.AddWithValue("@Par_Mes", "");
                    }

                    cmd.Transaction = tran;
                    try
                    {
                        dr = cmd.ExecuteReader();

                        metaIngreso = new MatrizGestionViewModel();
                        if (dr.Read())
                        {

                            metaIngreso.ingreso = decimal.Parse(dr["Ingreso"].ToString().Trim());

                        }
                        dr.Close();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return metaIngreso;
        }


        //----------------------------------    INDICADOR POR AÑO -------------------------
        //Consulta de Matriz de indicadores de gestion --REAL (Prestamo promedio)
        public MatrizGestionViewModel ConsultaPresPAnio(ConexionSucursalViewModel cadenaConexion, MatrizGestionViewModel SucMesAnio, int valor)
        {
            MatrizGestionViewModel metaIngreso = null;

            SqlDataReader dr = null;
            SqlTransaction tran = null;

            try
            {
                using (var conn = new SqlConnection(cadenaConexion.cadena))
                {
                    conn.Open();
                    tran = conn.BeginTransaction();
                    SqlCommand cmd = new SqlCommand("MATRIZGESTIONCON", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.CommandTimeout = 300; // Timeout en hardcode :( 

                    cmd.Parameters.AddWithValue("@Par_NumCon", valor);
                    cmd.Parameters.AddWithValue("@Par_Sucursal", "");
                    cmd.Parameters.AddWithValue("@Par_Anio", SucMesAnio.AnnEmp);
                    cmd.Parameters.AddWithValue("@Par_Mes", "");
                   

                    cmd.Transaction = tran;
                    try
                    {
                        dr = cmd.ExecuteReader();

                        metaIngreso = new MatrizGestionViewModel();
                        if (dr.Read())
                        {
                            metaIngreso.FolioCapPrest = decimal.Parse(dr["Folios"].ToString().Trim());
                            metaIngreso.PromedioSem = decimal.Parse(dr["PromedioSem"].ToString().Trim());

                        }
                        dr.Close();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return metaIngreso;
        }

        //Consulta de Matriz de indicadores de gestion --REAL (Porcentaje Maximo de enajenacion)
        public MatrizGestionViewModel ConsultaPorcMEA(ConexionSucursalViewModel cadenaConexion, MatrizGestionViewModel SucMesAnio, int valor)
        {
            MatrizGestionViewModel metaIngreso = null;

            SqlDataReader dr = null;
            SqlTransaction tran = null;

            try
            {
                using (var conn = new SqlConnection(cadenaConexion.cadena))
                {
                    conn.Open();
                    tran = conn.BeginTransaction();
                    SqlCommand cmd = new SqlCommand("MATRIZGESTIONCON", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandTimeout = 300;
                    cmd.Parameters.AddWithValue("@Par_NumCon", valor);
                    cmd.Parameters.AddWithValue("@Par_Sucursal", "");
                    cmd.Parameters.AddWithValue("@Par_Anio", SucMesAnio.AnnEmp);
                    cmd.Parameters.AddWithValue("@Par_Mes", "");
                    

                    cmd.Transaction = tran;
                    try
                    {
                        dr = cmd.ExecuteReader();

                        metaIngreso = new MatrizGestionViewModel();
                        if (dr.Read())
                        {

                            metaIngreso.Folios = decimal.Parse(dr["Folios"].ToString().Trim());

                        }
                        dr.Close();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return metaIngreso;
        }

        //Consulta de Matriz de indicadores de gestion --REAL (Cartera total)
        public MatrizGestionViewModel ConsultaCarteraTotalAnn(ConexionSucursalViewModel cadenaConexion, MatrizGestionViewModel SucMesAnio, int valorCarTotA)
        {
            MatrizGestionViewModel metaCartTotal = null;

            SqlDataReader dr = null;
            SqlTransaction tran = null;

            try
            {
                using (var conn = new SqlConnection(cadenaConexion.cadena))
                {
                    conn.Open();
                    tran = conn.BeginTransaction();
                    SqlCommand cmd = new SqlCommand("MATRIZGESTIONCON", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandTimeout = 300;
                    cmd.Parameters.AddWithValue("@Par_NumCon", valorCarTotA);
                    cmd.Parameters.AddWithValue("@Par_Sucursal", "");
                    cmd.Parameters.AddWithValue("@Par_Anio", SucMesAnio.AnnEmp);
                    cmd.Parameters.AddWithValue("@Par_Mes", "");

                    cmd.Transaction = tran;
                    try
                    {
                        dr = cmd.ExecuteReader();

                        metaCartTotal = new MatrizGestionViewModel();
                        if (dr.Read())
                        {

                            metaCartTotal.CarteraTotal = int.Parse(dr["CarteraTotal"].ToString().Trim());

                        }
                        dr.Close();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return metaCartTotal;
        }

        //Consulta de  Matriz de indicadores de gestion  --REAL  (CAPITAL PRESTADO)
        public MatrizGestionViewModel ConsultaCapPresAnn(ConexionSucursalViewModel cadenaConexion, MatrizGestionViewModel SucMesAnio, int valorCapPreA)
        {
            MatrizGestionViewModel metaCartera = null;

            SqlDataReader dr = null;
            SqlTransaction tran = null;

            try
            {
                
                using (var conn = new SqlConnection(cadenaConexion.cadena))
                {
                    conn.Open();
                    tran = conn.BeginTransaction();
                    SqlCommand cmd = new SqlCommand("MATRIZGESTIONCON", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandTimeout = 300;
                    cmd.Parameters.AddWithValue("@Par_NumCon", valorCapPreA);
                    cmd.Parameters.AddWithValue("@Par_Sucursal", "");
                    cmd.Parameters.AddWithValue("@Par_Anio", SucMesAnio.AnnEmp);
                    cmd.Parameters.AddWithValue("@Par_Mes", "");
                    
                    cmd.Transaction = tran;
                    try
                    {
                        dr = cmd.ExecuteReader();

                        metaCartera = new MatrizGestionViewModel();
                        if (dr.Read())
                        {
                            //aqui puse los archivos del ticket
                            metaCartera.Cartera = decimal.Parse(dr["Cartera"].ToString().Trim());
                            metaCartera.Producto = dr["Producto"].ToString().Trim();

                        }
                        dr.Close();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return metaCartera;
        }


        //Consulta de Matriz de indicadores de gestion --REAL (Cliente nuevo empeño y el Promedio de préstamo clientes nuevos)
        public MatrizGestionViewModel ConsultaCltNvoAnn(ConexionSucursalViewModel cadenaConexion, MatrizGestionViewModel SucMesAnio, int valorCTeNvoEA)
        {
            MatrizGestionViewModel metaCTNVO = null;

            SqlDataReader dr = null;
            SqlTransaction tran = null;

            try
            {
                using (var conn = new SqlConnection(cadenaConexion.cadena))
                {
                    conn.Open();
                    tran = conn.BeginTransaction();
                    SqlCommand cmd = new SqlCommand("MATRIZGESTIONCON", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandTimeout = 300;
                    cmd.Parameters.AddWithValue("@Par_NumCon", valorCTeNvoEA);
                    cmd.Parameters.AddWithValue("@Par_Sucursal", "");
                    cmd.Parameters.AddWithValue("@Par_Anio", SucMesAnio.AnnEmp);
                    cmd.Parameters.AddWithValue("@Par_Mes", "");

                   

                    cmd.Transaction = tran;
                    try
                    {
                        dr = cmd.ExecuteReader();

                        metaCTNVO = new MatrizGestionViewModel();
                        if (dr.Read())
                        {

                            metaCTNVO.TotalCteNvo = decimal.Parse(dr["TotalCteNvo"].ToString().Trim());
                            metaCTNVO.NumClientes = int.Parse(dr["NumClietes"].ToString().Trim());
                            //NumClietes

                        }
                        dr.Close();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return metaCTNVO;
        }

        //Consulta de Matriz de indicadores de gestion --REAL (Cliente nuevo ventas)
        public MatrizGestionViewModel ConsultaCltNvoVtaAnn(ConexionSucursalViewModel cadenaConexion, MatrizGestionViewModel SucMesAnio, int valorCTeNvoVA)
        {
            MatrizGestionViewModel metaCTNVOVta = null;

            SqlDataReader dr = null;
            SqlTransaction tran = null;

            try
            {
                using (var conn = new SqlConnection(BaseDAO.sicavibd))
                {
                    conn.Open();
                    tran = conn.BeginTransaction();
                    SqlCommand cmd = new SqlCommand("MATRIZGESTIONCON", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandTimeout = 300;
                    cmd.Parameters.AddWithValue("@Par_NumCon", valorCTeNvoVA);
                    cmd.Parameters.AddWithValue("@Par_Sucursal", cadenaConexion.sucursal);
                    cmd.Parameters.AddWithValue("@Par_Anio", SucMesAnio.AnnEmp);
                    cmd.Parameters.AddWithValue("@Par_Mes", "");

                    cmd.Transaction = tran;
                    try
                    {
                        dr = cmd.ExecuteReader();

                        metaCTNVOVta = new MatrizGestionViewModel();
                        if (dr.Read())
                        {

                            metaCTNVOVta.NumClientesVta = int.Parse(dr["NumClientes"].ToString().Trim());

                        }
                        dr.Close();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return metaCTNVOVta;
        }

        //Consulta de  Matriz de indicadores de gestion  --REAL  (INGRESO(EMPEÑOS))
        public MatrizGestionViewModel ConsultaIngEmpeñoAnn(ConexionSucursalViewModel cadenaConexion, MatrizGestionViewModel SucMesAnio, int valorIngEmA)
        {
            MatrizGestionViewModel metaIngreso = null;

            SqlDataReader dr = null;
            SqlTransaction tran = null;

            try
            {
                using (var conn = new SqlConnection(cadenaConexion.cadena))
                {
                    conn.Open();
                    tran = conn.BeginTransaction();
                    SqlCommand cmd = new SqlCommand("MATRIZGESTIONCON", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandTimeout = 300;
                    cmd.Parameters.AddWithValue("@Par_NumCon", valorIngEmA);
                    cmd.Parameters.AddWithValue("@Par_Sucursal", "");
                    cmd.Parameters.AddWithValue("@Par_Anio", SucMesAnio.AnnEmp);
                    cmd.Parameters.AddWithValue("@Par_Mes", "");

                    cmd.Transaction = tran;
                    try
                    {
                        dr = cmd.ExecuteReader();

                        metaIngreso = new MatrizGestionViewModel();
                        if (dr.Read())
                        {

                            metaIngreso.ingreso = decimal.Parse(dr["Ingreso"].ToString().Trim());

                        }
                        dr.Close();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return metaIngreso;
        }


    }
}
