﻿using DAO.Base;
using ENTIDADES.Operaciones;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO.Operaciones
{
    public class PromedioPrestamoListDAO
    {

        public List<PromedioPrestamoViewModel> ListaNomSucDAO(int valorLista)
        {
            List<PromedioPrestamoViewModel> listaPromedioPrestamo = new List<PromedioPrestamoViewModel>();
            PromedioPrestamoViewModel listaDao;

            SqlDataReader drd = null;

            using (var conn = new SqlConnection(BaseDAO.operacionesbd))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("PROMEDIOPRESTAMOLIS", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Par_NumLis", valorLista);
                    cmd.Parameters.AddWithValue("@Par_Sucursal", "");
                    cmd.Parameters.AddWithValue("@Par_Anio", "");
                    cmd.Parameters.AddWithValue("@Par_Mes", "");
                    cmd.Parameters.AddWithValue("@Par_Aliado", "");
                    cmd.Parameters.AddWithValue("@Par_TipoProd", "");
                    cmd.Parameters.AddWithValue("@Par_Producto", "");


                    drd = cmd.ExecuteReader();

                    while (drd.Read())
                    {
                        listaDao = new PromedioPrestamoViewModel()
                        {
                            NomSuc = drd["NomSuc"].ToString(),
                        };
                        listaPromedioPrestamo.Add(listaDao);
                    }
                    drd.Close();
                }

                catch (Exception ex)
                {
                    throw ex;
                }
                return listaPromedioPrestamo;
            }
        }



        public List<PromedioPrestamoViewModel> ListaAnioDAO(int valorLista, PromedioPrestamoViewModel promedio)
        {
            List<PromedioPrestamoViewModel> listaPromedioPrestamo = new List<PromedioPrestamoViewModel>();
            PromedioPrestamoViewModel listaDao;

            SqlDataReader drd = null;

            using (var conn = new SqlConnection(BaseDAO.operacionesbd))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("PROMEDIOPRESTAMOLIS", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Par_NumLis", valorLista);
                    cmd.Parameters.AddWithValue("@Par_Sucursal",promedio.NomSuc);  
                    cmd.Parameters.AddWithValue("@Par_Anio", "");
                    cmd.Parameters.AddWithValue("@Par_Mes", "");
                    cmd.Parameters.AddWithValue("@Par_Aliado", "");
                    cmd.Parameters.AddWithValue("@Par_TipoProd", "");
                    cmd.Parameters.AddWithValue("@Par_Producto", "");


                    drd = cmd.ExecuteReader();

                    while (drd.Read())
                    {
                        listaDao = new PromedioPrestamoViewModel()
                        {
                            AnnEmp = int.Parse(drd["AnnEmp"].ToString()),
                        };
                        listaPromedioPrestamo.Add(listaDao);
                    }
                    drd.Close();
                }

                catch (Exception ex)
                {
                    throw ex;
                }
                return listaPromedioPrestamo;
            }
        }



        public List<PromedioPrestamoViewModel> ListaMesDAO(int valorLista, PromedioPrestamoViewModel promedio)
        {
            List<PromedioPrestamoViewModel> listaPromedioPrestamo = new List<PromedioPrestamoViewModel>();
            PromedioPrestamoViewModel listaDao;

            SqlDataReader drd = null;

            using (var conn = new SqlConnection(BaseDAO.operacionesbd))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("PROMEDIOPRESTAMOLIS", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Par_NumLis", valorLista);
                    cmd.Parameters.AddWithValue("@Par_Sucursal", promedio.NomSuc);
                    cmd.Parameters.AddWithValue("@Par_Anio", promedio.AnnEmp);
                    cmd.Parameters.AddWithValue("@Par_Mes", "");
                    cmd.Parameters.AddWithValue("@Par_Aliado", "");
                    cmd.Parameters.AddWithValue("@Par_TipoProd", "");
                    cmd.Parameters.AddWithValue("@Par_Producto", "");


                    drd = cmd.ExecuteReader();

                    while (drd.Read())
                    {
                        listaDao = new PromedioPrestamoViewModel()
                        {

                            MesEmp = int.Parse(drd["MesEmp"].ToString()),
                            Mes = drd["Mes"].ToString(),
                        };
                        listaPromedioPrestamo.Add(listaDao);
                    }
                    drd.Close();
                }

                catch (Exception ex)
                {
                    throw ex;
                }
                return listaPromedioPrestamo;
            }
        }

        //Aliado 
        public List<PromedioPrestamoViewModel> ListaAliadoDAO(int valorLista, PromedioPrestamoViewModel promedio)
        {
            List<PromedioPrestamoViewModel> listaPromedioPrestamo = new List<PromedioPrestamoViewModel>();
            PromedioPrestamoViewModel listaDao;

            SqlDataReader drd = null;

            using (var conn = new SqlConnection(BaseDAO.operacionesbd))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("PROMEDIOPRESTAMOLIS", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Par_NumLis", valorLista);
                    cmd.Parameters.AddWithValue("@Par_Sucursal", promedio.NomSuc);                    
                    cmd.Parameters.AddWithValue("@Par_Anio", promedio.AnnEmp);
                    cmd.Parameters.AddWithValue("@Par_Mes", promedio.MesEmp);
                    cmd.Parameters.AddWithValue("@Par_Aliado", "");
                    cmd.Parameters.AddWithValue("@Par_TipoProd", "");
                    cmd.Parameters.AddWithValue("@Par_Producto", "");


                    drd = cmd.ExecuteReader();

                    while (drd.Read())
                    {
                        listaDao = new PromedioPrestamoViewModel()
                        {
                            NomUser = drd["NomUser"].ToString(),
                        };
                        listaPromedioPrestamo.Add(listaDao);
                    }
                    drd.Close();
                }

                catch (Exception ex)
                {
                    throw ex;
                }
                return listaPromedioPrestamo;
            }
        }

        //tipo
        public List<PromedioPrestamoViewModel> ListaTipoProductoDAO(int valorLista, PromedioPrestamoViewModel promedio)
        {
            List<PromedioPrestamoViewModel> listaPromedioPrestamo = new List<PromedioPrestamoViewModel>();
            PromedioPrestamoViewModel listaDao;

            SqlDataReader drd = null;

            using (var conn = new SqlConnection(BaseDAO.operacionesbd))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("PROMEDIOPRESTAMOLIS", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Par_NumLis", valorLista);
                    cmd.Parameters.AddWithValue("@Par_Sucursal", promedio.NomSuc);
                    cmd.Parameters.AddWithValue("@Par_Anio", promedio.AnnEmp);
                    cmd.Parameters.AddWithValue("@Par_Mes", promedio.MesEmp);
                    cmd.Parameters.AddWithValue("@Par_Aliado", "");
                    cmd.Parameters.AddWithValue("@Par_TipoProd", "");
                    cmd.Parameters.AddWithValue("@Par_Producto", "");


                    drd = cmd.ExecuteReader();

                    while (drd.Read())
                    {
                        listaDao = new PromedioPrestamoViewModel()
                        {
                            Tipo = drd["Tipo"].ToString(),
                        };
                        listaPromedioPrestamo.Add(listaDao);
                    }
                    drd.Close();
                }

                catch (Exception ex)
                {
                    throw ex;
                }
                return listaPromedioPrestamo;
            }
        }

        //producto
        public List<PromedioPrestamoViewModel> ListaProductoDAO(int valorLista, PromedioPrestamoViewModel promedio)
        {
            List<PromedioPrestamoViewModel> listaPromedioPrestamo = new List<PromedioPrestamoViewModel>();
            PromedioPrestamoViewModel listaDao;

            SqlDataReader drd = null;

            using (var conn = new SqlConnection(BaseDAO.operacionesbd))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("PROMEDIOPRESTAMOLIS", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Par_NumLis", valorLista);
                    cmd.Parameters.AddWithValue("@Par_Sucursal", promedio.NomSuc);
                    cmd.Parameters.AddWithValue("@Par_Anio", promedio.AnnEmp);
                    cmd.Parameters.AddWithValue("@Par_Mes", promedio.MesEmp);
                    cmd.Parameters.AddWithValue("@Par_Aliado", "");
                    cmd.Parameters.AddWithValue("@Par_TipoProd", "");
                    cmd.Parameters.AddWithValue("@Par_Producto", "");


                    drd = cmd.ExecuteReader();

                    while (drd.Read())
                    {
                        listaDao = new PromedioPrestamoViewModel()
                        {
                            Producto = drd["Producto"].ToString(),
                        };
                        listaPromedioPrestamo.Add(listaDao);
                    }
                    drd.Close();
                }

                catch (Exception ex)
                {
                    throw ex;
                }
                return listaPromedioPrestamo;
            }
        }


        //Promedio por año valor Lista 7
        public List<PromedioPrestamoViewModel> ListaPromAnioDAO(int valorLista, PromedioPrestamoViewModel promedio)
        {
            List<PromedioPrestamoViewModel> listaPromedioPrestamo = new List<PromedioPrestamoViewModel>();
            PromedioPrestamoViewModel listaDao;

            SqlDataReader drd = null;

            using (var conn = new SqlConnection(BaseDAO.operacionesbd))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("PROMEDIOPRESTAMOLIS", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Par_NumLis", valorLista);
                    cmd.Parameters.AddWithValue("@Par_Sucursal", promedio.NomSuc);
                    cmd.Parameters.AddWithValue("@Par_Anio", promedio.AnnEmp);
                    if (promedio.MesEmp.Equals(null))
                    {
                        cmd.Parameters.AddWithValue("@Par_Mes", "");                        
                    }else { cmd.Parameters.AddWithValue("@Par_Mes", promedio.MesEmp); }
                    
                    if (promedio.NomUser!= null)
                    {
                        cmd.Parameters.AddWithValue("@Par_Aliado", promedio.NomUser);                        
                    }else { cmd.Parameters.AddWithValue("@Par_Aliado", ""); }
                    
                    if (promedio.Tipo != null)
                    {
                        cmd.Parameters.AddWithValue("@Par_TipoProd", promedio.Tipo);
                    }else{
                        cmd.Parameters.AddWithValue("@Par_TipoProd", "");
                    }
                    
                    if (promedio.Producto != null)
                    {
                        cmd.Parameters.AddWithValue("@Par_Producto", promedio.Producto);                       
                    }else { cmd.Parameters.AddWithValue("@Par_Producto", ""); }
                    
                    drd = cmd.ExecuteReader();

                    while (drd.Read())
                    {
                        listaDao = new PromedioPrestamoViewModel()
                        {
                            AnnEmp = int.Parse(drd["AnnEmp"].ToString()),
                            Promedio = drd["Promedio"].ToString(),

                        };
                        listaPromedioPrestamo.Add(listaDao);
                    }
                    drd.Close();
                }

                catch (Exception ex)
                {
                    throw ex;
                }
                return listaPromedioPrestamo;
            }
        }


        //promedio por mes
        public List<PromedioPrestamoViewModel> ListaPromMesDAO(int valorLista, PromedioPrestamoViewModel promedio)
        {
            List<PromedioPrestamoViewModel> listaPromedioPrestamo = new List<PromedioPrestamoViewModel>();
            PromedioPrestamoViewModel listaDao;

            SqlDataReader drd = null;

            using (var conn = new SqlConnection(BaseDAO.operacionesbd))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("PROMEDIOPRESTAMOLIS", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Par_NumLis", valorLista);
                    cmd.Parameters.AddWithValue("@Par_Sucursal", promedio.NomSuc);
                    cmd.Parameters.AddWithValue("@Par_Anio", promedio.AnnEmp);
                    if (promedio.MesEmp.Equals(null))
                    {
                        cmd.Parameters.AddWithValue("@Par_Mes", "");
                    }
                    else { cmd.Parameters.AddWithValue("@Par_Mes", promedio.MesEmp); }

                    if (promedio.NomUser != null)
                    {
                        cmd.Parameters.AddWithValue("@Par_Aliado", promedio.NomUser);
                    }
                    else { cmd.Parameters.AddWithValue("@Par_Aliado", ""); }

                    if (promedio.Tipo != null)
                    {
                        cmd.Parameters.AddWithValue("@Par_TipoProd", promedio.Tipo);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@Par_TipoProd", "");
                    }

                    if (promedio.Producto != null)
                    {
                        cmd.Parameters.AddWithValue("@Par_Producto", promedio.Producto);
                    }
                    else { cmd.Parameters.AddWithValue("@Par_Producto", ""); }


                    drd = cmd.ExecuteReader();

                    while (drd.Read())
                    {
                        listaDao = new PromedioPrestamoViewModel()
                        {
                            AnnEmp = int.Parse(drd["AnnEmp"].ToString()),
                            Promedio = drd["Promedio"].ToString(),
                            MesEmp = int.Parse(drd["MesEmp"].ToString()),
                            Mes = drd["NomMes"].ToString(),
                            
                        };
                        listaPromedioPrestamo.Add(listaDao);
                    }
                    drd.Close();
                }

                catch (Exception ex)
                {
                    throw ex;
                }
                return listaPromedioPrestamo;
            }
        }

    }
}
