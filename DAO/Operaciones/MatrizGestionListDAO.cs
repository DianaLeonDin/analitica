﻿using DAO.Base;
using ENTIDADES.Login;
using ENTIDADES.Mantenimiento;
using ENTIDADES.Operaciones;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO.Operaciones
{
    public class MatrizGestionListDAO
    {

        //--------------------------****     MATRIZ DE INDICADOR DE GESTION (DIARIO)   ****----------------------//
        //Listado de las sucursales PARA CATALOGOS DE MANTENIMIENTO 
        public List<MatrizGestionViewModel> ListaSucursalesDAO(int valorLista)
        {
            List<MatrizGestionViewModel> listaPromedioPrestamo = new List<MatrizGestionViewModel>();
            MatrizGestionViewModel listaDao;

            SqlDataReader drd = null;

            using (var conn = new SqlConnection(BaseDAO.operacionesbd))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("MATRIZGESTIONLIS", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Par_NumLis", valorLista);
                    cmd.Parameters.AddWithValue("@Par_SucursalID", "");
                    cmd.Parameters.AddWithValue("@Par_Anio", "");
                    cmd.Parameters.AddWithValue("@Par_Mes", "");

                    drd = cmd.ExecuteReader();

                    while (drd.Read())
                    {
                        listaDao = new MatrizGestionViewModel()
                        {
                            AnnEmp = int.Parse(drd["AnnEmp"].ToString()),
                            NomSuc = drd["NomSuc"].ToString(),
                            CveSuc = drd["CveSuc"].ToString(),
                        };
                        listaPromedioPrestamo.Add(listaDao);
                    }
                    drd.Close();
                }

                catch (Exception ex)
                {
                    throw ex;
                }
                return listaPromedioPrestamo;
            }
        }

        // Lista de tipo producto, aliado, familia
        public List<MatrizGestionViewModel> ListadoDAO(int valorLista, MatrizGestionViewModel NomSuc)
        {
            List<MatrizGestionViewModel> lista = new List<MatrizGestionViewModel>();
            MatrizGestionViewModel listaDao;

            SqlDataReader drd = null;

            using (var conn = new SqlConnection(BaseDAO.operacionesbd))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("MATRIZGESTIONLIS", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Par_NumLis", valorLista);
                    if (NomSuc.NomSuc=="" | NomSuc.NomSuc == null)
                    {
                        cmd.Parameters.AddWithValue("@Par_SucursalID", "");
                    }else
                    {
                        cmd.Parameters.AddWithValue("@Par_SucursalID", NomSuc.NomSuc);
                    }
                    cmd.Parameters.AddWithValue("@Par_Anio", "");
                    cmd.Parameters.AddWithValue("@Par_Mes", "");

                    drd = cmd.ExecuteReader();

                    while (drd.Read())
                    {
                        listaDao = new MatrizGestionViewModel()
                        {
                            nombre = drd["Nombre"].ToString(),
                        };
                        lista.Add(listaDao);
                    }
                    drd.Close();
                }

                catch (Exception ex)
                {
                    throw ex;
                }
                return lista;
            }
        }

        //Listado del real para el ingreso de ventas, (Listado de aparatos, joyeria y celulares)
        public List<IngresoVentaViewModel> ListadoIngresoVentaDAO(int valorLista, ConexionSucursalViewModel cadenaConexion, MatrizGestionViewModel SucMesAnio)
         {
            List<IngresoVentaViewModel> listaIngresoVenta = new List<IngresoVentaViewModel>();
            IngresoVentaViewModel listaDao;

            SqlDataReader drd = null;

            // using (var conn = new SqlConnection(BaseDAO.analiticabd))
            using (var conn = new SqlConnection(BaseDAO.sicavibd))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("MATRIZGESTIONLIS", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Par_NumLis", valorLista);
                    cmd.Parameters.AddWithValue("@Par_SucursalID",cadenaConexion.sucursal );
                    cmd.Parameters.AddWithValue("@Par_AliadoID", "");
                    
                    if (!SucMesAnio.Anio.Equals(0) & !SucMesAnio.Mes.Equals(0))
                    {
                        cmd.Parameters.AddWithValue("@Par_Anio", SucMesAnio.Anio);
                        cmd.Parameters.AddWithValue("@Par_Mes", SucMesAnio.Mes);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@Par_Anio", "");
                        cmd.Parameters.AddWithValue("@Par_Mes", "");
                    }


                    drd = cmd.ExecuteReader();

                    while (drd.Read())
                    {
                        listaDao = new IngresoVentaViewModel()
                        {
                            Tipo = drd["Tipo"].ToString(),
                            Ingreso = Decimal.Parse(drd["Ingreso"].ToString()),
                        };
                        listaIngresoVenta.Add(listaDao);
                    }
                    drd.Close();
                }

                catch (Exception ex)
                {
                    throw ex;
                }
                return listaIngresoVenta;
            }
        }

        //Listado del real para la cartera, (Listado de aparatos, joyeria y celulares)
        public List<CarteraViewModel> ListadoCarteraDAO(int valorLista, ConexionSucursalViewModel cadenaConexion, MatrizGestionViewModel SucMesAnio)
        {
            List<CarteraViewModel> listaCartera = new List<CarteraViewModel>();
            CarteraViewModel listaDao;

            SqlDataReader drd = null;

            // using (var conn = new SqlConnection(BaseDAO.analiticabd))
            using (var conn = new SqlConnection(cadenaConexion.cadena))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("MATRIZGESTIONLIS", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandTimeout = 300;
                    cmd.Parameters.AddWithValue("@Par_NumLis", valorLista);
                    cmd.Parameters.AddWithValue("@Par_SucursalID", "");
                    cmd.Parameters.AddWithValue("@Par_AliadoID", "");
                    
                    if (!SucMesAnio.Anio.Equals(0) & !SucMesAnio.Mes.Equals(0))
                    {
                        cmd.Parameters.AddWithValue("@Par_Anio", SucMesAnio.Anio);
                        cmd.Parameters.AddWithValue("@Par_Mes", SucMesAnio.Mes);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@Par_Anio", "");
                        cmd.Parameters.AddWithValue("@Par_Mes", "");
                    }


                    drd = cmd.ExecuteReader();

                    while (drd.Read())
                    {
                        listaDao = new CarteraViewModel()
                        {
                            CapitalVigente = int.Parse(drd["CapitalVigente"].ToString()),
                            Tipo = drd["Tipo"].ToString(),
                        };
                        listaCartera.Add(listaDao);
                    }
                    drd.Close();
                }

                catch (Exception ex)
                {
                    throw ex;
                }
                return listaCartera;
            }
        }


        //--------------------------****     MATRIZ DE INDICADOR DE GESTION (MENSUAL)   ****----------------------//
        //Listado del real para la cartera, (Listado de aparatos, joyeria y celulares)
        public List<CarteraViewModel> ListadoPrestPromDAO(int valorLista, ConexionSucursalViewModel cadenaConexion, MatrizGestionViewModel NomSuc)
        {
            List<CarteraViewModel> listaCartera = new List<CarteraViewModel>();
            CarteraViewModel listaDao;

            SqlDataReader drd = null;

            // using (var conn = new SqlConnection(BaseDAO.analiticabd))
            using (var conn = new SqlConnection(cadenaConexion.cadena))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("MATRIZGESTIONLIS", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Par_NumLis", valorLista);
                    cmd.Parameters.AddWithValue("@Par_SucursalID", "");

                    drd = cmd.ExecuteReader();

                    while (drd.Read())
                    {
                        listaDao = new CarteraViewModel()
                        {
                            CapitalVigente = int.Parse(drd["CapitalVigente"].ToString()),
                            Tipo = drd["Tipo"].ToString(),
                        };
                        listaCartera.Add(listaDao);
                    }
                    drd.Close();
                }

                catch (Exception ex)
                {
                    throw ex;
                }
                return listaCartera;
            }
        }


        //--------------------------****     MATRIZ DE INDICADOR DE GESTION POR ALIADO    ****----------------------//
        //Listado de los adliados por la sucursal 

        public List<MatrizGestionViewModel> ListadoAliadoDAO(int valorLista, MatrizGestionViewModel NomSuc, ConexionSucursalViewModel cadenaConexion)
        {
            List<MatrizGestionViewModel> lista = new List<MatrizGestionViewModel>();
            MatrizGestionViewModel listaDao;

            SqlDataReader drd = null;

            //using (var conn = new SqlConnection(BaseDAO.dbdinbd))
            using (var conn = new SqlConnection(cadenaConexion.cadena))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("MATRIZGESTIONLIS", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandTimeout = 300;
                    cmd.Parameters.AddWithValue("@Par_NumLis", valorLista);
                    cmd.Parameters.AddWithValue("@Par_SucursalID", cadenaConexion.sucursal);
                    cmd.Parameters.AddWithValue("@Par_AliadoID", "");
                    //cmd.Parameters.AddWithValue("@Par_Anio", "");
                    //cmd.Parameters.AddWithValue("@Par_Mes", "");

                    if (!NomSuc.Anio.Equals(0) & !NomSuc.Mes.Equals(0))
                    {
                        cmd.Parameters.AddWithValue("@Par_Anio", NomSuc.Anio);
                        cmd.Parameters.AddWithValue("@Par_Mes", NomSuc.Mes);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@Par_Anio", "");
                        cmd.Parameters.AddWithValue("@Par_Mes", "");
                    }


                    drd = cmd.ExecuteReader();

                    while (drd.Read())
                    {
                        listaDao = new MatrizGestionViewModel()
                        {
                            Cusr = drd["CveUser"].ToString(),
                            nombre = drd["Nombre"].ToString(),
                            //puesto = drd["Puesto"].ToString(),
                        };
                        lista.Add(listaDao);
                    }
                    drd.Close();
                }

                catch (Exception ex)
                {
                    throw ex;
                }
                return lista;
            }
        }


        public List<MetaOperacionesViewModel> ListadoMetaAliadoDAO(int baseVal, MatrizGestionViewModel matriz, ConexionSucursalViewModel cadenaConexion)
        {
            List<MetaOperacionesViewModel> lista = new List<MetaOperacionesViewModel>();
            MetaOperacionesViewModel listaDao;

            SqlDataReader drd = null;

            //using (var conn = new SqlConnection(BaseDAO.operacionesbd))
            using (var conn = new SqlConnection(BaseDAO.analiticabd))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("MATRIZGESTIONLIS", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Par_NumLis", baseVal);
                    cmd.Parameters.AddWithValue("@Par_SucursalID", matriz.NomSuc);
                    cmd.Parameters.AddWithValue("@Par_AliadoID", matriz.Cusr);
                    cmd.Parameters.AddWithValue("@Par_Anio", "");
                    cmd.Parameters.AddWithValue("@Par_Mes", "");
                   


                    drd = cmd.ExecuteReader();

                    while (drd.Read())
                    {
                        listaDao = new MetaOperacionesViewModel()
                        {
                            NomIndicador = drd["NomIndicador"].ToString(),
                            meta = decimal.Parse(drd["ValBase"].ToString()),

                            //
                            //
                        };
                        lista.Add(listaDao);
                    }
                    drd.Close();
                }

                catch (Exception ex)
                {
                    throw ex;
                }
                return lista;
            }
        }


        public List<IngresoVentaViewModel> ListadoIngVtaAliadoDAO(int valorLista, ConexionSucursalViewModel cadenaConexion, MatrizGestionViewModel SucMesAnio)
        {
            List<IngresoVentaViewModel> listaIngresoVenta = new List<IngresoVentaViewModel>();
            IngresoVentaViewModel listaDao;

            SqlDataReader drd = null;

            // using (var conn = new SqlConnection(BaseDAO.analiticabd))
            using (var conn = new SqlConnection(BaseDAO.sicavibd))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("MATRIZGESTIONLIS", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Par_NumLis", valorLista);
                    cmd.Parameters.AddWithValue("@Par_SucursalID", cadenaConexion.sucursal);
                    cmd.Parameters.AddWithValue("@Par_AliadoID", SucMesAnio.Cusr);

                    if (!SucMesAnio.Anio.Equals(0) & !SucMesAnio.Mes.Equals(0))
                    {
                        cmd.Parameters.AddWithValue("@Par_Anio", SucMesAnio.Anio);
                        cmd.Parameters.AddWithValue("@Par_Mes", SucMesAnio.Mes);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@Par_Anio", "");
                        cmd.Parameters.AddWithValue("@Par_Mes", "");
                    }


                    drd = cmd.ExecuteReader();

                    while (drd.Read())
                    {
                        listaDao = new IngresoVentaViewModel()
                        {
                            Tipo = drd["Tipo"].ToString(),
                            Ingreso = Decimal.Parse(drd["Ingreso"].ToString()),
                        };
                        listaIngresoVenta.Add(listaDao);
                    }
                    drd.Close();
                }

                catch (Exception ex)
                {
                    throw ex;
                }
                return listaIngresoVenta;
            }
        }



        //--------------------------****     MATRIZ DE INDICADOR DE GESTION POR PRODUCTO   ****----------------------//
        //Listado de los tipos de producto por la sucursal 

        public List<MatrizGestionViewModel> ListadoTProductoDAO(int valorLista, MatrizGestionViewModel NomSuc, ConexionSucursalViewModel cadenaConexion)
        {
            List<MatrizGestionViewModel> lista = new List<MatrizGestionViewModel>();
            MatrizGestionViewModel listaDao;

            SqlDataReader drd = null;

            //using (var conn = new SqlConnection(BaseDAO.operacionesbd))
            using (var conn = new SqlConnection(cadenaConexion.cadena))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("MATRIZGESTIONLIS", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Par_NumLis", valorLista);

                    cmd.Parameters.AddWithValue("@Par_AliadoID", "");

                    
                    if (NomSuc.NomSuc == "" | NomSuc.NomSuc == null)
                    {
                        cmd.Parameters.AddWithValue("@Par_SucursalID", "");
                    }
                    else
                    {
                        //cmd.Parameters.AddWithValue("@Par_SucursalID", NomSuc.NomSuc);
                        cmd.Parameters.AddWithValue("@Par_SucursalID", NomSuc.CveSuc);
                    }
                    if (!NomSuc.Anio.Equals(0) & !NomSuc.Mes.Equals(0))
                    {
                        cmd.Parameters.AddWithValue("@Par_Anio", NomSuc.Anio);
                        cmd.Parameters.AddWithValue("@Par_Mes", NomSuc.Mes);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@Par_Anio", "");
                        cmd.Parameters.AddWithValue("@Par_Mes", "");
                    }


                    drd = cmd.ExecuteReader();

                    while (drd.Read())
                    {
                        listaDao = new MatrizGestionViewModel()
                        {
                            Cve = int.Parse(drd["CveTipProd"].ToString()),
                            nombre = drd["Nombre"].ToString(),
                        };
                        lista.Add(listaDao);
                    }
                    drd.Close();
                }

                catch (Exception ex)
                {
                    throw ex;
                }
                return lista;
            }
        }


        public List<IngresoVentaViewModel> ListadoIngVtaProductoDAO(int valorLista, ConexionSucursalViewModel cadenaConexion, MatrizGestionViewModel SucMesAnio)
        {
            List<IngresoVentaViewModel> listaIngresoVenta = new List<IngresoVentaViewModel>();
            IngresoVentaViewModel listaDao;

            SqlDataReader drd = null;

            // using (var conn = new SqlConnection(BaseDAO.analiticabd))
            using (var conn = new SqlConnection(BaseDAO.sicavibd))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("MATRIZGESTIONLIS", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Par_NumLis", valorLista);
                    cmd.Parameters.AddWithValue("@Par_SucursalID", cadenaConexion.sucursal);
                    cmd.Parameters.AddWithValue("@Par_AliadoID", SucMesAnio.Producto);

                    if (!SucMesAnio.Anio.Equals(0) & !SucMesAnio.Mes.Equals(0))
                    {
                        cmd.Parameters.AddWithValue("@Par_Anio", SucMesAnio.Anio);
                        cmd.Parameters.AddWithValue("@Par_Mes", SucMesAnio.Mes);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@Par_Anio", "");
                        cmd.Parameters.AddWithValue("@Par_Mes", "");
                    }


                    drd = cmd.ExecuteReader();

                    while (drd.Read())
                    {
                        listaDao = new IngresoVentaViewModel()
                        {
                            Tipo = drd["Tipo"].ToString(),
                            Ingreso = Decimal.Parse(drd["Ingreso"].ToString()),
                        };
                        listaIngresoVenta.Add(listaDao);
                    }
                    drd.Close();
                }

                catch (Exception ex)
                {
                    throw ex;
                }
                return listaIngresoVenta;
            }
        }



        //COMENTARIO PARA LOS INDICADORES DIARIOS DE OPERACIONES(INFORMACION)
        public List<ComentarioViewModel> ListadoComentariosDAO(int valorLista, ComentarioViewModel comentario)
        {
            List<ComentarioViewModel> listaComentario = new List<ComentarioViewModel>();
            ComentarioViewModel listaDao;

            SqlDataReader drd = null;

            // using (var conn = new SqlConnection(BaseDAO.analiticabd))
            using (var conn = new SqlConnection(BaseDAO.analiticabd))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("MANTOPERACIONESCON", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Par_NumCon", valorLista);
                    cmd.Parameters.AddWithValue("@Par_FolioID", 0);
                    cmd.Parameters.AddWithValue("@Par_SucursalID", comentario.SucursalID);
                    cmd.Parameters.AddWithValue("@Par_Nombre", comentario.Nombre);
                    cmd.Parameters.AddWithValue("@Par_Mes", "");
                    cmd.Parameters.AddWithValue("@Par_Anio", "");

                    drd = cmd.ExecuteReader();

                    while (drd.Read())
                    {
                        listaDao = new ComentarioViewModel()
                        {
                            Descripcion = drd["Descripcion"].ToString(),
                            Fecha = DateTime.Parse(drd["FechaAlta"].ToString()),
                        };
                        listaComentario.Add(listaDao);
                    }
                    drd.Close();
                }

                catch (Exception ex)
                {
                    throw ex;
                }
                return listaComentario;
            }
        }

        //--------------------------****     MATRIZ DE INDICADOR DE GESTION POR AÑO    ****----------------------//

        //LISTADO DE LOS AÑOS DE CADA UNA DE LAS SUCURSALES
        public List<MatrizGestionViewModel> ListadoAnioDAO(int valorLista, MatrizGestionViewModel NomSuc, ConexionSucursalViewModel cadenaConexion)
        {
            List<MatrizGestionViewModel> lista = new List<MatrizGestionViewModel>();
            MatrizGestionViewModel listaDao;

            SqlDataReader drd = null;

            //using (var conn = new SqlConnection(BaseDAO.dbdinbd))
            using (var conn = new SqlConnection(cadenaConexion.cadena))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("MATRIZGESTIONLIS", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandTimeout = 300;
                    cmd.Parameters.AddWithValue("@Par_NumLis", valorLista);
                    cmd.Parameters.AddWithValue("@Par_SucursalID", "");
                    cmd.Parameters.AddWithValue("@Par_AliadoID", "");
                    cmd.Parameters.AddWithValue("@Par_Anio", "");
                    cmd.Parameters.AddWithValue("@Par_Mes", "");
                    
                    drd = cmd.ExecuteReader();

                    while (drd.Read())
                    {
                        listaDao = new MatrizGestionViewModel()
                        {
                            CveSuc = drd["CveSuc"].ToString(),
                            AnnEmp = int.Parse(drd["AnnEmp"].ToString()),
                        };
                        lista.Add(listaDao);
                    }
                    drd.Close();
                }

                catch (Exception ex)
                {
                    throw ex;
                }
                return lista;
            }
        }

        //Listado del real para la cartera, (Listado de aparatos, joyeria y celulares)
        public List<CarteraViewModel> ListadoCarteraAnioDAO(int valorLista, ConexionSucursalViewModel cadenaConexion, MatrizGestionViewModel SucMesAnio)
        {
            List<CarteraViewModel> listaCartera = new List<CarteraViewModel>();
            CarteraViewModel listaDao;

            SqlDataReader drd = null;

            // using (var conn = new SqlConnection(BaseDAO.analiticabd))
            using (var conn = new SqlConnection(cadenaConexion.cadena))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("MATRIZGESTIONLIS", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandTimeout = 300;
                    cmd.Parameters.AddWithValue("@Par_NumLis", valorLista);
                    cmd.Parameters.AddWithValue("@Par_SucursalID", "");
                    cmd.Parameters.AddWithValue("@Par_AliadoID", "");
                    cmd.Parameters.AddWithValue("@Par_Anio", SucMesAnio.AnnEmp);
                    cmd.Parameters.AddWithValue("@Par_Mes", "");
                   
                    drd = cmd.ExecuteReader();

                    while (drd.Read())
                    {
                        listaDao = new CarteraViewModel()
                        {
                            CapitalVigente = int.Parse(drd["CapitalVigente"].ToString()),
                            Tipo = drd["Tipo"].ToString(),
                        };
                        listaCartera.Add(listaDao);
                    }
                    drd.Close();
                }

                catch (Exception ex)
                {
                    throw ex;
                }
                return listaCartera;
            }
        }

        //Listado del real para el ingreso de ventas, (Listado de aparatos, joyeria y celulares)
        public List<IngresoVentaViewModel> ListadoIngresoVtaAnioDAO(int valorLista, ConexionSucursalViewModel cadenaConexion, MatrizGestionViewModel SucMesAnio)
        {
            List<IngresoVentaViewModel> listaIngresoVenta = new List<IngresoVentaViewModel>();
            IngresoVentaViewModel listaDao;

            SqlDataReader drd = null;

            // using (var conn = new SqlConnection(BaseDAO.analiticabd))
            using (var conn = new SqlConnection(BaseDAO.sicavibd))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("MATRIZGESTIONLIS", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandTimeout = 300;
                    cmd.Parameters.AddWithValue("@Par_NumLis", valorLista);
                    cmd.Parameters.AddWithValue("@Par_SucursalID", cadenaConexion.sucursal);
                    cmd.Parameters.AddWithValue("@Par_AliadoID", "");
                    cmd.Parameters.AddWithValue("@Par_Anio", SucMesAnio.AnnEmp);
                    cmd.Parameters.AddWithValue("@Par_Mes", "");
                    drd = cmd.ExecuteReader();

                    while (drd.Read())
                    {
                        listaDao = new IngresoVentaViewModel()
                        {
                            Tipo = drd["Tipo"].ToString(),
                            Ingreso = Decimal.Parse(drd["Ingreso"].ToString()),
                        };
                        listaIngresoVenta.Add(listaDao);
                    }
                    drd.Close();
                }

                catch (Exception ex)
                {
                    throw ex;
                }
                return listaIngresoVenta;
            }
        }

        public List<MetaOperacionesViewModel> ListadoMetaAnioDAO(int baseVal, MatrizGestionViewModel matriz)
        {
            List<MetaOperacionesViewModel> lista = new List<MetaOperacionesViewModel>();
            MetaOperacionesViewModel listaDao;

            SqlDataReader drd = null;

            //using (var conn = new SqlConnection(BaseDAO.operacionesbd))
            using (var conn = new SqlConnection(BaseDAO.analiticabd))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("MATRIZGESTIONLIS", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Par_NumLis", baseVal);
                    cmd.Parameters.AddWithValue("@Par_SucursalID", matriz.NomSuc);
                    cmd.Parameters.AddWithValue("@Par_AliadoID","");
                    cmd.Parameters.AddWithValue("@Par_Anio", matriz.AnnEmp);
                    cmd.Parameters.AddWithValue("@Par_Mes", "");



                    drd = cmd.ExecuteReader();

                    while (drd.Read())
                    {
                        listaDao = new MetaOperacionesViewModel()
                        {
                            NomIndicador = drd["NomIndicador"].ToString(),
                            meta = decimal.Parse(drd["ValBase"].ToString()),

                            //
                            //
                        };
                        lista.Add(listaDao);
                    }
                    drd.Close();
                }

                catch (Exception ex)
                {
                    throw ex;
                }
                return lista;
            }
        }

        //Listado de las sucursales PARA CATALOGOS DE MANTENIMIENTO 
        public List<MatrizGestionViewModel> ListaSucursalesIND(int valorLista, UsuarioViewModel usuarioResponse)
        {
            List<MatrizGestionViewModel> listaPromedioPrestamo = new List<MatrizGestionViewModel>();
            MatrizGestionViewModel listaDao;

            SqlDataReader drd = null;

            using (var conn = new SqlConnection(BaseDAO.analiticabd))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("MATRIZGESTIONLIS", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Par_NumLis", valorLista);
                    cmd.Parameters.AddWithValue("@Par_SucursalID","");
                    cmd.Parameters.AddWithValue("@Par_AliadoID", usuarioResponse.CodigoAlterno);
                    cmd.Parameters.AddWithValue("@Par_Anio", "");
                    cmd.Parameters.AddWithValue("@Par_Mes", "");

                    drd = cmd.ExecuteReader();

                    while (drd.Read())
                    {
                        listaDao = new MatrizGestionViewModel()
                        {
                            
                            NomSuc = drd["Sucursal"].ToString(),
                            CveSuc = drd["SucursalID"].ToString(),
                        };
                        listaPromedioPrestamo.Add(listaDao);
                    }
                    drd.Close();
                }

                catch (Exception ex)
                {
                    throw ex;
                }
                return listaPromedioPrestamo;
            }
        }



    }
}
