﻿using DAO.Base;
using ENTIDADES.Base;
using ENTIDADES.Login;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO.Login
{
    public class BitacoraAccesoAltDAO
    {

        //ALTA DE BITACORA DE ACCESO
        public MensajeTransaccion AltaBitacoraAcceso(UsuarioViewModel usuarioResponse)
        {
            MensajeTransaccion mensaje = null;
            SqlDataReader dr = null;
            SqlTransaction tran = null;

            try
            {
                using (var conn = new SqlConnection(BaseDAO.analiticabd))
                {
                    conn.Open();

                    tran = conn.BeginTransaction();
                    SqlCommand cmd = new SqlCommand("BITACORAACCESOALT", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Par_Usr", usuarioResponse.CodigoAlterno);
                    //cmd.Parameters.AddWithValue("@Par_Fecha", '');
                    cmd.Parameters.AddWithValue("@Par_SucursalID", usuarioResponse.CveSucursal);

                    cmd.Parameters.AddWithValue("@Par_Salida", "S");
                    cmd.Parameters.AddWithValue("@Par_NumErr", "");
                    cmd.Parameters.AddWithValue("@Par_ErrMenDev", "");
                    // cmd.Parameters.AddWithValue("@Par_Consecutivo", "");
                    cmd.Transaction = tran;

                    try
                    {
                        dr = cmd.ExecuteReader();
                        while (dr.Read())
                        {
                            mensaje = new MensajeTransaccion();
                            mensaje.CodigoRespuesta = dr["NumErr"].ToString();
                            mensaje.MensajeRespuesta = dr["ErrMenDev"].ToString();
                            //   mensaje.Consecutivo = dr["Consecutivo"].ToString();
                        }
                        dr.Close();
                        if (!mensaje.CodigoRespuesta.Equals("000000"))
                        {
                            throw new Exception();
                        }
                        tran.Commit();
                    }
                    catch (Exception ex)
                    {
                        tran.Rollback();
                        if (mensaje == null)
                        {
                            mensaje = new MensajeTransaccion();
                            mensaje.CodigoRespuesta = "CD-000001";
                            mensaje.MensajeRespuesta = "-CAPA DAO- \n OCURRIÓ UN ERROR AL CREAR LA BITACORA DE ACCESO, EN EL PROCESO " +
                                " BITACORAACCESOALT \n Error técnico: " + ex.Message;
                        }
                    }

                }
            }
            catch (Exception)
            {
                throw;
            }
            return mensaje;
        }


    }
}
