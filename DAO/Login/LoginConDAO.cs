﻿
using ENTIDADES.Login;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO.Login
{
    public class LoginConDAO
    {
        public UsuarioViewModel ConsultaUsr(UsuarioViewModel usuarioResponse)
        {
            UsuarioViewModel login = null;

            SqlDataReader dr = null;
            SqlTransaction tran = null;
            try
            {


                using (var conn = new SqlConnection(Base.BaseDAO.dbdinbd))
                {
                    conn.Open();

                    tran = conn.BeginTransaction();
                    SqlCommand cmd = new SqlCommand("USRINDCON", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Par_Usr", usuarioResponse.CodigoAlterno);
                    cmd.Parameters.AddWithValue("@Par_Password", usuarioResponse.Password);
                    cmd.Parameters.AddWithValue("@Par_numCon", 1);
                    cmd.Transaction = tran;
                    try
                    {
                        dr = cmd.ExecuteReader();

                        login = new UsuarioViewModel();
                        if (dr.Read())
                        {
                            // ,   P.
                            //aqui puse los archivos del ticket
                            login.CveEmpleado = dr["Em_Cve_Empleado"].ToString().Trim();
                            login.CodigoAlterno = dr["Em_UserDef_1"].ToString().Trim();
                            login.Password = dr["Em_Password"].ToString().Trim();
                            login.NombreEmp = dr["Em_Nombre"].ToString().Trim();
                            login.CvePuestoEmpleado = dr["Pe_Cve_Puesto_Empleado"].ToString().Trim();
                            login.NomPuestoEmp = dr["Pe_Descripcion"].ToString().Trim();
                        }
                        dr.Close();
                        //return empleado;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return login;
        }
    }
}
