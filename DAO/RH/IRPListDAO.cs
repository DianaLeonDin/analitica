﻿using DAO.Base;
using ENTIDADES.RH;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO.RH
{
    public class IRPListDAO
    {
        //Indicador Rotacion de Personal(IRP)
        public List<IRPViewModel> ListaIRPDAO(int valorLista)
        {
            List<IRPViewModel> listaIRP = new List<IRPViewModel>();
            IRPViewModel listaDao;

            SqlDataReader drd = null;

            using (var conn = new SqlConnection(BaseDAO.rhbd))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("IRPLIS", conn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Par_NumLis", valorLista);

                    drd = cmd.ExecuteReader();

                    while (drd.Read())
                    {
                        listaDao = new IRPViewModel()
                        {
                            Ann = int.Parse(drd["Ann"].ToString()),
                            //Ene = drd["Ene"].ToString(),
                            //Feb = drd["Feb"].ToString(),
                            //Mar = drd["Mar"].ToString(),
                            //Abr = drd["Abr"].ToString(),
                            //May = drd["May"].ToString(),
                            //Jun = drd["Jun"].ToString(),
                            //Jul = drd["Jul"].ToString(),
                            //Ago = drd["Ago"].ToString(),
                            //Sep = drd["Sep"].ToString(),
                            //Oct = drd["Oct"].ToString(),
                            //Nov = drd["Nov"].ToString(),
                            //Dic = decimal.Parse(drd["Dic"].ToString()),

                            Ene = decimal.Parse(drd["Ene"].ToString()),
                            Feb = decimal.Parse(drd["Feb"].ToString()),
                            Mar = decimal.Parse(drd["Mar"].ToString()),
                            Abr = decimal.Parse(drd["Abr"].ToString()),
                            May = decimal.Parse(drd["May"].ToString()),
                            Jun = decimal.Parse(drd["Jun"].ToString()),
                            Jul = decimal.Parse(drd["Jul"].ToString()),
                            Ago = decimal.Parse(drd["Ago"].ToString()),
                            Sep = decimal.Parse(drd["Sep"].ToString()),
                            Oct = decimal.Parse(drd["Oct"].ToString()),
                            Nov = decimal.Parse(drd["Nov"].ToString()),
                            Dic = decimal.Parse(drd["Dic"].ToString()),
                        };
                        listaIRP.Add(listaDao);
                    }
                    drd.Close();
                }

                catch (Exception ex)
                {
                    throw ex;
                }
                return listaIRP;
            }
        }



    }
}
