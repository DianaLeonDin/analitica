﻿Public Class Crypto

    Private LCW As Integer                 'Length of CodeWord
    Private LS2E As Integer                 'Length of String to be Encrypted
    Private LAM As Integer                 'Length of Array Matrix
    Private MP As Integer                    'Matrix Position
    Private Matrix As String                  'Starting Matrix
    Private mov1 As String                    'First Part of Replacement String
    Private mov2 As String                    'Second Part of Replacement String
    Private CodeWord As String            'CodeWord
    Private CWL As String                    'CodeWord Letter
    Private EncryptedString As String     'String to Return for Encrypt or String to UnEncrypt for UnEncrypt
    Private EncryptedLetter As String     'Storage Variable for Character just Encrypted
    Private strCryptMatrix(89) As String 'Matrix Array


    Public Property KeyString()
        Get
            Return CodeWord
        End Get
        Set(ByVal Value)
            CodeWord = Value
        End Set
    End Property


    Private Function Encrypt(ByVal mstext As String) As String
        Dim X As Integer                    ' Loop Counter
        Dim Y As Integer                    'Loop Counter
        Dim Z As Integer                     'Loop Counter
        Dim C2E As String                   'Character to Encrypt
        Dim Str2Encrypt As String        'Text from TextBox

        Str2Encrypt = mstext
        LS2E = Len(mstext)
        LCW = Len(CodeWord)
        EncryptedLetter = ""
        EncryptedString = ""

        Y = 1
        For X = 1 To LS2E
            C2E = Mid(Str2Encrypt, X, 1)
            MP = InStr(1, Matrix, C2E, 0)
            CWL = Mid(CodeWord, Y, 1)
            For Z = 1 To LAM
                If Mid(strCryptMatrix(Z), MP, 1) = CWL Then
                    EncryptedLetter = Left(strCryptMatrix(Z), 1)
                    EncryptedString = EncryptedString + EncryptedLetter
                    Exit For
                End If
            Next Z
            Y = Y + 1
            If Y > LCW Then Y = 1
        Next X
        Encrypt = EncryptedString

    End Function

    Public Function Encriptar(ByVal Valor As String)
        Encriptar = Encrypt(Valor)
    End Function

    Public Function Desencriptar(ByVal Valor As String)
        Desencriptar = Encrypt(Valor)
    End Function

    Public Sub New()
        Dim W As Integer 'Loop Counter to set up Matrix
        Dim X As Integer     'Loop through Matrix
        '         123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
        Matrix = "8x3p5BeabcdfghijklmnoqrstuvwyzACDEFGHIJKLMNOPQRSTUVWXYZ 1246790-.#/\!@$<>&*()[]{};:,?=+%_"
        CodeWord = "CrIpToKeY"

        W = 1
        LAM = Len(Matrix)
        strCryptMatrix(1) = Matrix

        For X = 2 To LAM ' LAM = Length of Array Matrix
            mov1 = Left(strCryptMatrix(W), 1)   'First Character of strCryptMatrix
            mov2 = Right(strCryptMatrix(W), (LAM - 1))   'All but First Character of strCryptMatrix
            strCryptMatrix(X) = mov2 + mov1  'Makes up each row of the Array
            W = W + 1
        Next X

    End Sub








End Class
