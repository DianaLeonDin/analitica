﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilidades.Utilidades
{
    public class Constantes
    {
        public static String[] CodigoExito = { "000000", "Operación Realizada con Éxito!" };

        public static String[] CodigoErrorDAO = { "DAO-000001", "Error! - Ocurrió un problema en la capa de datos" };

        public static String[] CodigoErrorServicio = { "SERV-000002", "Error! - Ocurrió un problema en la capa de servicio" };
    }
}
