﻿using ENTIDADES.Operaciones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAO.Operaciones;

namespace SERVICIO.Operaciones
{
    public class PromedioPrestamoServicio
    {
        PromedioPrestamoListDAO ListadoPromPrestamo = new PromedioPrestamoListDAO();


        public enum ListaPromedioPrestamo
        {
            List_Suc = 1,
            List_Anio = 2,
            List_Mes = 3,
            List_Aliado = 4,
            List_TipoProducto = 5,
            List_Producto= 6,
            List_PromedioAnio = 7,
            List_PromedioMes = 8,
            List_Aliados = 9,//Liatdo de todos los aliados
            List_Productos = 10,//Listado de todos los productos
        }

        //nombre sucursal
        public List<PromedioPrestamoViewModel> ListarNomSucServicio(int valorLista)
        {
            List<PromedioPrestamoViewModel> listaServicio = null;
            try
            {
                listaServicio = ListadoPromPrestamo.ListaNomSucDAO(valorLista);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return listaServicio;
        }
        //año
        public List<PromedioPrestamoViewModel> ListaranioServicio(int valorLista, PromedioPrestamoViewModel promedio)
        {
            List<PromedioPrestamoViewModel> listaServicio = null;
            try
            {
                listaServicio = ListadoPromPrestamo.ListaAnioDAO(valorLista, promedio);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return listaServicio;
        }

        public List<PromedioPrestamoViewModel> ListarMesServicio(int valorLista, PromedioPrestamoViewModel promedio)
        {
            List<PromedioPrestamoViewModel> listaServicio = null;
            try
            {
                listaServicio = ListadoPromPrestamo.ListaMesDAO(valorLista, promedio);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return listaServicio;
        }

        public List<PromedioPrestamoViewModel> ListarAliadoServicio(int valorLista, PromedioPrestamoViewModel promedio)
        {
            List<PromedioPrestamoViewModel> listaServicio = null;
            try
            {
                listaServicio = ListadoPromPrestamo.ListaAliadoDAO(valorLista, promedio);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return listaServicio;
        }

        public List<PromedioPrestamoViewModel> ListarTipoProductoServicio(int valorLista, PromedioPrestamoViewModel promedio)
        {
            List<PromedioPrestamoViewModel> listaServicio = null;
            try
            {
                listaServicio = ListadoPromPrestamo.ListaTipoProductoDAO(valorLista, promedio);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return listaServicio;
        }

        public List<PromedioPrestamoViewModel> ListarProductoServicio(int valorLista, PromedioPrestamoViewModel promedio)
        {
            List<PromedioPrestamoViewModel> listaServicio = null;
            try
            {
                listaServicio = ListadoPromPrestamo.ListaProductoDAO(valorLista, promedio);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return listaServicio;
        }


        public List<PromedioPrestamoViewModel> ListarPromedioAnioServicio(int valorLista, PromedioPrestamoViewModel promedio)
        {
            List<PromedioPrestamoViewModel> listaServicio = null;
            try
            {
                listaServicio = ListadoPromPrestamo.ListaPromAnioDAO(valorLista, promedio);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return listaServicio;
        }

        public List<PromedioPrestamoViewModel> ListarPromedioMesServicio(int valorLista, PromedioPrestamoViewModel promedio)
        {
            List<PromedioPrestamoViewModel> listaServicio = null;
            try
            {
                listaServicio = ListadoPromPrestamo.ListaPromMesDAO(valorLista, promedio);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return listaServicio;
        }


    }
}
