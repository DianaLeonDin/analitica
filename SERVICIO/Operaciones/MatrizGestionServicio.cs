﻿using ENTIDADES.Base;
using ENTIDADES.Login;
using ENTIDADES.Operaciones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERVICIO.Operaciones
{
    public class MatrizGestionServicio
    {
        DAO.Operaciones.MatrizGestionListDAO MatizGestionlis = new DAO.Operaciones.MatrizGestionListDAO();
        DAO.Operaciones.MatrizGestionConDAO MatrizGestionCon = new DAO.Operaciones.MatrizGestionConDAO();
        DAO.Operaciones.MatrizGestionAltDAO MatrizGestionAlt = new DAO.Operaciones.MatrizGestionAltDAO();

        public enum ListaMatrizGestion
        {
            Lista_Suc = 1,//Lista de las sucusales
            Lista_TipoProducto = 4,
            Lista_Aliado = 5,
            Lista_Familia = 6,
            Lista_IngresoVta = 8,//Listado de ingresos de ventas(Apartados, celulares y joyeria)
            Lista_Cartera = 9,//Listado de la cartera (Apartados, celulares y joyeria)
            Lista_AliadoXSuc = 10,//Listado de lalos aliados por las sucursales
            Lista_Comentarios= 5,//Comentarios(Estrategias) de cada uno de los indicadores, 
            Lista_SucInd = 44,//Lista de las sucusales paa la pantalla principal de indicadores diarios
        }
        public enum ConsultaMatrizGestion
        {
            // Lista_MetaCartera = 1,//Lista de las sucusales
            Con_MetaCartera = 1,//CAptital prestado
            Con_MetaIngreso = 2,//Consulta del real de la meta de ingreso
            Con_ClienteNvo = 3,//Clientes nuevos
            Con_PromMontoEmp = 4,// Promedio monto empeño
            Con_PorcMaxEna = 5,// Porcentaje maximo enajenacion
            Con_DiaHabil = 6,// Dias habiles
            Con_IngTotal = 7,// Ingresos totales
            // Real por indicador de aliado
            con_Ali_PromEm = 8,//Real del indicador de promedio empeños
            Con_CarTotal = 8, //Cartera total del indicador diario
            Con_ClienteNvoVta = 50,//Clientes nuevos ventas
        }
        
        //ñLISTA Y CONSULTA DEL REAL POR MES Y AÑO ANTERIOR DEL MES AnUAL
        public enum ListaMatrizGestionMes
        {
            Lista_Carteras = 17,//Listado de la cartera (Apartados, celulares y joyeria)
            Lista_IVenta = 18,//Listado de ingresos de ventas(Apartados, celulares y joyeria)
        }

        public enum ConsultaMatrizGestionMes
        {
            // Lista_MetaCartera = 1,//Lista de las sucusales
            Con_PMEmpenio = 10,// Promedio monto empeño
            Con_PMEnajenacion = 11,// Porcentaje maximo enajenacion
            Con_MCartera = 12,//CAptital prestado
            Con_CNvo = 13,//Clientes nuevos
            Con_MIngreso = 14,//Consulta del real de la meta de ingreso
            Con_CartTot = 9,//Consulta la cartera total del mes anterior de los indicadores diario  
            Con_CteNvoVta= 51,
        }
        
        //LISTADO Y CONSULTA  DE LOS INDICADORES POR ALIADO
        public enum ListaMatrizGestionAliado
        {
            Lista_Aliado = 20,//Listado de los aliados que se filtran al seleccionar la sucursal
            Lista_Meta = 21,
            Lista_MetaMA = 22,//Meta del mes anterior
            Lista_IngAliado = 23,//Ingreso de aparatos , celulares y joyeria Y TOTAL
            Lista_IngAliadoMA = 24,//Ingreso de aparatos , celulares y joyeria Y TOTAL
            Lista_AliadoMA = 25,//Listado de aliados del mes anterior 
        }
        public enum ConsultaMatrizGestionAliado
        {
            Con_PPAliado = 20,// Prestano promedio
            Con_MCAliado = 21,//Mata cartera
            Con_CNAliado = 22,//Clientes nuevos
            Con_MIAliado = 26,//Meta ingreso (empeño)

            /////////MES ANTERIOR DE INDICADORES POR AILADO
            Con_PPAliadoMA = 23,// Prestano promedio
            Con_MCAliadoMA = 24,//Mata cartera
            Con_CNAliadoMA = 25,//Clientes nuevos
            Con_IEAliadoMA = 27,//-REAL Meta ingresos (empeño)---(INGRESO (EMPEÑO)) 

        }

        //LISTADO Y CONSULTA  DE LOS INDICADORES POR ALIADO
        public enum ListaMatrizGestionProducto
        {
            //MES ACTUAL
            Lista_Producto = 20,//Listado de los aliados que se filtran al seleccionar la sucursal
            Lista_IngProducto = 30,//Listado de los ingresos (apartados, celulares, joyeria y total)
            //MES ANTERIOR
            Lista_IngProductoMA = 31, //Listado de los ingresos (apartados, celulares, joyeria y total)

        }
        public enum ConsultaMatrizGestionProducto
        {
            Con_PPProducto = 30,// Prestano promedio
            Con_MCProducto = 31,//Mata cartera
            Con_CNProducto = 32,//Clientes nuevos
            Con_MIProducto = 33,//Meta ingreso(Empeo)
            //MES ANTERIOR 
            Con_PPProductoMA = 40, //Prestamo promedio del mes anterior
            Con_CPProductoMA = 41,
            Con_CNProductoMA = 42,
            Con_MIProductoMA = 43,
        }
        //LISTADO DE INDICADORES ANUALES
        public enum ListaAnual
        {
            ListaAnio= 40,
            ListaMeta = 43,
        }
        public enum ConsultaAnual
        {
            Con_PresProm = 60, //Prestamo promedio
            Con_PorcMaxE = 61, //Porcentaje maximo enajenacion
            ListCartera= 41,//Listado de cartera aparato, celular y joyeria
            Con_CapPres = 62,
            con_CteNvoEM= 63,
            con_CteNvoVM = 64,
            Con_CartTot = 65,//Cartera total
            ListIngVta = 42, //Listado de los ingresos de ventas aparatos, celulares y joyeria
            Con_IngEmp = 66, //Ingreso (empeños)
        }


        //Lista de las sucursales
        public List<MatrizGestionViewModel> ListarSucursalServicio(int valorLista)
        {
            List<MatrizGestionViewModel> listaServicio = null;
            try
            {
                listaServicio = MatizGestionlis.ListaSucursalesDAO(valorLista);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return listaServicio;
        }

        //Consulta de las mestas de cartera
        public MatrizGestionViewModel ConsultaMetaCartera(int valor, ConexionSucursalViewModel cadenaConexion)
        {
            MatrizGestionViewModel metas = null;
            MatrizGestionViewModel vacio = new MatrizGestionViewModel();
            try
            {
                metas = MatrizGestionCon.ConsultaMetaCartera(valor, cadenaConexion, vacio);
            }
            catch
            {
                throw;
            }
            return metas;
        }
        
        //Lista de los tipos de producto, aliados y familia
        public List<MatrizGestionViewModel> ListadoServicio(int valorLista, MatrizGestionViewModel NomSuc)
        {
            List<MatrizGestionViewModel> listaServicio = null;
            try
            {
                listaServicio = MatizGestionlis.ListadoDAO(valorLista, NomSuc);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return listaServicio;
        }

        //Consulta de las mestas de ingreso
        public MatrizGestionViewModel ConsultaMetaIngreso(int valor, ConexionSucursalViewModel cadenaConexion)
        {
            MatrizGestionViewModel metas = null;
            MatrizGestionViewModel vacio = new MatrizGestionViewModel();
            try
            {
                metas = MatrizGestionCon.ConsultaMetaIngreso(valor, cadenaConexion, vacio);
            }
            catch
            {
                throw;
            }
            return metas;
        }

        //Consulta de los clientes nuevos empeños (REAL)
        public MatrizGestionViewModel ConsultaCteNvo(int valor, ConexionSucursalViewModel cadenaConexion)
        {
            MatrizGestionViewModel metas = null;
            MatrizGestionViewModel vacio = new MatrizGestionViewModel();
            try
            {
                metas = MatrizGestionCon.ConsultaCltNvo(valor, cadenaConexion, vacio);
            }
            catch
            {
                throw;
            }
            return metas;
        }

        //Consulta de los clientes nuevos ventas (REAL)
        public MatrizGestionViewModel ConsultaCteNvoVta(int valor, ConexionSucursalViewModel cadenaConexion)
        {
            MatrizGestionViewModel metas = null;
            MatrizGestionViewModel vacio = new MatrizGestionViewModel();
            try
            {
                metas = MatrizGestionCon.ConsultaCltNvoVta(valor, cadenaConexion, vacio);
            }
            catch
            {
                throw;
            }
            return metas;
        }

        //Consulta de los Promedio monto empeño (REAL)
        public MatrizGestionViewModel ConsultaPromedioMontoEmpeño(int valor, ConexionSucursalViewModel cadenaConexion)
        {
            MatrizGestionViewModel metas = null;
            MatrizGestionViewModel vacio = new MatrizGestionViewModel();
            try
            {
                metas = MatrizGestionCon.ConsultaPromMontoEMpeño(valor, cadenaConexion, vacio);
            }
            catch
            {
                throw;
            }
            return metas;
        }

        //Consulta de los Porcentaje maximo de enajenacion (REAL)
        public MatrizGestionViewModel ConsultaPorcMaxEna(int valor, ConexionSucursalViewModel cadenaConexion)
        {
            MatrizGestionViewModel metas = null;
            MatrizGestionViewModel vacio = new MatrizGestionViewModel();
            try
            {
                metas = MatrizGestionCon.ConsultaPorcMaxEna(valor, cadenaConexion, vacio);
            }
            catch
            {
                throw;
            }
            return metas;
        }

        //Listado de ingreso de venta, para apartados, celulares y joyeria
        public MatrizGestionViewModel ListarIngresoVtaServicio(int valor, ConexionSucursalViewModel cadenaConexion)
        {
            MatrizGestionViewModel listaIngVta = null;
            MatrizGestionViewModel vacio = new MatrizGestionViewModel();
            try
            {
                listaIngVta = new MatrizGestionViewModel();
                listaIngVta.IngresoVenta = MatizGestionlis.ListadoIngresoVentaDAO(valor, cadenaConexion, vacio);

            }
            catch (Exception)
            {
                throw;
            }
            return listaIngVta;
        }
        
        public BusquedaViewModel ListarCombo()
        {
            BusquedaViewModel busqueda = new BusquedaViewModel();
            List<MatrizGestionViewModel> matriz = new List<MatrizGestionViewModel>();

            MatrizGestionViewModel matrizModel = new MatrizGestionViewModel();
            try
            {
                var tipoProd = (int)MatrizGestionServicio.ListaMatrizGestion.Lista_TipoProducto;
                var Familia = (int)MatrizGestionServicio.ListaMatrizGestion.Lista_Familia;
                var aliado = (int)MatrizGestionServicio.ListaMatrizGestion.Lista_Aliado;

                busqueda.BusquedaTipoProducto = MatizGestionlis.ListadoDAO(tipoProd, matrizModel);
                busqueda.BusquedasFamilia = MatizGestionlis.ListadoDAO(Familia, matrizModel);
                busqueda.BusquedasAliado = MatizGestionlis.ListadoDAO(aliado, matrizModel);
                //MatizGestionlis.ListadoDAO(valorLista);
            }
            catch
            {
                throw;
            }
            return busqueda;
        }

        //Consulta de los ingresos totales (REAL)
        public MatrizGestionViewModel ConsultaIngTotal(int valor, ConexionSucursalViewModel cadenaConexion)
        {
            MatrizGestionViewModel metas = null;
            try
            {
                metas = MatrizGestionCon.ConsultaIngTotal(valor, cadenaConexion);
            }
            catch
            {
                throw;
            }
            return metas;
        }
        
        //Listado de cartera para apartados, celulares y joyeria
        public MatrizGestionViewModel ListarCarteraServicio(int valor, ConexionSucursalViewModel cadenaConexion)
        {
            MatrizGestionViewModel listaCartera = null;
            MatrizGestionViewModel vacio = new MatrizGestionViewModel();
            try
            {
                listaCartera = new MatrizGestionViewModel();
                listaCartera.CarteraView = MatizGestionlis.ListadoCarteraDAO(valor, cadenaConexion, vacio);

            }
            catch (Exception)
            {
                throw;
            }
            return listaCartera;
        }

        public MatrizGestionViewModel RealMensual(ConexionSucursalViewModel cadenaConexion, MatrizGestionViewModel SucMesAnio, int valorPMonEmp, int valorPMaxEna, int valorCartera, int valorCteNvo, int valorMetIng, int valorLisCartera, int valorIngVta, int valorCTMA, int valorCteNvoVta)
        {
            MatrizGestionViewModel metaCartera = new MatrizGestionViewModel();
            MatrizGestionViewModel RealRespuestas = new MatrizGestionViewModel();
            //MatrizGestionViewModel metaCartera = new MatrizGestionViewModel();

            try
            {

                //PROMEDIO MONTO EMPEÑO
                metaCartera = MatrizGestionCon.ConsultaPromMontoEMpeño(valorPMonEmp, cadenaConexion, SucMesAnio);
                RealRespuestas.PromedioSem = metaCartera.PromedioSem;
                //PORCENTAJE MAXIMO ENAJENACIN
                metaCartera = MatrizGestionCon.ConsultaPorcMaxEna(valorPMaxEna, cadenaConexion, SucMesAnio);
                RealRespuestas.Folios = metaCartera.Folios;
                //CARTERA
                metaCartera = MatrizGestionCon.ConsultaMetaCartera(valorCartera, cadenaConexion, SucMesAnio);
                RealRespuestas.Cartera = metaCartera.Cartera;
                //CLIENTE NUEVO EMPEÑO
                metaCartera = MatrizGestionCon.ConsultaCltNvo(valorCteNvo, cadenaConexion, SucMesAnio);
                RealRespuestas.TotalCteNvo = metaCartera.TotalCteNvo;
                RealRespuestas.NumClientes = metaCartera.NumClientes;
                //META INGRESO
                metaCartera = MatrizGestionCon.ConsultaMetaIngreso(valorMetIng, cadenaConexion, SucMesAnio);
                RealRespuestas.ingreso = metaCartera.ingreso;

                //LISTA CARTERA
                metaCartera.CarteraView = MatizGestionlis.ListadoCarteraDAO(valorLisCartera, cadenaConexion, SucMesAnio);
                RealRespuestas.CarteraView = metaCartera.CarteraView;
                //VALOR INGRESO VENTA
                metaCartera.IngresoVenta = MatizGestionlis.ListadoIngresoVentaDAO(valorIngVta, cadenaConexion, SucMesAnio);
                RealRespuestas.IngresoVenta = metaCartera.IngresoVenta;

                //CARTERA TOTAL
                metaCartera = MatrizGestionCon.ConsultaCarteraTotal(cadenaConexion, valorCTMA);
                RealRespuestas.CarteraTotal = metaCartera.CarteraTotal;

                //CLIENTE NUEVO VENTA
                metaCartera = MatrizGestionCon.ConsultaCltNvoVta(valorCteNvoVta, cadenaConexion, SucMesAnio);
                RealRespuestas.NumClientesVta = metaCartera.NumClientesVta;

            }
            catch (Exception ex)
            {
                throw ex;
            }


            return RealRespuestas;

        }
        
        //Consulta de la cartera total (REAL)
        public MatrizGestionViewModel ConsultaCartTotal(ConexionSucursalViewModel cadenaConexion, int valorCTAliado)
        {
            MatrizGestionViewModel metas = null;
            try
            {
                metas = MatrizGestionCon.ConsultaCarteraTotal(cadenaConexion, valorCTAliado);
            }
            catch
            {
                throw;
            }
            return metas;
        }

        //-------------------------Aliado
        public List<MatrizGestionViewModel> ListadoAliadoServicio(int valorLista, MatrizGestionViewModel NomSuc, ConexionSucursalViewModel cadenaConexion)
        {
            List<MatrizGestionViewModel> listaServicio = null;
            try
            {
                listaServicio = MatizGestionlis.ListadoAliadoDAO(valorLista, NomSuc, cadenaConexion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return listaServicio;
        }

        //LISTAR EL REAL DE LOS INDICADORES POR ALIADO
        public MatrizGestionViewModel RealAliado(ConexionSucursalViewModel cadenaConexion, MatrizGestionViewModel metaCartera, int valor, int valorMCAliado, int valorCNAliado, int valorIEAliado, int valorIngVtaAliad)
        {
            MatrizGestionViewModel real = null;
            MatrizGestionViewModel RealRespuestas = new MatrizGestionViewModel();
            try
            {
                real = MatrizGestionCon.PromEmpAliado(cadenaConexion, metaCartera, valor);
                RealRespuestas.PromedioSem = real.PromedioSem;
                real = MatrizGestionCon.ConsultaMetaAliadoCartera(cadenaConexion, metaCartera, valorMCAliado);
                RealRespuestas.Cartera = real.Cartera;
                real = MatrizGestionCon.ConsultaCltNvoAliado(cadenaConexion, metaCartera, valorCNAliado);
                RealRespuestas.TotalCteNvo = real.TotalCteNvo;
                //Meta ingreso empeño
                real = MatrizGestionCon.ConsultaMIAliado(cadenaConexion, metaCartera, valorIEAliado);
                RealRespuestas.ingreso = real.ingreso;
                //Ingreso venta aparatos, celular y joyeria
                real.IngresoVenta = MatizGestionlis.ListadoIngVtaAliadoDAO(valorIngVtaAliad, cadenaConexion, metaCartera);
                RealRespuestas.IngresoVenta = real.IngresoVenta;
            }
            catch
            {
                throw;
            }
            return RealRespuestas;
        }
        
        //Listar base del aliaod
        //valorLista, matriz, cadenaConexion
        public MatrizGestionViewModel ListarBaseServicio(int baseVal, MatrizGestionViewModel operaciones, ConexionSucursalViewModel cadenaConexion)
        {
            /// List<MatrizGestionViewModel> lista = null;
            MatrizGestionViewModel listaMeta = null;
            try
            {
                listaMeta = new MatrizGestionViewModel();
                listaMeta.ValorMeta = MatizGestionlis.ListadoMetaAliadoDAO(baseVal, operaciones, cadenaConexion);
            }
            catch (Exception)
            {
                throw;
            }
            return listaMeta;
        }
        
        //---------------------Producto
        //Lista de los tipos de producto, aliados y familia
        public List<MatrizGestionViewModel> ListadoProductoServicio(int valorLista, MatrizGestionViewModel NomSuc, ConexionSucursalViewModel cadenaConexion)
        {
            List<MatrizGestionViewModel> listaServicio = null;
            try
            {
                listaServicio = MatizGestionlis.ListadoTProductoDAO(valorLista, NomSuc, cadenaConexion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return listaServicio;
        }
        //--Real porducto
        //public MatrizGestionViewModel RealProducto(ConexionSucursalViewModel cadenaConexion, MatrizGestionViewModel metaCartera, int valor, int valorMCProducto, int valorCNProducto, int valorMIProducto, int valorIVProducto)
        public ProductoViewModel RealProducto(ConexionSucursalViewModel cadenaConexion, MatrizGestionViewModel metaCartera, int valor, int valorMCProducto, int valorCNProducto, int valorMIProducto, int valorIVProducto)
        {
            MatrizGestionViewModel real = null;
            MatrizGestionViewModel RealRespuestas = new MatrizGestionViewModel();

            MatrizGestionViewModel Respuestas = null;
            //Este debe ser una lista , para ir iterando --Lista Real contiene el productViewModel
            ProductoViewModel productViewModel = new ProductoViewModel();
            List<MatrizGestionViewModel> ListaReal = new List<MatrizGestionViewModel>();
            try
            {

               metaCartera.Producto = metaCartera.Producto.TrimEnd(',');


                char[] delimiterChars = { ',' };
                string[] arr = metaCartera.Producto.Split(delimiterChars);
                


                foreach (string producto in arr)
                {

                    real = new MatrizGestionViewModel();
                    Respuestas = new MatrizGestionViewModel();
                    metaCartera.Producto= producto;

                    real = MatrizGestionCon.ConsultaPromEmpProducto(cadenaConexion, metaCartera, valor);
                    Respuestas.PromedioSem = real.PromedioSem;

                    real = MatrizGestionCon.ConsultaMetaCarteraProducto(cadenaConexion, metaCartera, valorMCProducto);
                    Respuestas.Cartera = real.Cartera;
                    real = MatrizGestionCon.ConsultaCltNvoProdcuto(cadenaConexion, metaCartera, valorCNProducto);
                    Respuestas.TotalCteNvo = real.TotalCteNvo;
                    real = MatrizGestionCon.ConsultaMetIngProducto(cadenaConexion, metaCartera, valorMIProducto);
                    Respuestas.ingreso = real.ingreso;
                    //Ingreso venta aparatos, celular y joyeria
                    real.IngresoVenta = MatizGestionlis.ListadoIngVtaProductoDAO(valorIVProducto, cadenaConexion, metaCartera);
                    Respuestas.IngresoVenta = real.IngresoVenta;
                    
                    Respuestas.Producto = producto;
                    //productViewModel.Productos = metaCartera.Producto;
                    //real
                    ListaReal.Add(Respuestas);
                    //ListaReal.Add(Respuestas);
                }
                //Respuestas.
                productViewModel.ProductosVM = ListaReal;

                //Recorrer ingresos 
                //foreach(MatrizGestionViewModel LisReal in productViewModel.ProductosVM)
                //{
                //    productViewModel.IngresoVenta = LisReal.IngresoVenta;                    
                //}
                



                }
            catch
            {
                throw;
            }
            // return RealRespuestas;
            return productViewModel;
        }

        //Alta de comentarios
        public MensajeTransaccion AltaComentarioServicio(ComentarioViewModel comentario, UsuarioViewModel usuarioLogin)
        {
            MensajeTransaccion mensajeAltaProducto = null;

            try
            {
                mensajeAltaProducto = MatrizGestionAlt.AltaComentario(comentario, usuarioLogin);
               
            }
            catch (Exception e)
            {
                if (mensajeAltaProducto == null)
                {
                    mensajeAltaProducto = new MensajeTransaccion();
                    mensajeAltaProducto.CodigoRespuesta = "CS-000001";
                    mensajeAltaProducto.MensajeRespuesta = "Ocurrió un problema al dar de alta el comentario. " + e.Message;
                }
            }
            return mensajeAltaProducto;
        }

      
        public MatrizGestionViewModel ListarComentarioServicio(int valorLista, ComentarioViewModel comentario)
        {
            MatrizGestionViewModel lista = new MatrizGestionViewModel();
            try
            {
                lista.comentarios = MatizGestionlis.ListadoComentariosDAO(valorLista, comentario);
                //listaServicio = MatizGestionlis.ListadoComentariosDAO(valorLista, comentario);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lista;
        }

        //-------------------------Anual
        public List<MatrizGestionViewModel> ListadoAnioServicio(int valorLista, MatrizGestionViewModel NomSuc, ConexionSucursalViewModel cadenaConexion)
        {
            List<MatrizGestionViewModel> listaServicio = null;
            try
            {
                listaServicio = MatizGestionlis.ListadoAnioDAO(valorLista, NomSuc, cadenaConexion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return listaServicio;
        }

        public MatrizGestionViewModel RealAnio(ConexionSucursalViewModel cadenaConexion, MatrizGestionViewModel metaCartera, int valorPresPromA, int valorPorMaxEA, int valorCartTA, int valorCarTotA, int valorCapPreA, int valorCTeNvoEA, int valorCTeNvoVA,int valorIngEmA, int valorIngVtaTA)
        {
            MatrizGestionViewModel RealRespuestas = new MatrizGestionViewModel();

            MatrizGestionViewModel Respuestas = null;
            //Este debe ser una lista , para ir iterando --Lista Real contiene el productViewModel
           // ProductoViewModel real = new ProductoViewModel();
            List<MatrizGestionViewModel> ListaReal = new List<MatrizGestionViewModel>();
            try
            {

                Respuestas = new MatrizGestionViewModel();
                //Prestamo promedio
                RealRespuestas = MatrizGestionCon.ConsultaPresPAnio(cadenaConexion, metaCartera, valorPresPromA);
                Respuestas.PromedioSem= RealRespuestas.PromedioSem;
                Respuestas.FolioCapPrest = RealRespuestas.FolioCapPrest;
                //porcentaje maximo enajenacion
                RealRespuestas = MatrizGestionCon.ConsultaPorcMEA(cadenaConexion, metaCartera, valorPorMaxEA);
                Respuestas.Folios = RealRespuestas.Folios;
                //Cartera joyeria, aparatos y celulares
                RealRespuestas.CarteraView = MatizGestionlis.ListadoCarteraAnioDAO(valorCartTA, cadenaConexion, metaCartera);
                Respuestas.CarteraView = RealRespuestas.CarteraView;
                //Cartera total
                RealRespuestas = MatrizGestionCon.ConsultaCarteraTotalAnn(cadenaConexion, metaCartera, valorCarTotA);
                Respuestas.CarteraTotal = RealRespuestas.CarteraTotal;
                //Capital prestado
                RealRespuestas = MatrizGestionCon.ConsultaCapPresAnn(cadenaConexion, metaCartera, valorCapPreA);
                Respuestas.Cartera = RealRespuestas.Cartera;
                //Promedio de préstamo clientes nuevos y clientes nuevos empeños
                RealRespuestas = MatrizGestionCon.ConsultaCltNvoAnn(cadenaConexion, metaCartera, valorCTeNvoEA);
                Respuestas.TotalCteNvo = RealRespuestas.TotalCteNvo;
                Respuestas.NumClientes = RealRespuestas.NumClientes;
                //clientes nuevos ventas
                RealRespuestas = MatrizGestionCon.ConsultaCltNvoVtaAnn(cadenaConexion, metaCartera, valorCTeNvoVA);
                Respuestas.NumClientesVta = RealRespuestas.NumClientesVta;
                //ingreso empeño
                RealRespuestas = MatrizGestionCon.ConsultaIngEmpeñoAnn(cadenaConexion, metaCartera, valorIngEmA);
                Respuestas.ingreso = RealRespuestas.ingreso;

                //Cartera joyeria, aparatos y celulares
                RealRespuestas.IngresoVenta = MatizGestionlis.ListadoIngresoVtaAnioDAO(valorIngVtaTA, cadenaConexion, metaCartera);
                Respuestas.IngresoVenta = RealRespuestas.IngresoVenta;



            }
            catch
            {
                throw;
            }
            // return RealRespuestas;
            return Respuestas;
        }
        //Lista de las metas por año
        public MatrizGestionViewModel ListaMetaAnioServicio(int baseVal, MatrizGestionViewModel operaciones)
        {
            /// List<MatrizGestionViewModel> lista = null;
            MatrizGestionViewModel listaMeta = null;
            try
            {
                listaMeta = new MatrizGestionViewModel();
                listaMeta.ValorMeta = MatizGestionlis.ListadoMetaAnioDAO(baseVal, operaciones);
            }
            catch (Exception)
            {
                throw;
            }
            return listaMeta;
        }


        //LISTA DE SUCURSALES PARA LA PANTALLA PRINCIPAL DE LOS INDICADORES
        public List<MatrizGestionViewModel> ListarSucursalINDServicio(int valorLista, UsuarioViewModel usuarioResponse)
        {
            List<MatrizGestionViewModel> listaServicio = null;
            try
            {
                listaServicio = MatizGestionlis.ListaSucursalesIND(valorLista, usuarioResponse);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return listaServicio;
        }


    }
}
