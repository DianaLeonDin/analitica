﻿using ENTIDADES.RH;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERVICIO.RH
{
    public class IRPServicio
    {
        DAO.RH.IRPListDAO ListadoIRP = new DAO.RH.IRPListDAO();

        public enum ListaIRP
        {
            IRP=1,
            IRP_Alta = 2,
            IRP_Baja= 3,
            IRP_Acumulado = 4,
        }

        
        public List<IRPViewModel> ListarIRPServicio(int valorLista)
        {
            List<IRPViewModel> listaServicio = null;
            try
            {
                listaServicio = ListadoIRP.ListaIRPDAO(valorLista);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return listaServicio;
        }

    }
}
