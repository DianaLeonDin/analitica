﻿using ENTIDADES.Login;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERVICIO.Seguridad
{
    public class ValidarPassword
    {
        public Boolean Validar(UsuarioViewModel usuarioResponse)
        {
            int minimo = 8;
            int mayuscula = 1;
            int minuscula = 1;
            int numero = 1;

            string MayusculasUno;
            MayusculasUno = "[A-Z]";
            System.Text.RegularExpressions.Regex May = new System.Text.RegularExpressions.Regex(MayusculasUno);

            string MinusculaslasUno;
            MinusculaslasUno = @"[a-z]";
            System.Text.RegularExpressions.Regex Min = new System.Text.RegularExpressions.Regex(MinusculaslasUno);
            //new Regex(MinusculaslasUno);

            string NumeroUno;
            NumeroUno = @"[0-9]";
            System.Text.RegularExpressions.Regex Num = new System.Text.RegularExpressions.Regex(NumeroUno);

            if (usuarioResponse.Password.Length < minimo)
            {
                return false;

            }
            else if (May.Matches(usuarioResponse.Password).Count < mayuscula)
            {
                return false;
            }
            else if (Min.Matches(usuarioResponse.Password).Count < minuscula)
            {
                return false;
            }
            else if (Num.Matches(usuarioResponse.Password).Count < numero)
            {
                return false;
            }
            return true;

            //Dim upper As New System.Text.RegularExpressions.Regex("[A-Z]")
            //Dim lower As New System.Text.RegularExpressions.Regex("[a-z]")
            //Dim number As New System.Text.RegularExpressions.Regex("[0-9]")
            //'Dim special As New System.Text.RegularExpressions.Regex("[^a-zA-Z0-9]")

            //If Len(pwd) < minLength Then Return False
            //If upper.Matches(pwd).Count < numUpper Then Return False
            //If lower.Matches(pwd).Count < numLower Then Return False
            //If number.Matches(pwd).Count < numNumbers Then Return False
            //'If special.Matches(pwd).Count < numSpecial Then Return False
            //Return True


        }

    }
}
