﻿using ENTIDADES.Seguridad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAO.Seguridad;
using ENTIDADES.Login;
using ENTIDADES.Base;

namespace SERVICIO.Seguridad
{
    public partial class PermisoEmpleadoServicio
    {

        PermisoEmpleadoListDAO ListadoPermisoEmpleado = new PermisoEmpleadoListDAO();

        public enum Acceso
        {
            
            Lista_Permiso = 1,//BAse por sucursal
            Lista_Modulo = 2,//Listado de todas las bases de la matriz de indicaciones de gestion 
            Lista_Puesto = 3,//Listado de todas las bases de la matriz de indicaciones de gestion 
            Lista_AccesoLogin = 4,//Listado de los usuarios que iniciaron sesion 
            Lista_AccesoOperacion = 5,//Listado de acceso de indicadores por operaciones diario
            Lista_PermisoRol = 6,//Listado de los puestos con permisos para la vista de permisos
            Lista_PermisoMod = 7,//Listado de los modulos de permisos en la vista de permisos

            Lista_Anio = 20,//Listado de los años del servdor 4
            Lista_Mes = 21,//Listado de los meses del servidor 4
            Lista_Sucursal = 22,//Listado de las sucursales del servidor 4

            Lista_PuestoDBDIN =23,
            Lista_EmpleadoDBDIN = 24,
            Lista_SucSucursalDBDIN = 25,
            Lista_EmpSuc = 26,//Listado para arbol, empleados con permiso para sus sucursales 
            Lista_Emp = 27,//Listado para arbol, ESMPLEADO , PARA LOS PERMISS
            Eliminar_Permiso = 28,//Eliminar ermiso(para quitar permiso de visualizar sucursales a los aliados en la pantalla principal) 
            Elimina_Login =29, //Elimina los permisos en el login , permisos que se le asigno por puesto
     
        }

        public PermisoEmpleadoViewModel ConsultaPermisoEmpleado(int valor, string Cvepuesto)
        {
            PermisoEmpleadoViewModel permiso = null;
            try
            {
                permiso = ListadoPermisoEmpleado.ConsultaPermisoEmpleadoDAO(valor, Cvepuesto);
            }
            catch
            {

                throw;
            }
            return permiso;
        }

        //Listado de los modulos para los Accesos / permisos


        //public List<PermisoEmpleadoViewModel> ListarModuloServicio()
        //{
        //    List<PermisoEmpleadoViewModel> lista = null;
        //    try
        //    {
        //        lista = ListadoPermisoEmpleado.ListaModuloDAO();
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //    return lista;
        //}


         //PUESTO Y ROL
        public AccesoViewModel AccesoServicio(int valorPer, int valorMod,int valorPue)
        {
            AccesoViewModel Acceso = new AccesoViewModel();
            try
            {
                Acceso.Modulo= ListadoPermisoEmpleado.ListaAccesoDAO(valorMod);
                Acceso.Rol = ListadoPermisoEmpleado.ListaAccesoDAO(valorPue);
            }
            catch
            {
                throw;
            }
            return Acceso;
        }

        //cONTROL ACCESO LOGIN Y ACCESO INDICADORES DE OPERACIONES DIARIO
        public List<PermisoEmpleadoViewModel> ControlAccesoServicio(PermisoEmpleadoViewModel permiso,int valorAcceso)
        {
            List<PermisoEmpleadoViewModel> acceso = null;
            try
            {
                acceso = ListadoPermisoEmpleado.ListaControlAccesoDAO(permiso,valorAcceso);
            }
            catch
            {
                throw;
            }
            return acceso;
        }
        //AGREGAR PERMISOS POR MEDIO DEL PUESTO
        public MensajeTransaccion AltaPermisoServicio(PermisoEmpleadoViewModel perfilModel)
        {
            MensajeTransaccion mensajeAlta = null;

            try
            {
                perfilModel.Modulo = perfilModel.Modulo.TrimEnd(',');
                
                char[] delimiterChars = { ',' };
                string[] arr = perfilModel.Modulo.Split(delimiterChars);


                foreach (string modulo in arr)
                {
                    perfilModel.Modulo = modulo;
                    mensajeAlta = ListadoPermisoEmpleado.AltaPermiso(perfilModel);
                }

            }
            catch (Exception e)
            {
                if (mensajeAlta == null)
                {
                    mensajeAlta = new MensajeTransaccion();
                    mensajeAlta.CodigoRespuesta = "CS-000001";
                    mensajeAlta.MensajeRespuesta = "Ocurrió un problema al dar de alta el permiso. " + e.Message;
                }
            }
            return mensajeAlta;
        }


        public AccesoViewModel PuestosPermisoServicio(int Rol, int Modulo)
        {
            AccesoViewModel acceso = null;
            try
            {
                acceso = new AccesoViewModel();
                acceso.Rol = ListadoPermisoEmpleado.ListaTreeRolDAO(Rol);
                //acceso.Modulo = ListadoPermisoEmpleado.ListaAccesoDAO(Modulo);
                acceso.Modulo = ListadoPermisoEmpleado.ListaTreeModuloDAO(Modulo);


            }
            catch
            {
                throw;
            }
            return acceso;
        }

        //AÑÑO, MES Y SUCURSAL DE LOS QUE YA ACCEDIEROON A BUSCAR EN EL INDICADOR DIARIO
        public AccesoViewModel ControlServicio(int anio, int mes, int sucursal)
        {
            AccesoViewModel acceso = null;
            try
            {
                acceso = new AccesoViewModel();
                //para el año
                acceso.anio = ListadoPermisoEmpleado.ListaAccesoAnioDAO(anio);
                //para el mes
                acceso.mes = ListadoPermisoEmpleado.ListaAccesoMesDAO(mes);
                //para la sucursal
                acceso.sucursal = ListadoPermisoEmpleado.ListaAccesoSucursalDAO(sucursal);

            }
            catch
            {
                throw;
            }
            return acceso;
        }


        //PUESTO y EMPLEADO 

        public List<PermisoEmpleadoViewModel> PuestoPermisoServicio(int valorPto)
        {
            List<PermisoEmpleadoViewModel> Acceso = new List<PermisoEmpleadoViewModel>();
            try
            {
                Acceso = ListadoPermisoEmpleado.ListaPuestoDAO(valorPto);
                //Acceso.Empleado = ListadoPermisoEmpleado.ListaEmpleadoDAO(valorEdo);
                //Acceso.sucursal = ListadoPermisoEmpleado.ListaSucursalDAO(valorSal);
            }
            catch
            {
                throw;
            }
            return Acceso;
        }
        
        public List<PermisoEmpleadoViewModel> EmpleadoPermisoServicio(int valorEdo, string puesto)
        {
            List<PermisoEmpleadoViewModel> Acceso = new List<PermisoEmpleadoViewModel>();
            try
            {
                Acceso = ListadoPermisoEmpleado.ListaEmpleadoDAO(valorEdo, puesto);
                //Acceso.sucursal = ListadoPermisoEmpleado.ListaSucursalDAO(valorSal);
            }
            catch
            {
                throw;
            }
            return Acceso;
        }

        //AGREGAR PERMISOS EMPLEADOS CON SUS SUCURSALES
        public MensajeTransaccion AltaEmpSucServicio(PermisoEmpleadoViewModel perfilModel)
        {
            MensajeTransaccion mensajeAlta = null;

            try
            {
                perfilModel.Sucursal = perfilModel.Sucursal.TrimEnd(',');

                char[] delimiterChars = { ',' };
                string[] arr = perfilModel.Sucursal.Split(delimiterChars);


                foreach (string suc in arr)
                {
                    char[] valor = { ':' };
                    string[] sucID =suc.Split(valor);

                    perfilModel.SucursalID = sucID[1];
                    perfilModel.Sucursal = sucID[0];

                    mensajeAlta = ListadoPermisoEmpleado.AltaPermisoSucursal(perfilModel);
                }

            }
            catch (Exception e)
            {
                if (mensajeAlta == null)
                {
                    mensajeAlta = new MensajeTransaccion();
                    mensajeAlta.CodigoRespuesta = "CS-000001";
                    mensajeAlta.MensajeRespuesta = "Ocurrió un problema al dar de alta el permiso en las sucursales para los empleados. " + e.Message;
                }
            }
            return mensajeAlta;
        }
        
        public AccesoViewModel TreePuestosPermisoServicio(int modulo, int emp)
        {
            AccesoViewModel acceso = null;
            try
            {
                acceso = new AccesoViewModel();
                acceso.Rol = ListadoPermisoEmpleado.ListaTreeEmpDAO(emp);
                acceso.Modulo = ListadoPermisoEmpleado.ListaTreeEmpSucDAO(modulo);


            }
            catch
            {
                throw;
            }
            return acceso;
        }


        //ELIMINAR PERMISO EL USUARIO NO PUEDE VER TODAS LAS SUCURSALES EN EL PANTALLA PRINCIPAL
        public MensajeTransaccion DeletePermisoServicio(UsuarioViewModel usr, int valor)
        {
            MensajeTransaccion mensajeAlta = null;

            try
            {
                    mensajeAlta = ListadoPermisoEmpleado.DeletePermiso(usr, valor);   
            }
            catch (Exception e)
            {
                if (mensajeAlta == null)
                {
                    mensajeAlta = new MensajeTransaccion();
                    mensajeAlta.CodigoRespuesta = "CS-000002";
                    mensajeAlta.MensajeRespuesta = "Ocurrió un problema al eliminar el permiso. " + e.Message;
                }
            }
            return mensajeAlta;
        }
        //El puesto del  usuario no puede ver todas las vistas
        public MensajeTransaccion DeleteLogServicio(UsuarioViewModel usr, int valor)
        {
            MensajeTransaccion mensajeAlta = null;

            try
            {
                mensajeAlta = ListadoPermisoEmpleado.DeletePermisoLog(usr, valor);
            }
            catch (Exception e)
            {
                if (mensajeAlta == null)
                {
                    mensajeAlta = new MensajeTransaccion();
                    mensajeAlta.CodigoRespuesta = "CS-000002";
                    mensajeAlta.MensajeRespuesta = "Ocurrió un problema al eliminar el permiso . " + e.Message;
                }
            }
            return mensajeAlta;
        }



    }
}
