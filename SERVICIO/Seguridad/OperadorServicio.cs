﻿using DAO.EmpleadoERP;
using DAO.Seguridad;
using ENTIDADES.Base;
using ENTIDADES.Login;
using ENTIDADES.Permisos;
using Seguridad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERVICIO.Seguridad
{
   public  class OperadorServicio: IOperadorServicio
    {
        
        IEmpleadoConDAO _iEmpleadoConDAO;
        IEmpleadoActDAO _iEmpleadoActDAO;
        IEmpleadoLisDAO _iEmpleadoLisDAO;

        public OperadorServicio(IEmpleadoConDAO _iEmpleadoConDAO, IEmpleadoActDAO _iEmpleadoActDAO, IEmpleadoLisDAO _iEmpleadoLisDAO)
        {

            this._iEmpleadoConDAO = _iEmpleadoConDAO;
            this._iEmpleadoActDAO = _iEmpleadoActDAO;
            this._iEmpleadoLisDAO = _iEmpleadoLisDAO;
        }

        public List<Modulo> ListarAccesoModulo (PUESTOOPERACION puestoOperacion)  
        {
            List<Modulo> listaModulo = null;
            try
            {
                listaModulo = _iEmpleadoLisDAO.PermisoModuloEmpleado(puestoOperacion);
            }
            catch (Exception)
            {

                throw;
            }

            return listaModulo;
        }

        public UsuarioViewModel OperadoresCon(Empleado usuario)
        {
            UsuarioViewModel usuariOpera = null;
            try
            {
                
                usuariOpera = _iEmpleadoConDAO.EmpleadoLoginCon(usuario);

                if (usuariOpera == null)
                {
                    usuariOpera = new UsuarioViewModel();
                    usuariOpera.codigoRespuesta = "S-000002";
                    usuariOpera.mensajeRespuesta = "El Usuario/Contraseña ingresado no es válido: ";
                }
            }
            catch (Exception ex)
            {
                if (usuariOpera == null)
                {
                    usuariOpera = new UsuarioViewModel();
                    usuariOpera.codigoRespuesta = "S-000001";
                    usuariOpera.mensajeRespuesta = "Ocurrió un error en la capa Servicio: " + ex.Message;
                }
            }
            return usuariOpera;
        }



        public MensajeTransaccion ActualizarContraseña(UsuarioViewModel usuarioResponse)
        {
            Crypto crypto = new Crypto();
            MensajeTransaccion mensaje = null;
            try
            {
                string clave = usuarioResponse.Password.ToString();
                usuarioResponse.Password = crypto.Encriptar(clave).ToString();
                mensaje = _iEmpleadoActDAO.empleadoAct_01(usuarioResponse);
            }
            catch (Exception ex)
            {
                if (mensaje == null)
                {
                    mensaje = new MensajeTransaccion();
                    mensaje.CodigoRespuesta = "S-000001";
                    mensaje.MensajeRespuesta = "Ocurrió un error en la capa Servicio: " + ex.Message;
                }
            }
            return mensaje;
        }
    }
}
