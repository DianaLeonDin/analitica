﻿using ENTIDADES.Base;
using ENTIDADES.Mantenimiento;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERVICIO.Mantenimiento
{
    public class OperacionesServicio
    {

        DAO.Mantenimiento.OperacionesAltDAO AltaOperaciones = new DAO.Mantenimiento.OperacionesAltDAO();
        DAO.Mantenimiento.OperacionesLisDAO ListaOperaciones = new DAO.Mantenimiento.OperacionesLisDAO();
        DAO.Mantenimiento.OperacionConDAO ConsultaOperaciones = new DAO.Mantenimiento.OperacionConDAO();
        DAO.Mantenimiento.OperacionesActDAO ActualizarOperaciones = new DAO.Mantenimiento.OperacionesActDAO();

        public enum Lista
        {
            Lista_BaseAll = 2,//Listado de todas las bases de la matriz de indicaciones de gestion 
            Lista_BaseSuc = 3,//BAse por sucursal
            Lista_BaseMes = 19,//BAse por sucursal
            Lista_DiasHabiles = 7,//Lista Dias habiles
        }

        public enum Consulta
        {
            Con_Base = 1,//Consulta de todas las bases de la matriz de indicaciones de gestion 
            Con_DiaHabil = 2,//Consulta de todas los dias habiles de la matriz de indicaciones de gestion 
            Con_ConDH = 3,//Consulta de los dias habiles por folio de la matriz de indicaciones de gestion 
            Con_ConDHMes=4, //Consulta de los dias habiles del mes anterior
        }
        //Actualizar MAntenimiento de operaciones
        public enum Actualizar
        {
            Act_Base = 1,//Actualizar la base/Meta de la matriz de indicaciones de gestion 
            Act_DiaHabil = 2,//Actualizar el dia habil de la matriz de indicaciones de gestion 
        }


        //agregar nueva base de matriz de indicadores de gestion para operaciones  //AltaMatrizGestionBase
        public MensajeTransaccion AltaProductoServicio(OperacionesViewModel operacion)
        {
            MensajeTransaccion mensajeAltaProducto = null;

            try
            {
                if (operacion.AliadoID=="" | operacion.AliadoID == null)
                {
                    
                    foreach (OperacionesViewModel man in operacion.SucMeta)
                    {
                        operacion.SucursalID = man.SucursalID;
                        operacion.Valor = man.Valor;

                        if (operacion.Valor != 0)
                        {
                            mensajeAltaProducto = AltaOperaciones.AltaBase(operacion);
                        }
                    }
                }
                else
                {
                    mensajeAltaProducto = AltaOperaciones.AltaBase(operacion);
                }
                
            }
            catch (Exception e)
            {
                if (mensajeAltaProducto == null)
                {
                    mensajeAltaProducto = new MensajeTransaccion();
                    mensajeAltaProducto.CodigoRespuesta = "CS-000001";
                    mensajeAltaProducto.MensajeRespuesta = "Ocurrió un problema al dar de alta la base. " + e.Message;
                }
            }
            return mensajeAltaProducto;
        }


        //Lista de la base de de matriz de indicadores de gestion
        public List<OperacionesViewModel> ListarBaseServicio(OperacionesViewModel operaciones, int valorLista)
        {
            List<OperacionesViewModel> lista = null;
            try
            {
                lista = ListaOperaciones.ListaBaseDAO(operaciones,valorLista);
            }
            catch (Exception)
            {
                throw;
            }
            return lista;
        }

        //consulta base del mantenimiento , en la  matriz de indicadores de gestion
        public OperacionesViewModel ConsultaBase(int valor, int bases)
        {
            OperacionesViewModel baseCon = null;
            try
            {
               
                baseCon = ConsultaOperaciones.ConsultaBase(valor, bases);
            }
            catch
            {
                throw;
            }
            return baseCon;
        }

        //actualizar base/Meta del manteniiento de operaciones en el indicador de la matriz de indicadores de gestion
        public MensajeTransaccion ActualizarBaseServicio(OperacionesViewModel mantenimiento, int valorAct)
        {
            MensajeTransaccion mensaje = null;
            try
            {
                mensaje = ActualizarOperaciones.ActualizarMantenimientoOperaciones(mantenimiento, valorAct);
            }
            catch (Exception e)
            {
                if (mensaje == null)
                {
                    mensaje = new MensajeTransaccion();
                    mensaje.CodigoRespuesta = "CS-000001";
                    mensaje.MensajeRespuesta = "Ocurrió un problema al modificar la base. " + e.Message;
                }
            }

            return mensaje;
        }


        //Lista de los dias habiles (catalogo manteniimento )de de matriz de indicadores de gestion
        public List<OperacionesViewModel> ListarDiaHabilServicio(int valorLista)
        {
            List<OperacionesViewModel> lista = null;
            try
            {
                lista = ListaOperaciones.ListaDiaHabilDAO(valorLista);
            }
            catch (Exception)
            {
                throw;
            }
            return lista;
        }

        //agregar nuevo dia habil de matriz de indicadores de gestion para operaciones  //AltaMatrizGestionBase
        public MensajeTransaccion AltaDiaHabilServicio(OperacionesViewModel operacion)
        {
            MensajeTransaccion mensajeAltaProducto = null;

            try
            {
                string[] Fecha = operacion.FechaInhabil.Split(',');

                foreach (string DiaInhabil in Fecha)
                {
                    //Console.WriteLine(DiaInhabil);
                    mensajeAltaProducto = AltaOperaciones.AltaDiaHabil(operacion, DiaInhabil);
                }

                //mensajeAltaProducto = AltaOperaciones.AltaDiaHabil(operacion);
            }
            catch (Exception e)
            {
                if (mensajeAltaProducto == null)
                {
                    mensajeAltaProducto = new MensajeTransaccion();
                    mensajeAltaProducto.CodigoRespuesta = "CS-000001";
                    mensajeAltaProducto.MensajeRespuesta = "Ocurrió un problema al dar de alta la base. " + e.Message;
                }
            }
            return mensajeAltaProducto;
        }

        //Consulta de los dias habiles con el flio ID
        public OperacionesViewModel ConsultaDiasHAbiles(OperacionesViewModel Operacions,int valor)
        {
            OperacionesViewModel diasHabiles = null;
            try
            {
                diasHabiles = ConsultaOperaciones.ConsultaDiaHabil(Operacions, valor);
            }
            catch
            {
                throw;
            }
            return diasHabiles;
        }
        //
        //Consulta de los dias habiles por medio de la sucursal
        public OperacionesViewModel ConsultaDiasHAbilesSjuc(OperacionesViewModel Operacions, int valor)
        {
            OperacionesViewModel diasHabiles = null;
            try
            {
                diasHabiles = ConsultaOperaciones.ConsultaDiaHabilSuc(Operacions, valor);
            }
            catch
            {
                throw;
            }
            return diasHabiles;
        }

        //actualizar Dia habil del manteniiento de operaciones en el indicador de la matriz de indicadores de gestion
        public MensajeTransaccion ActualizarDiaHabilServicio(OperacionesViewModel mantenimiento, int valorAct)
        {
            MensajeTransaccion mensaje = null;
            try
            {
                mensaje = ActualizarOperaciones.ActualizarDiaHabil(mantenimiento, valorAct);
            }
            catch (Exception e)
            {
                if (mensaje == null)
                {
                    mensaje = new MensajeTransaccion();
                    mensaje.CodigoRespuesta = "CS-000002";
                    mensaje.MensajeRespuesta = "Ocurrió un problema al modificar el dia habil. " + e.Message;
                }
            }

            return mensaje;
        }




    }
}
