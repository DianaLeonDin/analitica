﻿
using ENTIDADES.Login;
using SistemaBase.Controllers.Login;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SistemaBase.Filters
{
    public class VerificaSession: ActionFilterAttribute
    {
        UsuarioViewModel usuarioResponse = new UsuarioViewModel();
        // LoginViewModel usuarioResponse = new LoginViewModel();
        //ConexionViewModel conexion = new ConexionViewModel();

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            try
            {
                base.OnActionExecuting(filterContext);

                //Diana Leon inicio


                usuarioResponse = (UsuarioViewModel)HttpContext.Current.Session["usuario"];
                if (usuarioResponse == null)
                {

                    if (filterContext.Controller is LoginController == false)
                    {
                        filterContext.HttpContext.Response.Redirect("~/Login/Index");
                    }



                }

                //Diana Leon fin

            }
            catch (Exception)
            {
                filterContext.Result = new RedirectResult("~/Login/Index");
            }

        }
    }
}