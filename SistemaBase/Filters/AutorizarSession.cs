﻿using ENTIDADES.Login;
using ENTIDADES.Permisos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SistemaBase.Filters
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
    public class AutorizarSession : AuthorizeAttribute
    {
       
        private int idOperacion;
        UsuarioViewModel usuarioResponse = new UsuarioViewModel();
        public AutorizarSession(int idOperacion = 0)
        {
            this.idOperacion = idOperacion;

        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            //String nombreOperacion = "";
            //String nombreModulo = "";
            try
            {

                usuarioResponse = (UsuarioViewModel)HttpContext.Current.Session["usuario"];
                //using (ReportesERPDinEntities conn = new ReportesERPDinEntities())
                //{
                //    var listaOPeraciones = (from op in conn.RolOperacion
                //                            join rl in conn.Rol
                //                            on op.RolID equals rl.RolID
                //                            where rl.NombrePuestoERP == usuarioResponse.CvePuestoEmpleado && op.PermisoOperacionID == idOperacion
                //                            select op).FirstOrDefault();

                //    if (listaOPeraciones == null)
                //    {

                //        filterContext.Result = new RedirectResult("~/Error/Index?mensaje=" + "NO CUENTA CON PERMISOS PARA ACCEDER A ESTA PANTALLA");

                //    }


                //}

                var listaOPeraciones = (from op in usuarioResponse.listaModulo
                                        where op.ModuloID == idOperacion
                                        select op).FirstOrDefault();

                if (listaOPeraciones == null)
                  {

                           filterContext.Result = new RedirectResult("~/Error/Index?mensaje=" + "NO CUENTA CON PERMISOS PARA ACCEDER A ESTA PANTALLA");

                        }
            }
            catch (Exception ex)
            {
                filterContext.Result = new RedirectResult("~/Error/Index?mensaje=" +  "LO SENTIMOS, OCURRIÓ UN ERROR EN EL SISTEMA" + ex.Message);
            }
        }

    }
}