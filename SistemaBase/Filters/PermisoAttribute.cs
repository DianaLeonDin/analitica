﻿using ENTIDADES.Login;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace SistemaBase.Filters
{
    public class PermisoAttribute: ActionFilterAttribute
    {

        public RolesPermisos Permiso { get; set; }
        UsuarioViewModel usuarioResponse = new UsuarioViewModel();
        

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            usuarioResponse = (UsuarioViewModel)HttpContext.Current.Session["usuario"];

            if (Controllers.MenuLateralController.TienePermiso(this.Permiso, usuarioResponse.CvePuestoEmpleado))
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                {
                    controller = "Home",
                    action = "Index"
                }));
            }
        }

    }
}