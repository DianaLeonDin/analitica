﻿using ENTIDADES.Login;
using ENTIDADES.Seguridad;
using SERVICIO.Seguridad;
using SistemaBase.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SistemaBase.Controllers
{
    public class MenuLateralController : Controller
    {

        UsuarioViewModel usuarioResponse = new UsuarioViewModel();
        

        // GET: MenuLateral
        public ActionResult MenuLateral()
        {
            return PartialView();
        }


        public static bool TienePermiso(RolesPermisos valor, string Cvepuesto)
        {
            PermisoEmpleadoViewModel permiso = new PermisoEmpleadoViewModel();
            permiso = MenuLateralController.Get(Convert.ToInt32(valor), Cvepuesto);
            if (permiso.RolID.Equals(0))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public static PermisoEmpleadoViewModel Get(int valor, string Cvepuesto)
        {

            PermisoEmpleadoViewModel permisoo = new PermisoEmpleadoViewModel();
            
            PermisoEmpleadoServicio perm = new PermisoEmpleadoServicio();
            PermisoEmpleadoViewModel modelo = new PermisoEmpleadoViewModel();
            
            permisoo = perm.ConsultaPermisoEmpleado(valor, Cvepuesto);
            return permisoo;
        }



    }
}