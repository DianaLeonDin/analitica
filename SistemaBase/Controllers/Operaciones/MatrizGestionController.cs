﻿using DAO.Base;
using DevExpress.Web.Mvc;
using ENTIDADES.Base;
using ENTIDADES.Login;
using ENTIDADES.Mantenimiento;
using ENTIDADES.Operaciones;
using SERVICIO.Login;
using SERVICIO.Mantenimiento;
using SERVICIO.Operaciones;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SistemaBase.Controllers.Operaciones
{
    public class MatrizGestionController : Controller
    {
        SERVICIO.Operaciones.MatrizGestionServicio MatrizGestionService = new SERVICIO.Operaciones.MatrizGestionServicio();
        SERVICIO.Mantenimiento.OperacionesServicio OperacionService = new SERVICIO.Mantenimiento.OperacionesServicio();
        UsuarioViewModel usuarioResponse = new UsuarioViewModel();
        LoginServicio LoginSer = new LoginServicio();

        // GET: MatrizGestion
        //public ActionResult Index()
        //{
        //    return View();
        //}

        public ActionResult MatrizGestion()
        {
            return View();
        }

        //Listado de las sucursales
        [HttpPost]
        public JsonResult Sucursal()
        {
            List<MatrizGestionViewModel> listaMatrizGestion = null;
            List<SelectListItem> Lista = new List<SelectListItem>();
            try
            {
                int valorLista = (int)MatrizGestionServicio.ListaMatrizGestion.Lista_Suc;
                listaMatrizGestion = MatrizGestionService.ListarSucursalServicio(valorLista);
                foreach (var val in listaMatrizGestion)
                {
                    Lista.Add(new SelectListItem { Text = Convert.ToString(val.NomSuc), Value = Convert.ToString(val.CveSuc) });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(new SelectList(Lista, "Value", "Text", JsonRequestBehavior.AllowGet));
        }

        //Crear conexion con las sucursales
        public JsonResult Conexion(MatrizGestionViewModel matrizGestion)
        {
            ConexionSucursalViewModel cadenaConexion = null;
            cadenaConexion = new ConexionSucursalViewModel();
            MatrizGestionViewModel metaCartera = new MatrizGestionViewModel();
            int totalResgistros = 0;

            // cadena de conexion del web config (cadenaConexion);
            string[] valores = System.Web.Configuration.WebConfigurationManager.AppSettings[matrizGestion.CveSuc.Trim()].Split(':');
            //crear cadena de conexion
            cadenaConexion.cadena = "Data Source =" + valores[0] + "\\sqlserver;Initial Catalog=Empeño; Persist Security Info=True; Uid=sa;Pwd=" + valores[1] + "";
            //agregarlo a una session
            cadenaConexion.sucursal = valores[2];

            System.Web.HttpContext.Current.Session["conexionDB"] = cadenaConexion;

            //validar si se encontro en el web config
            if (valores.Equals(""))
            {
                return Json("0.0.0.0: No se encontro conexion");
                //cadenaConexion.cadena = "0.0.0.0: No se encontro conexion";
            }
            else
            {
                //validar si se realizo la conexion
                if (VerifyConnection(cadenaConexion) == true)
                {
                    int valor = (int)MatrizGestionServicio.ConsultaMatrizGestion.Con_MetaCartera;
                    //consultar la meta de cartera (REAL)
                    metaCartera = MatrizGestionService.ConsultaMetaCartera(valor, cadenaConexion);

                    //consultar la meta de ingreso (REAL)
                    valor = (int)MatrizGestionServicio.ConsultaMatrizGestion.Con_MetaIngreso;
                    MatrizGestionViewModel metaIngreso = new MatrizGestionViewModel();
                    //consultar la meta de ingreso (REAL)
                    metaIngreso = MatrizGestionService.ConsultaMetaIngreso(valor, cadenaConexion);
                    metaCartera.ingreso = metaIngreso.ingreso;

                    //cosulta clientes nuevos empeños (REAL)
                    valor = (int)MatrizGestionServicio.ConsultaMatrizGestion.Con_ClienteNvo;
                    MatrizGestionViewModel CltNvo = new MatrizGestionViewModel();
                    CltNvo = MatrizGestionService.ConsultaCteNvo(valor, cadenaConexion);
                    metaCartera.TotalCteNvo = CltNvo.TotalCteNvo;
                    metaCartera.NumClientes = CltNvo.NumClientes;

                    //cosulta clientes nuevos ventas(REAL)
                    valor = (int)MatrizGestionServicio.ConsultaMatrizGestion.Con_ClienteNvoVta;
                    MatrizGestionViewModel CltNvoVta = new MatrizGestionViewModel();
                    CltNvoVta = MatrizGestionService.ConsultaCteNvoVta(valor, cadenaConexion);
                    metaCartera.NumClientesVta = CltNvoVta.NumClientesVta;

                    //cosulta promedio monto empeño (REAL)
                    valor = (int)MatrizGestionServicio.ConsultaMatrizGestion.Con_PromMontoEmp;
                    MatrizGestionViewModel PromMontoEmp = new MatrizGestionViewModel();
                    PromMontoEmp = MatrizGestionService.ConsultaPromedioMontoEmpeño(valor, cadenaConexion);
                    metaCartera.PromedioSem = PromMontoEmp.PromedioSem;
                    metaCartera.FolioCapPrest = PromMontoEmp.FolioCapPrest;


                    //cosulta promedio monto empeño (REAL)
                    valor = (int)MatrizGestionServicio.ConsultaMatrizGestion.Con_PorcMaxEna;
                    MatrizGestionViewModel PorcMaxEna = new MatrizGestionViewModel();
                    PorcMaxEna = MatrizGestionService.ConsultaPorcMaxEna(valor, cadenaConexion);
                    metaCartera.Folios = PorcMaxEna.Folios;


                    //cosulta promedio monto empeño (REAL)
                    valor = (int)MatrizGestionServicio.ListaMatrizGestion.Lista_IngresoVta;
                    MatrizGestionViewModel IngreVta = new MatrizGestionViewModel();
                    IngreVta = MatrizGestionService.ListarIngresoVtaServicio(valor, cadenaConexion);
                    metaCartera.IngresoVenta = IngreVta.IngresoVenta;
                    //metaCartera.


                    //cosulta ingreso total (REAL)
                    valor = (int)MatrizGestionServicio.ConsultaMatrizGestion.Con_IngTotal;
                    MatrizGestionViewModel IngreTotal = new MatrizGestionViewModel();
                    IngreTotal = MatrizGestionService.ConsultaIngTotal(valor, cadenaConexion);
                    metaCartera.IngTotal = IngreTotal.IngTotal;

                    //cosulta cartera apartado, celular y joyeria (REAL)
                    valor = (int)MatrizGestionServicio.ListaMatrizGestion.Lista_Cartera;
                    MatrizGestionViewModel Cartera = new MatrizGestionViewModel();
                    Cartera = MatrizGestionService.ListarCarteraServicio(valor, cadenaConexion);
                    metaCartera.CarteraView = Cartera.CarteraView;

                    //Cartera total 
                    valor = (int)MatrizGestionServicio.ConsultaMatrizGestion.Con_CarTotal;
                    MatrizGestionViewModel CarteraTotal = new MatrizGestionViewModel();
                    CarteraTotal = MatrizGestionService.ConsultaCartTotal(cadenaConexion, valor);
                    metaCartera.CarteraTotal = CarteraTotal.CarteraTotal;

                    //Guardar datos en la bitacora acceso

                    MensajeTransaccion mensaje = new MensajeTransaccion();
                    usuarioResponse = (UsuarioViewModel)Session["usuario"];
                    usuarioResponse.CveSucursal = valores[3];


                    using (var conn = new SqlConnection(BaseDAO.analiticabd))
                    {
                        string query = "select count(BitacoraID) as totalResgistros  from BITACORAACCESO where Usuario ='" + usuarioResponse.CodigoAlterno + "' and "
                             + "SucursalID ='" + valores[3].Trim() + "' and YEAR(Fecha) = YEAR(GETDATE()) and month(Fecha) = month(GETDATE()) and DAY(Fecha) = DAY(GETDATE())";
                        SqlCommand cmds = new SqlCommand(query, conn);
                        conn.Open();
                        SqlDataReader reader = cmds.ExecuteReader();
                        if (reader.Read())
                        {
                            totalResgistros = int.Parse(reader["totalResgistros"].ToString().Trim());
                        }
                        reader.Close();
                    }

                    if (totalResgistros < 1)
                    {
                        mensaje = LoginSer.AltaBitacoraAcceso(usuarioResponse);
                    }

                    return Json(metaCartera);
                }
                else
                {
                    return Json("Error al conectar, intentar mas tarde.");
                }


            }
            // return cadenaConexion;
            // return Json("");
        }

        public bool VerifyConnection(ConexionSucursalViewModel cadenaConexion)
        {

            string connetionString = null;
            SqlConnection cnn;
            connetionString = cadenaConexion.cadena.ToString();

            cnn = new SqlConnection(connetionString);
            try
            {
                cnn.Open();
                cnn.Close();
                return true;
            }
            catch
            {
                return false;
            }

        }

        public JsonResult Base(OperacionesViewModel operaciones)
        {
            List<OperacionesViewModel> lista = null;
            try
            {
                int valorLista = (int)OperacionesServicio.Lista.Lista_BaseSuc;
                lista = OperacionService.ListarBaseServicio(operaciones, valorLista);
            }
            catch (Exception)
            {

                throw;
            }
            return Json(lista, JsonRequestBehavior.AllowGet);
        }



        public JsonResult BaseMes(OperacionesViewModel operaciones)
        {
            List<OperacionesViewModel> lista = null;
            try
            {
                int valorLista = (int)OperacionesServicio.Lista.Lista_BaseMes;
                lista = OperacionService.ListarBaseServicio(operaciones, valorLista);
            }
            catch (Exception)
            {
                throw;
            }
            return Json(lista, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]

        //CARGAR EL MES ANTERIOR DE LA MATRIZ DE INDICADORES DE GESTION DIARIO
        //OperacionesViewModel operacionMes
        public JsonResult MesAnterior(MatrizGestionViewModel SucMesAnio)
        {
            ConexionSucursalViewModel cadenaConexion = null;
            cadenaConexion = new ConexionSucursalViewModel();
            MatrizGestionViewModel metaCartera = new MatrizGestionViewModel();

            try
            {
                // cadena de conexion del web config (cadenaConexion);
                string[] valores = System.Web.Configuration.WebConfigurationManager.AppSettings[SucMesAnio.CveSuc].Split(':');
                //crear cadena de conexion
                cadenaConexion.cadena = "Data Source =" + valores[0] + "\\sqlserver;Initial Catalog=Empeño; Persist Security Info=True; Uid=sa;Pwd=" + valores[1] + "";
                //agregarlo a una session
                cadenaConexion.sucursal = valores[2];

                System.Web.HttpContext.Current.Session["conexionDB"] = cadenaConexion;

                //validar si se encontro en el web config
                if (valores.Equals(""))
                {
                    return Json("0.0.0.0: No se encontro conexion");
                    //cadenaConexion.cadena = "0.0.0.0: No se encontro conexion";
                }
                else
                {
                    //validar si se realizo la conexion
                    if (VerifyConnection(cadenaConexion) == true)
                    {

                        //VALORES PARA CONSULTAS EN LOS STORE PROCEDURE
                        var valorPMonEmp = (int)MatrizGestionServicio.ConsultaMatrizGestionMes.Con_PMEmpenio;
                        var valorPMaxEna = (int)MatrizGestionServicio.ConsultaMatrizGestionMes.Con_PMEnajenacion;
                        var valorCartera = (int)MatrizGestionServicio.ConsultaMatrizGestionMes.Con_MCartera;
                        var valorCteNvo = (int)MatrizGestionServicio.ConsultaMatrizGestionMes.Con_CNvo;
                        var valorMetIng = (int)MatrizGestionServicio.ConsultaMatrizGestionMes.Con_MIngreso;
                        var valorLisCartera = (int)MatrizGestionServicio.ListaMatrizGestionMes.Lista_Carteras;
                        var valorIngVta = (int)MatrizGestionServicio.ListaMatrizGestionMes.Lista_IVenta;

                        var valorCTMA = (int)MatrizGestionServicio.ConsultaMatrizGestionMes.Con_CartTot;
                        
                        var valorCteNvoVta = (int)MatrizGestionServicio.ConsultaMatrizGestionMes.Con_CteNvoVta;

                        //consultar la real de cartera (REAL)
                        metaCartera = MatrizGestionService.RealMensual(cadenaConexion, SucMesAnio, valorPMonEmp, valorPMaxEna, valorCartera, valorCteNvo, valorMetIng, valorLisCartera, valorIngVta, valorCTMA, valorCteNvoVta);
                    }
                }


            }
            catch (Exception ex)
            {
                throw ex;
            }


            // agregar la conexion de sucursal para buscar las consultas y encontrar el real.
            return Json(metaCartera);
        }








        //Consulta de meta por cartera
        //public JsonResult MetaCartera()
        //{
        //    MatrizGestionViewModel metaCartera = new MatrizGestionViewModel();

        //    try
        //    {
        //        int valor = (int)MatrizGestionServicio.ConsultaMatrizGestion.Lista_MetaCartera;
        //        metaCartera = MatrizGestionService.ConsultaMetaCartera(valor);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    return Json("");
        //}


        public JsonResult TipoProducto()
        {
            List<MatrizGestionViewModel> listaTProducto = null;
            List<SelectListItem> Lista = new List<SelectListItem>();
            MatrizGestionViewModel matriz = new MatrizGestionViewModel();
            try
            {
                int valorLista = (int)MatrizGestionServicio.ListaMatrizGestion.Lista_TipoProducto;
                listaTProducto = MatrizGestionService.ListadoServicio(valorLista, matriz);

                Lista.Add(new SelectListItem { Text = Convert.ToString("Todos"), Value = Convert.ToString("") });
                foreach (var val in listaTProducto)
                {
                    Lista.Add(new SelectListItem { Text = Convert.ToString(val.nombre), Value = Convert.ToString(val.nombre) });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(new SelectList(Lista, "Value", "Text", JsonRequestBehavior.AllowGet));
        }
        //public JsonResult Aliado()
        //{
        //    List<MatrizGestionViewModel> listaAliado = null;
        //    List<SelectListItem> Lista = new List<SelectListItem>();
        //    try
        //    {
        //        MatrizGestionViewModel matriz = new MatrizGestionViewModel();
        //        int valorLista = (int)MatrizGestionServicio.ListaMatrizGestion.Lista_Aliado;
        //        listaAliado = MatrizGestionService.ListadoServicio(valorLista, matriz);

        //        Lista.Add(new SelectListItem { Text = Convert.ToString("Todos"), Value = Convert.ToString("") });
        //        foreach (var val in listaAliado)
        //        {
        //            Lista.Add(new SelectListItem { Text = Convert.ToString(val.nombre), Value = Convert.ToString(val.nombre) });
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    return Json(new SelectList(Lista, "Value", "Text", JsonRequestBehavior.AllowGet));
        //    //return Json("");
        //}

        //LLENAR COMBOBOX DE FAMILIA, TIPO DE PRODUCTO Y ALIADO
        public JsonResult Busqueda()
        {
            BusquedaViewModel busqueda = null;
            try
            {
                busqueda = new BusquedaViewModel();
                busqueda = MatrizGestionService.ListarCombo();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(busqueda, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// ///////////////Aliado vista 
        /// </summary>
        /// <returns></returns>



        //ALIADO POR SUCURSAL EN MATRIZ DE INDICADOR DE GESTION EN LA PESTAÑA DE ALIADO
        [HttpPost]
        public JsonResult AliadoXSuc(MatrizGestionViewModel matriz)
        {
            //NomSuc
            List<MatrizGestionViewModel> listaAliado = null;
            List<SelectListItem> Lista = new List<SelectListItem>();
            try
            {
                int valorLista = (int)MatrizGestionServicio.ListaMatrizGestion.Lista_AliadoXSuc;
                listaAliado = MatrizGestionService.ListadoServicio(valorLista, matriz);

                Lista.Add(new SelectListItem { Text = Convert.ToString(" Seleccionar "), Value = Convert.ToString("") });
                foreach (var val in listaAliado)
                {
                    Lista.Add(new SelectListItem { Text = Convert.ToString(val.nombre), Value = Convert.ToString(val.nombre) });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            ViewBag.Aliado = Lista;
            return Json(new SelectList(Lista, "Value", "Text", JsonRequestBehavior.AllowGet));
        }




        //BD SICAVI EN EL SERVIDOR 4
        //public JsonResult DiaHabil()
        //{
        //    MatrizGestionViewModel diaH = new MatrizGestionViewModel();
        //    ConexionSucursalViewModel cadenaConexion = null;
        //    cadenaConexion = new ConexionSucursalViewModel();


        //    try
        //    {
        //        cadenaConexion = (ConexionSucursalViewModel)Session["conexionDB"];

        //        int valorLista = (int)MatrizGestionServicio.ConsultaMatrizGestion.Con_DiaHabil;
        //        diaH = MatrizGestionService.ConsultaDiasHAbiles(valorLista, cadenaConexion);



        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    return Json(diaH.total, JsonRequestBehavior.AllowGet);
        //    // return Json("");
        //}



        //----------------------------MATRIZ POR ALIADO

        //ALIADO POR SUCURSAL EN MATRIZ DE INDICADOR DE GESTION EN LA PESTAÑA DE ALIADO TABLA SQL EMPEÑO DE SUCURSAL
        [HttpPost]
        public JsonResult Aliado(MatrizGestionViewModel matriz)
        {
            //NomSuc
            //List<MatrizGestionViewModel> listaAliado = null;
            //List<SelectListItem> Lista = new List<SelectListItem>();

            // ConexionSucursalViewModel cadenaConexion = null;
            //try
            //{
            //    cadenaConexion = (ConexionSucursalViewModel)Session["conexionDB"];


            //    int valorLista = (int)MatrizGestionServicio.ListaMatrizGestionAliado.Lista_Aliado;
            //    listaAliado = MatrizGestionService.ListadoAliadoServicio(valorLista, matriz);

            //    foreach (var val in listaAliado)
            //    {
            //        Lista.Add(new SelectListItem { Text = Convert.ToString(val.nombre), Value = Convert.ToString(val.CveUser) });
            //    }
            //}
            //catch (Exception ex)
            //{
            //    throw ex;
            //}
            //ViewBag.Aliado = Lista;
            //return Json(new SelectList(Lista, "Value", "Text", JsonRequestBehavior.AllowGet));


            ConexionSucursalViewModel cadenaConexion = null;
            cadenaConexion = new ConexionSucursalViewModel();
            MatrizGestionViewModel metaCartera = new MatrizGestionViewModel();

            List<MatrizGestionViewModel> listaAliado = null;
            List<SelectListItem> Lista = new List<SelectListItem>();


            // cadena de conexion del web config (cadenaConexion);
            string[] valores = System.Web.Configuration.WebConfigurationManager.AppSettings[matriz.CveSuc].Split(':');
            //crear cadena de conexion
            cadenaConexion.cadena = "Data Source =" + valores[0] + "\\sqlserver;Initial Catalog=Empeño; Persist Security Info=True; Uid=sa;Pwd=" + valores[1] + "";
            //agregarlo a una session
            cadenaConexion.sucursal = valores[2];

            System.Web.HttpContext.Current.Session["conexionDB"] = cadenaConexion;

            //validar si se encontro en el web config
            if (valores.Equals(""))
            {
                return Json("0.0.0.0: No se encontro conexion");
            }
            else
            {
                //validar si se realizo la conexion
                if (VerifyConnection(cadenaConexion) == true)
                {
                    //Mandar al services para que ahi se busquen ols valores de los reales.
                    int valorLista = (int)MatrizGestionServicio.ListaMatrizGestionAliado.Lista_Aliado;
                    listaAliado = MatrizGestionService.ListadoAliadoServicio(valorLista, matriz, cadenaConexion);

                    //Lista.Add(new SelectListItem { Text = Convert.ToString("Todos"), Value = Convert.ToString("Todos") });
                    Lista.Add(new SelectListItem { Text = Convert.ToString(" Seleccionar "), Value = Convert.ToString("") });
                    foreach (var val in listaAliado)
                    {
                        Lista.Add(new SelectListItem { Text = Convert.ToString(val.nombre), Value = Convert.ToString(val.Cusr) });
                    }
                }

            }
            // ViewBag.Aliado = Lista;
            return Json(new SelectList(Lista, "Value", "Text", JsonRequestBehavior.AllowGet));
        }




        //ALIADO POR SUCURSAL EN MATRIZ DE INDICADOR DE GESTION EN LA PESTAÑA DE ALIADO TABLA SQL EMPEÑO DE SUCURSAL
        [HttpPost]
        public JsonResult AliadoMesAnterior(MatrizGestionViewModel matriz)
        {
            ConexionSucursalViewModel cadenaConexion = null;

            cadenaConexion = (ConexionSucursalViewModel)Session["conexionDB"];



            MatrizGestionViewModel metaCartera = new MatrizGestionViewModel();

            List<MatrizGestionViewModel> listaAliado = null;
            List<SelectListItem> Lista = new List<SelectListItem>();



            //Mandar al services para que ahi se busquen ols valores de los reales.
            int valorLista = (int)MatrizGestionServicio.ListaMatrizGestionAliado.Lista_AliadoMA;
            listaAliado = MatrizGestionService.ListadoAliadoServicio(valorLista, matriz, cadenaConexion);

            Lista.Add(new SelectListItem { Text = Convert.ToString(" Seleccionar "), Value = Convert.ToString("") });
            //Lista.Add(new SelectListItem { Text = Convert.ToString("Todos"), Value = Convert.ToString("Todos") });
            foreach (var val in listaAliado)
            {
                Lista.Add(new SelectListItem { Text = Convert.ToString(val.nombre), Value = Convert.ToString(val.Cusr) });
            }

            // ViewBag.Aliado = Lista;
            return Json(new SelectList(Lista, "Value", "Text", JsonRequestBehavior.AllowGet));
        }

        //ALIADO POR SUCURSAL EN MATRIZ DE INDICADOR DE GESTION EN LA PESTAÑA DE ALIADO
        [HttpPost]
        public JsonResult IndicadorAliado(MatrizGestionViewModel matriz)
        {
            //ConexionSucursalViewModel cadenaConexion = null;
            //cadenaConexion = new ConexionSucursalViewModel();
            //MatrizGestionViewModel metaCartera = new MatrizGestionViewModel();

            //// cadena de conexion del web config (cadenaConexion);
            //string[] valores = System.Web.Configuration.WebConfigurationManager.AppSettings[matriz.CveSuc].Split(':');
            ////crear cadena de conexion
            //cadenaConexion.cadena = "Data Source =" + valores[0] + "\\sqlserver;Initial Catalog=Empeño; Persist Security Info=True; Uid=sa;Pwd=" + valores[1] + "";
            ////agregarlo a una session
            //cadenaConexion.sucursal = valores[2];

            //System.Web.HttpContext.Current.Session["conexionDB"] = cadenaConexion;

            ////validar si se encontro en el web config
            //if (valores.Equals(""))
            //{
            //    return Json("0.0.0.0: No se encontro conexion");
            //}
            //else
            //{
            //    //validar si se realizo la conexion
            //    if (VerifyConnection(cadenaConexion) == true)
            //    {
            //        //Mandar al services para que ahi se busquen ols valores de los reales.
            //    }
            MatrizGestionViewModel metaCartera = new MatrizGestionViewModel();
            ConexionSucursalViewModel cadenaConexion = null;
            try
            {
                cadenaConexion = (ConexionSucursalViewModel)Session["conexionDB"];

                var valorPPAliado = (int)MatrizGestionServicio.ConsultaMatrizGestionAliado.Con_PPAliado;
                var valorMCAliado = (int)MatrizGestionServicio.ConsultaMatrizGestionAliado.Con_MCAliado;
                var valorCNAliado = (int)MatrizGestionServicio.ConsultaMatrizGestionAliado.Con_CNAliado;
                var valorIEAliado = (int)MatrizGestionServicio.ConsultaMatrizGestionAliado.Con_MIAliado;//meta ingreso(empeño)
                //var valorMetIng = (int)MatrizGestionServicio.ConsultaMatrizGestionMes.Con_MIngreso;
                //var valorLisCartera = (int)MatrizGestionServicio.ListaMatrizGestionMes.Lista_Carteras;
                var valorIngVtaAliad = (int)MatrizGestionServicio.ListaMatrizGestionAliado.Lista_IngAliado;

                //consultar la real de cartera (REAL)
                metaCartera = MatrizGestionService.RealAliado(cadenaConexion, matriz, valorPPAliado, valorMCAliado, valorCNAliado, valorIEAliado, valorIngVtaAliad);



                //Meta alido
                // List<MatrizGestionViewModel> lista = null;
                MatrizGestionViewModel metaResponse = null;
                //ListarBaseServicio
                int valorLista = (int)MatrizGestionServicio.ListaMatrizGestionAliado.Lista_Meta;
                metaResponse = MatrizGestionService.ListarBaseServicio(valorLista, matriz, cadenaConexion);
                metaCartera.ValorMeta = metaResponse.ValorMeta;

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Json(metaCartera);
            //RealAliado
        }


        //ALIADO POR SUCURSAL EN MATRIZ DE INDICADOR DE GESTION EN LA PESTAÑA DE ALIADO
        [HttpPost]
        public JsonResult IndicadorMesAnteriorAliado(MatrizGestionViewModel matriz)
        {

            MatrizGestionViewModel metaCartera = new MatrizGestionViewModel();
            ConexionSucursalViewModel cadenaConexion = null;
            try
            {
                cadenaConexion = (ConexionSucursalViewModel)Session["conexionDB"];

                var valorPPAliado = (int)MatrizGestionServicio.ConsultaMatrizGestionAliado.Con_PPAliadoMA;
                var valorMCAliado = (int)MatrizGestionServicio.ConsultaMatrizGestionAliado.Con_MCAliadoMA;
                var valorCNAliado = (int)MatrizGestionServicio.ConsultaMatrizGestionAliado.Con_CNAliadoMA;
                var valorIEAliado = (int)MatrizGestionServicio.ConsultaMatrizGestionAliado.Con_IEAliadoMA;//meta ingreso(empeño)
                var valorIngVtaAliad = (int)MatrizGestionServicio.ListaMatrizGestionAliado.Lista_IngAliadoMA;
                //consultar la real de cartera (REAL)
                metaCartera = MatrizGestionService.RealAliado(cadenaConexion, matriz, valorPPAliado, valorMCAliado, valorCNAliado, valorIEAliado, valorIngVtaAliad);


                MatrizGestionViewModel metaResponse = null;
                //ListarBaseServicio
                int valorLista = (int)MatrizGestionServicio.ListaMatrizGestionAliado.Lista_MetaMA;
                metaResponse = MatrizGestionService.ListarBaseServicio(valorLista, matriz, cadenaConexion);
                metaCartera.ValorMeta = metaResponse.ValorMeta;

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Json(metaCartera);
            //RealAliado
        }







        /// <summary>
        /// ///POR PRODUCTO
        /// </summary>
        /// <returns></returns>
        public JsonResult Familia(MatrizGestionViewModel matriz)
        {
            //List<MatrizGestionViewModel> listaFamilia = null;
            //List<SelectListItem> Lista = new List<SelectListItem>();
            //MatrizGestionViewModel matrix = new MatrizGestionViewModel();
            //ConexionSucursalViewModel cadenaConexion = null;

            //try
            //{
            //    cadenaConexion = (ConexionSucursalViewModel)Session["conexionDB"];
            //    int valorLista = (int)MatrizGestionServicio.ListaMatrizGestion.Lista_Familia;

            //    listaFamilia = MatrizGestionService.ListadoProductoServicio(valorLista, matriz, cadenaConexion);

            //    //Lista.Add(new SelectListItem { Text = Convert.ToString("Todos"), Value = Convert.ToString("") });
            //    foreach (var val in listaFamilia)
            //    {
            //        Lista.Add(new SelectListItem { Text = Convert.ToString(val.nombre), Value = Convert.ToString(val.Cve) });
            //    }

            //}
            //catch (Exception ex)
            //{
            //    throw ex;
            //}
            //return Json(new SelectList(Lista, "Value", "Text", JsonRequestBehavior.AllowGet));



            ConexionSucursalViewModel cadenaConexion = new ConexionSucursalViewModel();
            MatrizGestionViewModel metaCartera = null;

            List<MatrizGestionViewModel> listaFamilia = null;
            List<SelectListItem> Lista = new List<SelectListItem>();
            int valorLista = (int)MatrizGestionServicio.ListaMatrizGestion.Lista_Familia;

            try
            {
                // cadena de conexion del web config (cadenaConexion);
                string[] valores = System.Web.Configuration.WebConfigurationManager.AppSettings[matriz.CveSuc].Split(':');
                //crear cadena de conexion
                cadenaConexion.cadena = "Data Source =" + valores[0] + "\\sqlserver;Initial Catalog=Empeño; Persist Security Info=True; Uid=sa;Pwd=" + valores[1] + "";
                //agregarlo a una session
                cadenaConexion.sucursal = valores[2];

                System.Web.HttpContext.Current.Session["conexionDB"] = cadenaConexion;

                //validar si se encontro en el web config
                if (valores.Equals(""))
                {
                    return Json("0.0.0.0: No se encontro conexion");
                }
                else
                {
                    //validar si se realizo la conexion
                    if (VerifyConnection(cadenaConexion) == true)
                    {
                        listaFamilia = MatrizGestionService.ListadoProductoServicio(valorLista, matriz, cadenaConexion);

                        //Lista.Add(new SelectListItem { Text = Convert.ToString("Todos"), Value = Convert.ToString("") });
                        foreach (var val in listaFamilia)
                        {
                            Lista.Add(new SelectListItem { Text = Convert.ToString(val.nombre), Value = Convert.ToString(val.Cve) });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Json(new SelectList(Lista, "Value", "Text", JsonRequestBehavior.AllowGet));


        }
        
        //ALIADO POR SUCURSAL EN MATRIZ DE INDICADOR DE GESTION EN LA PESTAÑA DE ALIADO MatrizGestionViewModel matriz, string[] TProducto
        [HttpPost]
        public JsonResult IndicadorProducto(MatrizGestionViewModel matriz)
        {

            MatrizGestionViewModel metaCartera = new MatrizGestionViewModel();
            ProductoViewModel prodt = new ProductoViewModel();
            ConexionSucursalViewModel cadenaConexion = null;
            try
            {
                cadenaConexion = (ConexionSucursalViewModel)Session["conexionDB"];

                // Mandar al services para que ahi se busquen ols valores de los reales.
                var valorPPProducto = (int)MatrizGestionServicio.ConsultaMatrizGestionProducto.Con_PPProducto;
                var valorMCProducto = (int)MatrizGestionServicio.ConsultaMatrizGestionProducto.Con_MCProducto;
                var valorCNProducto = (int)MatrizGestionServicio.ConsultaMatrizGestionProducto.Con_CNProducto;
                var valorMIProducto = (int)MatrizGestionServicio.ConsultaMatrizGestionProducto.Con_MIProducto;


                var valorIVProducto = (int)MatrizGestionServicio.ListaMatrizGestionProducto.Lista_IngProducto;

                //consultar la real de cartera (REAL)
                //metaCartera = MatrizGestionService.RealProducto(cadenaConexion, matriz, valorPPProducto, valorMCProducto, valorCNProducto, valorMIProducto, valorIVProducto);
                prodt = MatrizGestionService.RealProducto(cadenaConexion, matriz, valorPPProducto, valorMCProducto, valorCNProducto, valorMIProducto, valorIVProducto);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(prodt);
            //return Json(metaCartera);
            //RealAliado
        }

        //mes anterior
        [HttpPost]
        public JsonResult IndicadorMesAnteriorProducto(MatrizGestionViewModel matriz)
        {

            MatrizGestionViewModel metaCartera = new MatrizGestionViewModel();
            ConexionSucursalViewModel cadenaConexion = null;
            try
            {
                cadenaConexion = (ConexionSucursalViewModel)Session["conexionDB"];

                var valorPPAliado = (int)MatrizGestionServicio.ConsultaMatrizGestionProducto.Con_PPProductoMA;
                var valorMCAliado = (int)MatrizGestionServicio.ConsultaMatrizGestionProducto.Con_CPProductoMA;

                var valorCNAliado = (int)MatrizGestionServicio.ConsultaMatrizGestionProducto.Con_CNProductoMA;
                //var valorCteNvo = (int)MatrizGestionServicio.ConsultaMatrizGestionMes.Con_CNvo;
                //var valorMetIng = (int)MatrizGestionServicio.ConsultaMatrizGestionMes.Con_MIngreso;
                //var valorLisCartera = (int)MatrizGestionServicio.ListaMatrizGestionMes.Lista_Carteras;
                //var valorIngVta = (int)MatrizGestionServicio.ListaMatrizGestionMes.Lista_IVenta;
                var valorMIProducto = (int)MatrizGestionServicio.ConsultaMatrizGestionProducto.Con_MIProductoMA;
                //Ingreso venta aparatos, celular y joyeria
                var valorIVProducto = (int)MatrizGestionServicio.ListaMatrizGestionProducto.Lista_IngProductoMA;
                //consultar la real de cartera (REAL)


                // metaCartera = MatrizGestionService.RealProducto(cadenaConexion, matriz, valorPPAliado, valorMCAliado, valorCNAliado, valorMIProducto, valorIVProducto);

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Json(metaCartera);
            //RealAliado
        }

        public ActionResult MatrizGestionDevExpress()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddComentario(ComentarioViewModel comentario)
        {
            MensajeTransaccion mensaje = null;
            MatrizGestionViewModel gestion= new MatrizGestionViewModel();

            try
            {
                //Alta comentario
                UsuarioViewModel usuarioLogin = (UsuarioViewModel)System.Web.HttpContext.Current.Session["usuario"];
                mensaje = MatrizGestionService.AltaComentarioServicio(comentario, usuarioLogin);

                //Listar comentarios
                int valorLista = (int)MatrizGestionServicio.ListaMatrizGestion.Lista_Comentarios;
                gestion = MatrizGestionService.ListarComentarioServicio(valorLista, comentario);
                
            }
            catch (Exception e)
            {
                if (mensaje == null)
                {
                    mensaje = new MensajeTransaccion();
                    mensaje.CodigoRespuesta = "CC-000001";
                    mensaje.MensajeRespuesta = "Ocurrió un problema al guardar el comentario del indicador. " + e.Message;
                }
            }
            //return Json(mensaje.MensajeRespuesta, gestion);
            return PartialView("Modalcomentario",gestion);
        }




        //ComentarioViewModel comentario
        public ActionResult Modalcomentario(ComentarioViewModel comentario)
        {
            List<ComentarioViewModel> listaAliado = null;
            MatrizGestionViewModel gestion;
            try
            {
                int valorLista = (int)MatrizGestionServicio.ListaMatrizGestion.Lista_Comentarios;
                //listaAliado = MatrizGestionService.ListarComentarioServicio(valorLista, comentario);
                gestion = MatrizGestionService.ListarComentarioServicio(valorLista, comentario);


            }
            catch (Exception ex)
            {
                throw ex;
            }
            

            return PartialView(gestion);
        }
        //[ValidateInput(false)]
        //public ActionResult PivotGrid1Partial()
        //{
        //    //var model = new object[0];
        //    List<OperacionesViewModel> lista = null;
        //    try
        //    {
        //        OperacionesViewModel operaciones = new OperacionesViewModel();
        //        operaciones.SucursalID = "Centro 69";
        //        int valorLista = (int)OperacionesServicio.Lista.Lista_BaseSuc;
        //        lista = OperacionService.ListarBaseServicio(operaciones, valorLista);
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //    return PartialView("~/Views/MatrizGestion/_PivotGrid1Partial.cshtml", lista);
        //}



        //ANUAL POR SUCURSAL EN MATRIZ DE INDICADOR DE GESTION EN LA PESTAÑA DE ANUAL TABLA SQL EMPEÑO DE SUCURSAL
        [HttpPost]
        public JsonResult AnioSucursal(MatrizGestionViewModel matriz)
        {


            ConexionSucursalViewModel cadenaConexion = null;
            cadenaConexion = new ConexionSucursalViewModel();
            MatrizGestionViewModel metaCartera = new MatrizGestionViewModel();

            List<MatrizGestionViewModel> listaAnio = null;
            List<SelectListItem> Lista = new List<SelectListItem>();


            // cadena de conexion del web config (cadenaConexion);
            string[] valores = System.Web.Configuration.WebConfigurationManager.AppSettings[matriz.CveSuc].Split(':');
            //crear cadena de conexion
            cadenaConexion.cadena = "Data Source =" + valores[0] + "\\sqlserver;Initial Catalog=Empeño; Persist Security Info=True; Uid=sa;Pwd=" + valores[1] + "";
            //agregarlo a una session
            cadenaConexion.sucursal = valores[2];

            System.Web.HttpContext.Current.Session["conexionDB"] = cadenaConexion;

            //validar si se encontro en el web config
            if (valores.Equals(""))
            {
                return Json("0.0.0.0: No se encontro conexion");
            }
            else
            {
                //validar si se realizo la conexion
                if (VerifyConnection(cadenaConexion) == true)
                {
                    //Mandar al services para que ahi se busquen ols valores de los reales.
                    int valorLista = (int)MatrizGestionServicio.ListaAnual.ListaAnio;
                    listaAnio = MatrizGestionService.ListadoAnioServicio(valorLista, matriz, cadenaConexion);

                    //Lista.Add(new SelectListItem { Text = Convert.ToString("Todos"), Value = Convert.ToString("Todos") });
                    foreach (var val in listaAnio)
                    {
                        Lista.Add(new SelectListItem { Text = Convert.ToString(val.AnnEmp), Value = Convert.ToString(val.CveSuc) });
                    }
                }

            }
            // ViewBag.Aliado = Lista;
            return Json(new SelectList(Lista, "Value", "Text", JsonRequestBehavior.AllowGet));
        }

        public ActionResult IndAnual(MatrizGestionViewModel matriz)
        {
            MatrizGestionViewModel metaCartera = null;
            ConexionSucursalViewModel cadenaConexion = null;
            try
            {
                cadenaConexion = (ConexionSucursalViewModel)Session["conexionDB"];

                // real
                var valorPresPromA = (int)MatrizGestionServicio.ConsultaAnual.Con_PresProm; //Prestamo promedio
                var valorPorMaxEA = (int)MatrizGestionServicio.ConsultaAnual.Con_PorcMaxE; //Porcentaje maximo enajenacion
                var valorCartTA = (int)MatrizGestionServicio.ConsultaAnual.ListCartera; //Lista cartera aparato, celular y joyeria              
                var valorCarTotA = (int)MatrizGestionServicio.ConsultaAnual.Con_CartTot;//Cartera total
                var valorCapPreA = (int)MatrizGestionServicio.ConsultaAnual.Con_CapPres;//Capital prestado
                var valorCTeNvoEA = (int)MatrizGestionServicio.ConsultaAnual.con_CteNvoEM;//Clientes nuevos empeños
                var valorCTeNvoVA = (int)MatrizGestionServicio.ConsultaAnual.con_CteNvoVM;//Clientes nuevos ventas

                var valorIngEmA = (int)MatrizGestionServicio.ConsultaAnual.Con_IngEmp;

                var valorIngVtaTA = (int)MatrizGestionServicio.ConsultaAnual.ListIngVta; //Lista ingreso vta aparato, celular y joyeria
                //consultar la real de cartera (REAL)
                metaCartera = MatrizGestionService.RealAnio(cadenaConexion, matriz, valorPresPromA, valorPorMaxEA, valorCartTA, valorCarTotA, valorCapPreA, valorCTeNvoEA, valorCTeNvoVA, valorIngEmA, valorIngVtaTA);


                //META POR AÑO
                MatrizGestionViewModel metaResponse = null;
                //ListarBaseServicio
                int ListaMeta = (int)MatrizGestionServicio.ListaAnual.ListaMeta;
                metaResponse = MatrizGestionService.ListaMetaAnioServicio(ListaMeta, matriz);
                metaCartera.ValorMeta = metaResponse.ValorMeta;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(metaCartera);

            //return View();
        }


        //Listado de las sucursales
        [HttpPost]
        public JsonResult SucursalIndOperacion()
        {
            List<MatrizGestionViewModel> listaMatrizGestion = null;
            List<SelectListItem> Lista = new List<SelectListItem>();
            UsuarioViewModel usuarioResponse = null;

            try
            {
                int valorLista = (int)MatrizGestionServicio.ListaMatrizGestion.Lista_SucInd;
                usuarioResponse = (UsuarioViewModel)Session["usuario"];
                listaMatrizGestion = MatrizGestionService.ListarSucursalINDServicio(valorLista, usuarioResponse);

                foreach (var val in listaMatrizGestion)
                {
                    Lista.Add(new SelectListItem { Text = Convert.ToString(val.NomSuc), Value = Convert.ToString(val.CveSuc) });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(new SelectList(Lista, "Value", "Text", JsonRequestBehavior.AllowGet));
        }




        [ValidateInput(false)]
        public ActionResult PivotGridPartial()
        {
            //var model = new object[0];
            List<OperacionesViewModel> lista = null;
            try
            {
                OperacionesViewModel operaciones = new OperacionesViewModel();
                operaciones.SucursalID = "Centro 69";
                int valorLista = (int)OperacionesServicio.Lista.Lista_BaseSuc;
                lista = OperacionService.ListarBaseServicio(operaciones, valorLista);
            }
            catch (Exception)
            {
                throw;
            }
            return PartialView("~/Views/MatrizGestion/_PivotGridPartial.cshtml", lista);
        }
    }
}