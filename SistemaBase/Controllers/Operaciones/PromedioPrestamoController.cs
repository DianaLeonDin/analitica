﻿using ENTIDADES.Operaciones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SERVICIO.Operaciones;

namespace SistemaBase.Controllers.Operaciones
{
    public class PromedioPrestamoController : Controller
    {

        SERVICIO.Operaciones.PromedioPrestamoServicio PromPrestamoServicio = new PromedioPrestamoServicio();

        // GET: PromedioPrestamo
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult Sucursal()
        {
            List<PromedioPrestamoViewModel> listaPromedioPrestamo = null;
            List<SelectListItem> Lista = new List<SelectListItem>();
            try
            {
                int valorLista = (int)PromedioPrestamoServicio.ListaPromedioPrestamo.List_Suc;
                listaPromedioPrestamo = PromPrestamoServicio.ListarNomSucServicio(valorLista);
                foreach (var val in listaPromedioPrestamo)
                {
                    Lista.Add(new SelectListItem { Text = Convert.ToString(val.NomSuc), Value = Convert.ToString(val.NomSuc) });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(new SelectList(Lista, "Value", "Text", JsonRequestBehavior.AllowGet));
        }

        public JsonResult Anio(PromedioPrestamoViewModel promedio)
        {
            List<PromedioPrestamoViewModel> listaPromedioPrestamo = null;
            List<SelectListItem> Lista = new List<SelectListItem>();
            try
            {
                int valorLista = (int)PromedioPrestamoServicio.ListaPromedioPrestamo.List_Anio;
                listaPromedioPrestamo = PromPrestamoServicio.ListaranioServicio(valorLista, promedio);
                foreach (var val in listaPromedioPrestamo)
                {
                    Lista.Add(new SelectListItem { Text = Convert.ToString(val.AnnEmp), Value = Convert.ToString(val.AnnEmp) });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(new SelectList(Lista, "Value", "Text", JsonRequestBehavior.AllowGet));
        }

        [HttpPost]
        public JsonResult Mes(PromedioPrestamoViewModel promedio)
        {
            List<PromedioPrestamoViewModel> listaPromedioPrestamo = null;
            List<SelectListItem> Lista = new List<SelectListItem>();
            try
            {
                int valorLista = (int)PromedioPrestamoServicio.ListaPromedioPrestamo.List_Mes;
                listaPromedioPrestamo = PromPrestamoServicio.ListarMesServicio(valorLista, promedio);

                Lista.Add(new SelectListItem { Text = Convert.ToString("Todos"), Value = Convert.ToString("") });
                foreach (var val in listaPromedioPrestamo)
                {
                    Lista.Add(new SelectListItem { Text = Convert.ToString(val.Mes), Value = Convert.ToString(val.MesEmp) });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(new SelectList(Lista, "Value", "Text", JsonRequestBehavior.AllowGet));
        }
        
        [HttpPost]
        public JsonResult Aliado(PromedioPrestamoViewModel promedio)
        {
            List<PromedioPrestamoViewModel> listaPromedioPrestamo = null;
            List<SelectListItem> Lista = new List<SelectListItem>();
            try
            {
                int valorLista = (int)PromedioPrestamoServicio.ListaPromedioPrestamo.List_Aliado;
                listaPromedioPrestamo = PromPrestamoServicio.ListarAliadoServicio(valorLista, promedio);

                Lista.Add(new SelectListItem { Text = Convert.ToString("Todos"), Value = Convert.ToString("") });
                foreach (var val in listaPromedioPrestamo)
                {
                    Lista.Add(new SelectListItem { Text = Convert.ToString(val.NomUser), Value = Convert.ToString(val.NomUser) });
                }
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(new SelectList(Lista, "Value", "Text", JsonRequestBehavior.AllowGet));
        }
        
        [HttpPost]
        public JsonResult TipoProducto(PromedioPrestamoViewModel promedio)
        {
            List<PromedioPrestamoViewModel> listaPromedioPrestamo = null;
            List<SelectListItem> Lista = new List<SelectListItem>();
            try
            {
                int valorLista = (int)PromedioPrestamoServicio.ListaPromedioPrestamo.List_TipoProducto;
                listaPromedioPrestamo = PromPrestamoServicio.ListarTipoProductoServicio(valorLista, promedio);

                Lista.Add(new SelectListItem { Text = Convert.ToString("Todos"), Value = Convert.ToString("") });
                foreach (var val in listaPromedioPrestamo)
                {
                    Lista.Add(new SelectListItem { Text = Convert.ToString(val.Tipo), Value = Convert.ToString(val.Tipo) });
                }
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(new SelectList(Lista, "Value", "Text", JsonRequestBehavior.AllowGet));
        }
        
        [HttpPost]
        public JsonResult Producto(PromedioPrestamoViewModel promedio)
        {
            List<PromedioPrestamoViewModel> listaPromedioPrestamo = null;
            List<SelectListItem> Lista = new List<SelectListItem>();
            try
            {
                int valorLista = (int)PromedioPrestamoServicio.ListaPromedioPrestamo.List_Producto;
                listaPromedioPrestamo = PromPrestamoServicio.ListarProductoServicio(valorLista, promedio);

                Lista.Add(new SelectListItem { Text = Convert.ToString("Todos"), Value = Convert.ToString("") });
                foreach (var val in listaPromedioPrestamo)
                {
                    Lista.Add(new SelectListItem { Text = Convert.ToString(val.Producto), Value = Convert.ToString(val.Producto) });
                }
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(new SelectList(Lista, "Value", "Text", JsonRequestBehavior.AllowGet));
        }

        [HttpPost]
        public JsonResult PromedioAnio(PromedioPrestamoViewModel promedio)
        {
            List<PromedioPrestamoViewModel> listaPromedioPrestamo = null;
            try
            {
                int valorLista = (int)PromedioPrestamoServicio.ListaPromedioPrestamo.List_PromedioAnio;
                listaPromedioPrestamo = PromPrestamoServicio.ListarPromedioAnioServicio(valorLista, promedio);
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(listaPromedioPrestamo, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult PromedioMes(PromedioPrestamoViewModel promedio)
        {
            List<PromedioPrestamoViewModel> listaPromedioPrestamo = null;
            try
            {
                int valorLista = (int)PromedioPrestamoServicio.ListaPromedioPrestamo.List_PromedioMes;
                listaPromedioPrestamo = PromPrestamoServicio.ListarPromedioMesServicio(valorLista, promedio);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(listaPromedioPrestamo, JsonRequestBehavior.AllowGet);
        }

        // GET: Matriz de indicadores de gestion
        //public ActionResult MatrizGestion()
        //{
        //    return View();
        //}


    }
}