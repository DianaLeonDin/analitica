﻿
using ENTIDADES.RH;
using SERVICIO.RH;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace SistemaBase.Controllers.RH
{
    public class IRPController : Controller
    {

        SERVICIO.RH.IRPServicio IRPservices = new SERVICIO.RH.IRPServicio();

        // GET: IRP
        public ActionResult Index()
        {
            //IRPView();
            // return View();
            List<IRPViewModel> listaIRP = null;
            try
            {
                int valorLista = (int)IRPServicio.ListaIRP.IRP;
                listaIRP = IRPservices.ListarIRPServicio(valorLista);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(listaIRP);
        }


        //public ActionResult prueb()
        //{
        //    // return View();
        //    List<IRPViewModel> listaIRP = null;
        //    try
        //    {
        //        int valorLista = (int)IRPServicio.ListaIRP.IRP;
        //        listaIRP = IRPservices.ListarIRPServicio(valorLista);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    return PartialView(listaIRP.ToList());
        //}

        public JsonResult IRP()
        {
            
            // return View();
            List<IRPViewModel> listaIRP = null;
            try
            {
                int valorLista = (int)IRPServicio.ListaIRP.IRP;
                listaIRP = IRPservices.ListarIRPServicio(valorLista);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //ViewBag.Semana = (new JavaScriptSerializer()).Serialize(listaIRP);
            //listaIRP.ToString()
            //return Json(ViewBag.Semana);

            var valor = (new JavaScriptSerializer().Serialize(listaIRP));
            return Json(valor);
           // return Json(listaIRP, JsonRequestBehavior.AllowGet);

        }
        public JsonResult IRPAlta()
        {
            List<IRPViewModel> listaIRP = null;
            try
            {
                int valorLista = (int)IRPServicio.ListaIRP.IRP_Alta;
                listaIRP = IRPservices.ListarIRPServicio(valorLista);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(listaIRP.ToList());
        }
        public JsonResult IRPBaja()
        {
            List<IRPViewModel> listaIRP = null;
            try
            {
                int valorLista = (int)IRPServicio.ListaIRP.IRP_Baja;
                listaIRP = IRPservices.ListarIRPServicio(valorLista);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(listaIRP.ToList());
        }
        public JsonResult IRPAcumulado()
        {
            List<IRPViewModel> listaIRP = null;
            try
            {
                int valorLista = (int)IRPServicio.ListaIRP.IRP_Acumulado;
                listaIRP = IRPservices.ListarIRPServicio(valorLista);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            //return Json(listaIRP.ToList());
            return Json(listaIRP);
        }



        




    }
}