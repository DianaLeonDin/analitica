﻿
using SistemaBase.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using ENTIDADES.Base;
using ENTIDADES.Login;

namespace SistemaBase.Controllers
{
    public class HomeController : Controller
    {


        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        //public async Task<ActionResult> entradas()
        //{
        //    //Check = new CheckServicio();

        //    //List<Entrada> entradasEmpobj = new List<Entrada>();
        //    //Task<List<Entrada>> entradasEmp = Check.consultarEntradas();
        //    //List<Entrada> entradaReal = await entradasEmp;

        //    return View();
        //}

    }
}