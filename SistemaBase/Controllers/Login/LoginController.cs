﻿
using DAO.Base;
using ENTIDADES.Base;
using ENTIDADES.Login;
using ENTIDADES.Permisos;
using Seguridad;
using SERVICIO.Login;
using SERVICIO.Seguridad;
using SistemaBase.Filters;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SistemaBase.Controllers.Login
{
    public class LoginController : Controller
    {

        IOperadorServicio _iOperadorServicio;

        Crypto crypto;
        
        

        UsuarioViewModel usuarioResponse = new UsuarioViewModel();
        LoginServicio Loginservicio = new LoginServicio();

        SERVICIO.Login.LoginServicio ServicioLogin = new LoginServicio();

        //SERVICIO.Conexion.ConexionServicio coonexionServicio = new SERVICIO.Conexion.ConexionServicio();

        //ENTIDADES.Conexion.ConexionSucursalViewModel conexionSucursal = new ConexionSucursalViewModel();
        //ENTIDADES.Conexion.ConexionBodegaViewModel conexionBodega = new ConexionBodegaViewModel();

        public LoginController(IOperadorServicio _iOperadorServicio)
        {
            this._iOperadorServicio = _iOperadorServicio;
        }

        // GET: Login
        [VerificaSession]
        public ActionResult Index()
        {
            return View();
        }


        [HttpPost]
        public JsonResult ConexionSucursal(string Sucursal)
        {

            try
            {
                //ConexionSucursalViewModel conexionModel = new ConexionSucursalViewModel();
                //conexionModel= Loginservicio.ConexionServicio(Sucursal);              
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json("");
        }



        [HttpPost]
        public ActionResult Index(string Usr, string Pass)
        {
            UsuarioViewModel LoginResponse = new UsuarioViewModel();
            UsuarioViewModel usuarioResponse = new UsuarioViewModel();
            MensajeTransaccion mensaje = null;

            try
            {
                //Session["usuario"] = usuarioResponse;
                usuarioResponse.CodigoAlterno = Usr;
                crypto = new Crypto();

                usuarioResponse.Password = crypto.Encriptar(Pass).ToString();
                //usuarioResponse.Password = crypto.Desencriptar(Pass).ToString();
                usuarioResponse = ServicioLogin.OperadoresCon(usuarioResponse);

                if (Usr.Length == 0 | Usr.Length == 0)
                {
                    ViewBag.MensajeRespuesta = "Llene todos los campos";
                    return View();
                }
               
                if (usuarioResponse.CodigoAlterno==null)
                {
                    ViewBag.MensajeRespuesta = "Usuario o contraseña incorrecto";
                    return View();
                }
                else
                {
                    Session["usuario"] = usuarioResponse;
                   // UsuarioViewModel usuarioLogin = (UsuarioViewModel)System.Web.HttpContext.Current.Session["usuario"];
                    LoginServicio LoginSer = new LoginServicio();
                    usuarioResponse.CveSucursal = "";
                    mensaje = LoginSer.AltaBitacoraAcceso(usuarioResponse);
                }
               
                return RedirectToAction("Index", "Home");               
            }
            catch (Exception ex)
            {
                throw ex;
            }
           
        }


        
     
        
        
        public ActionResult LogOut() {
            Session["usuario"] = null;
            return RedirectToAction("Index", "Login");
        }

        //[AutorizarSession(idOperacion: 2)]
        public ActionResult CambiarContraseña()
        {
            UsuarioViewModel usuarioLogin = (UsuarioViewModel)System.Web.HttpContext.Current.Session["usuario"];   
            if (usuarioLogin == null)
            {
                Session["usuario"] = null;
                return RedirectToAction("Index", "Login");
            }

            return View(usuarioLogin);
        }

        [HttpPost]
        public ActionResult CambiarContraseña(UsuarioViewModel usuarioResponse)
        {
            MensajeTransaccion mensaje = null;
            SERVICIO.Seguridad.ValidarPassword val = new ValidarPassword();

            bool Validar = new bool();

            //if (usuarioResponse == null)
            //{
            //    Session["usuario"] = null;
            //    return RedirectToAction("Index", "Login");
            //}
            //mensaje = _iOperadorServicio.ActualizarContraseña(usuarioResponse);
            //UsuarioViewModel usuarioLogin = (UsuarioViewModel)System.Web.HttpContext.Current.Session["usuario"];
            //usuarioLogin.codigoRespuesta = mensaje.CodigoRespuesta;
            //usuarioLogin.mensajeRespuesta = mensaje.MensajeRespuesta;
            //return View(usuarioLogin);

            UsuarioViewModel usuarioLogin = (UsuarioViewModel)System.Web.HttpContext.Current.Session["usuario"];


            if (usuarioResponse == null)
            {
                Session["usuario"] = null;
                return RedirectToAction("Index", "Login");
            }
            if (usuarioResponse.Password != usuarioResponse.ValidatePassword)
            {
                usuarioLogin.codigoRespuesta = "0001";
                usuarioLogin.mensajeRespuesta = string.Format("Las contraseñas no coinciden", Environment.NewLine);
            }
            else
            {
                //VALIDAR QUE LA CONTRASEÑA TENGA EL FORMATO CORRECTO
                Validar = val.Validar(usuarioResponse);

                //UsuarioViewModel usuarioLogin = (UsuarioViewModel)System.Web.HttpContext.Current.Session["usuario"];

                if (Validar == false)
                {
                    usuarioLogin.codigoRespuesta = "0002";
                    usuarioLogin.mensajeRespuesta = string.Format("Es obligatorio: {0}  * 8 Letras. {0} * 1 Mayúscula. {0} * 1 Minúscula. {0} * 1 Número", Environment.NewLine);
                }
                if (Validar == true)
                {
                    int registro=0;
                    //Validar que la contraseña sea la misma que se escriba con la sque se tiene en el servidor
                    using (var conn = new SqlConnection(BaseDAO.dbdinbd))
                    {
                        crypto = new Crypto();
                        //string ContraseniaActual = usuarioResponse.ActPassword;
                        usuarioResponse.ActPassword = crypto.Encriptar(usuarioResponse.ActPassword).ToString();

                        string query = "SELECT count(E.Em_Cve_Empleado) Usuario FROM DBDIN.dbo.Empleado AS E " +
                                       " INNER JOIN DBDIN.dbo.Puesto_Empleado as P ON E.Pe_Cve_Puesto_Empleado = P.Pe_Cve_Puesto_Empleado WHERE "
                                       + " E.Em_UserDef_1 = '"+ usuarioResponse.CodigoAlterno.Trim()+ "' AND Em_Password = '"+ usuarioResponse.ActPassword.Trim() + "' AND e.Es_Cve_Estado = 'AC' ";

                        SqlCommand cmds = new SqlCommand(query, conn);
                        conn.Open();
                        SqlDataReader reader = cmds.ExecuteReader();
                        if (reader.Read())
                        {
                            registro = int.Parse(reader["Usuario"].ToString().Trim());
                        }
                        reader.Close();
                    }

                    if (registro > 0)
                    {
                        //Actualizar la contraseña:
                        mensaje = _iOperadorServicio.ActualizarContraseña(usuarioResponse);
                    }
                    else
                    {
                        mensaje = new MensajeTransaccion();
                        mensaje.CodigoRespuesta = "La contraseña actual es incorrecta, intente de nuevo";
                    }
                    
                    usuarioLogin.codigoRespuesta = mensaje.CodigoRespuesta;
                    usuarioLogin.mensajeRespuesta = mensaje.MensajeRespuesta;
                }
            }



            return View(usuarioLogin);
        }


        //        



    }
}
