﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SistemaBase.Controllers.Login
{
    public class ErrorController : Controller
    {
        // GET: Error
        public ActionResult Index(string mensaje)
        {
            ViewBag.mensajeError = mensaje;
            return View();
        }
    }
}