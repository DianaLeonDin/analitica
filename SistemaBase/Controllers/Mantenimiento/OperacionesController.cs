﻿using DevExpress.Web.Mvc;
using ENTIDADES.Base;
using ENTIDADES.Mantenimiento;
using ENTIDADES.Operaciones;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SERVICIO.Mantenimiento;
using SERVICIO.Operaciones;
using SistemaBase.Controllers.Operaciones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using static ENTIDADES.Mantenimiento.OperacionesViewModel;

namespace SistemaBase.Controllers.Mantenimiento
{
    public class OperacionesController : Controller
    {

        SERVICIO.Mantenimiento.OperacionesServicio OperacionService = new SERVICIO.Mantenimiento.OperacionesServicio();
        SERVICIO.Operaciones.PromedioPrestamoServicio PromedioPService = new SERVICIO.Operaciones.PromedioPrestamoServicio();

        SERVICIO.Operaciones.MatrizGestionServicio MatrizGestionService = new SERVICIO.Operaciones.MatrizGestionServicio();
        

        // GET: Operaciones
        public ActionResult Index()
        {
            return View();
        }

        //catalogo manteniemiento para la base de matriz de gestion.
        public ActionResult MantMatrizGestion(OperacionesViewModel operaciones)
        {
            List<OperacionesViewModel> lista = null;
            try
            {
                int valorLista = (int)OperacionesServicio.Lista.Lista_BaseAll;
                lista = OperacionService.ListarBaseServicio(operaciones,valorLista);
            }
            catch (Exception)
            {
                throw;
            }
            return View(lista);
        }

        public JsonResult Sucursal()
        {
            List<PromedioPrestamoViewModel> listaPromedioPrestamo = null;
            List<SelectListItem> Lista = new List<SelectListItem>();
            try
            {
                int valorLista = (int)PromedioPrestamoServicio.ListaPromedioPrestamo.List_Suc;
                listaPromedioPrestamo = PromedioPService.ListarNomSucServicio(valorLista);
                foreach (var val in listaPromedioPrestamo)
                {
                    Lista.Add(new SelectListItem { Text = Convert.ToString(val.NomSuc), Value = Convert.ToString(val.NomSuc) });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            ViewBag.Sucursal = Lista;
            return Json(new SelectList(Lista, "Value", "Text", JsonRequestBehavior.AllowGet));
        }
        [HttpPost]
        //Alta de nueva base de la matriz de gestion en el catalogo de operaciones.
        public JsonResult AltaMatrizGestionBase(OperacionesViewModel MatrizGestion)
        {

            MensajeTransaccion mensaje = null;

            try
            {
                //mensaje = ProductoSer.AltaProductoServicio(producto, usuarioResponse);
                mensaje = OperacionService.AltaProductoServicio(MatrizGestion);
            }
            catch (Exception e)
            {
                if (mensaje == null)
                {
                    mensaje = new MensajeTransaccion();
                    mensaje.CodigoRespuesta = "CC-000001";
                    mensaje.MensajeRespuesta = "Ocurrió un problema al guardar la base de la matriz de indicadores de gestión. " + e.Message;
                }
            }
            //ViewBag.mensajeRespuesta = mensaje.MensajeRespuesta;
            return Json(mensaje.MensajeRespuesta);

            
        }

        //public JsonResult Aliado()
        //{
        //    List<MatrizGestionViewModel> listaAliado = null;
        //    List<SelectListItem> Lista = new List<SelectListItem>();
        //    MatrizGestionViewModel matrix = new MatrizGestionViewModel();
        //    try
        //    {
        //        int valorLista = (int)MatrizGestionServicio.ListaMatrizGestion.Lista_Aliado;
        //        listaAliado = MatrizGestionService.ListadoServicio(valorLista, matrix);

        //        Lista.Add(new SelectListItem { Text = Convert.ToString("Todos"), Value = Convert.ToString("Todos") });
        //        foreach (var val in listaAliado)
        //        {
        //            Lista.Add(new SelectListItem { Text = Convert.ToString(val.nombre), Value = Convert.ToString(val.nombre) });
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    ViewBag.Aliado =Lista;
        //    return Json(new SelectList(Lista, "Value", "Text", JsonRequestBehavior.AllowGet));
        //}
        
        public JsonResult NombreBase()
        {
            
            List<SelectListItem> ListaTR = new List<SelectListItem>();
            List<OperacionesViewModel> op = new List<OperacionesViewModel>();
            
            ListaTR.Add(new SelectListItem { Text = Convert.ToString("Prestamo promedio"), Value = Convert.ToString("Prestamo promedio") });
            ListaTR.Add(new SelectListItem { Text = Convert.ToString("Porcentaje máximo de enajenación"), Value = Convert.ToString("Porcentaje máximo de enajenación") });
            //ListaTR.Add(new SelectListItem { Text = Convert.ToString("Cartera (aparatos)"), Value = Convert.ToString("Cartera (aparatos)") });
            //ListaTR.Add(new SelectListItem { Text = Convert.ToString("Cartera (celulares)"), Value = Convert.ToString("Cartera (celulares)") });
            //ListaTR.Add(new SelectListItem { Text = Convert.ToString("Cartera (joyería)"), Value = Convert.ToString("Cartera (joyería)") });
            ListaTR.Add(new SelectListItem { Text = Convert.ToString("Cartera total"), Value = Convert.ToString("Cartera total") });
            ListaTR.Add(new SelectListItem { Text = Convert.ToString("Capital prestado"), Value = Convert.ToString("Capital prestado") });
            ListaTR.Add(new SelectListItem { Text = Convert.ToString("Ingresos (empeño)"), Value = Convert.ToString("Ingresos (empeño)") });
            ListaTR.Add(new SelectListItem { Text = Convert.ToString("Clientes nuevos empeños"), Value = Convert.ToString("Clientes nuevos empeños") });
            ListaTR.Add(new SelectListItem { Text = Convert.ToString("Clientes nuevos ventas"), Value = Convert.ToString("Clientes nuevos ventas") });
            ListaTR.Add(new SelectListItem { Text = Convert.ToString("Ingreso venta (aparatos)"), Value = Convert.ToString("Ingreso venta (aparatos)") });
            ListaTR.Add(new SelectListItem { Text = Convert.ToString("Ingreso venta (celulares)"), Value = Convert.ToString("Ingreso venta (celulares)") });
            ListaTR.Add(new SelectListItem { Text = Convert.ToString("Ingreso venta (joyería)"), Value = Convert.ToString("Ingreso venta (joyería)") });
            //ListaTR.Add(new SelectListItem { Text = Convert.ToString("Ingreso total (empeños - ventas)"), Value = Convert.ToString("Ingreso total (empeños - ventas)") });
            
            ViewBag.NombreBase = new SelectList(ListaTR, "Value", "Text");
            return Json(new SelectList(ListaTR, "Value", "Text", JsonRequestBehavior.AllowGet));
        }

        //Busqueda de la base por medio de su clave(catalogo mantenimiento base del Indicador de la matrz de indicadores de gestion)
        public ActionResult OpcionesBase(int BaseID)
        {
            OperacionesViewModel BaseCon = null;

            try
            {
                NombreBase();
                //Sucursal();
                //CONSULTA DE TODOS LOS PARAMETROS
                var valorCon = (int)OperacionesServicio.Consulta.Con_Base;                
                BaseCon = OperacionService.ConsultaBase(valorCon, BaseID);               
            }
            catch(Exception ex)
            {
                throw ex;
            }
            return PartialView(BaseCon);
        }


        //Metodo para actualizar la base
        [HttpPost]
        public JsonResult ActualizarBase(OperacionesViewModel mantenimientoBase)
        {
            MensajeTransaccion mensaje = null;
            try
            {
               
                int valor = (int)OperacionesServicio.Actualizar.Act_Base;
                mensaje = OperacionService.ActualizarBaseServicio(mantenimientoBase, valor);

            }
            catch (Exception e)
            {
                if (mensaje == null)
                {
                    mensaje = new MensajeTransaccion();
                    mensaje.CodigoRespuesta = "CC-000002";
                    mensaje.MensajeRespuesta = "Ocurrió un problema al modificar la base. " + e.Message;
                }
            }
            return Json(mensaje.MensajeRespuesta);
        }
        
        public ActionResult ManDiaHabil()
        {
            List<OperacionesViewModel> lista = null;
            try
            {
                int valorLista = (int)OperacionesServicio.Lista.Lista_DiasHabiles;
                lista = OperacionService.ListarDiaHabilServicio(valorLista);
            }
            catch (Exception)
            {
                throw;
            }
            return View(lista);
        }

        //Alta de nuevO dia habil de la matriz de gestion en el catalogo de operaciones.
        public JsonResult AltaDiaHabil(OperacionesViewModel MatrizGestion)
        {

            MensajeTransaccion mensaje = null;

            try
            {
                //mensaje = ProductoSer.AltaProductoServicio(producto, usuarioResponse);
                mensaje = OperacionService.AltaDiaHabilServicio(MatrizGestion);
            }
            catch (Exception e)
            {
                if (mensaje == null)
                {
                    mensaje = new MensajeTransaccion();
                    mensaje.CodigoRespuesta = "CC-000002";
                    mensaje.MensajeRespuesta = "Ocurrió un problema al guardar el dia habil de la matriz de indicadores de gestión. " + e.Message;
                }
            }
            //ViewBag.mensajeRespuesta = mensaje.MensajeRespuesta;
            return Json(mensaje.MensajeRespuesta);


        }

        //Consulta de dias habiles para visuali¿zar en la vista principal  de la matriz de indicadores de gestion.
        public JsonResult ConsultaDiaHabil(OperacionesViewModel Operacion)
        {
            OperacionesViewModel dia = null;
            try
            {
                int valorLista = (int)OperacionesServicio.Consulta.Con_DiaHabil;
                dia = OperacionService.ConsultaDiasHAbilesSjuc(Operacion,valorLista);
            }
            catch (Exception)
            {
                throw;
            }
            return Json(dia, JsonRequestBehavior.AllowGet);
        }

        //Consulta de dias habiles del  mes anterior (pasandoles un mes y un año
        public JsonResult ConsultaDHabilMes(OperacionesViewModel Operacion)
        {
            OperacionesViewModel dia = null;
            try
            {
                int valorLista = (int)OperacionesServicio.Consulta.Con_ConDHMes;
                dia = OperacionService.ConsultaDiasHAbilesSjuc(Operacion, valorLista);
            }
            catch (Exception)
            {
                throw;
            }
            return Json(dia, JsonRequestBehavior.AllowGet);
        }
        
        public ActionResult Prueba()
        {
            //        List<SelectListItem> CountryList = new List<SelectListItem>
            //{
            //    new SelectListItem{Text="India",Value="1"},
            //    new SelectListItem{Text="United States",Value="2"},
            //    new SelectListItem{Text="Australia",Value="3"},
            //    new SelectListItem{Text="South Africa",Value="4"},
            //    new SelectListItem{Text="China",Value="5"}
            //};

            //        ViewBag.CountryList = CountryList;

            //        return View();

            List<SelectListItem> CountryList = new List<SelectListItem>
    {
        new SelectListItem{Text="India",Value="1"},
        new SelectListItem{Text="United States",Value="2"},
        new SelectListItem{Text="Australia",Value="3"},
        new SelectListItem{Text="South Africa",Value="4"},
        new SelectListItem{Text="China",Value="5"}
    };

            ViewBag.CountryList = CountryList;
            return View();
        }

        
        
        //Busqueda del dia habil por medio de su clave(catalogo mantenimiento del dia habil del Indicador de la matrz de indicadores de gestion)
        //ELIMINAR LOS DIAS HABILES.
        public ActionResult OpcionesDiaHabil(OperacionesViewModel Operacions)
        {
            OperacionesViewModel BaseCon = null;

            try
            {
              
                BaseCon = new OperacionesViewModel();
                var valorCon = (int)OperacionesServicio.Consulta.Con_ConDH;

                // int tam_var = Operacions.mesAnio.Length;
                //string año = Operacions.mesAnio.Substring((tam_var - 4), 4);
                // Operacions.Anio =int.Parse(año);

                string[] valores = Operacions.mesAnio.Split(' ');
                Operacions.Anio = int.Parse(valores[1]);

                string mes = valores[0];

                switch (mes)
                {
                    case "Enero":
                        Operacions.Mes = 1;
                        break;
                    case "Febero":
                        Operacions.Mes = 2;
                        break;
                    
                }


                BaseCon = OperacionService.ConsultaDiasHAbiles(Operacions, valorCon);
                Sucursal();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return PartialView(BaseCon);
        }
        
        //Metodo para ELIMINAR dia habil
        [HttpPost]
        public JsonResult ActualizarDiaHabil(OperacionesViewModel mantenimientoBase)
        {
            MensajeTransaccion mensaje = null;
            try
            {

                int valor = (int)OperacionesServicio.Actualizar.Act_DiaHabil;
                mensaje = OperacionService.ActualizarDiaHabilServicio(mantenimientoBase, valor);

            }
            catch (Exception e)
            {
                if (mensaje == null)
                {
                    mensaje = new MensajeTransaccion();
                    mensaje.CodigoRespuesta = "CC-000003";
                    mensaje.MensajeRespuesta = "Ocurrió un problema al elimianr el dia habil. " + e.Message;
                }
            }
            return Json(mensaje.MensajeRespuesta);
        }



      


    }
}