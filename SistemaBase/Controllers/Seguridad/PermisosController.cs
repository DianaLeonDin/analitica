﻿using ENTIDADES.Base;
using ENTIDADES.Login;
using ENTIDADES.Seguridad;
using SERVICIO.Seguridad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace SistemaBase.Controllers.Seguridad
{
    public class PermisosController : Controller
    {
        SERVICIO.Seguridad.PermisoEmpleadoServicio ServicioSeguridad = new SERVICIO.Seguridad.PermisoEmpleadoServicio();

        // GET: Seguridad
        public ActionResult Index()
        {

            List<TreeViewNode> nodes = new List<TreeViewNode>();    
            List<PermisoEmpleadoViewModel> lista = null;

            AccesoViewModel listaModel = new AccesoViewModel();

            try
            {
                int Lista_PermisoRol = (int)PermisoEmpleadoServicio.Acceso.Lista_PermisoRol;
                int Lista_PermisoMod = (int)PermisoEmpleadoServicio.Acceso.Lista_PermisoMod;
                listaModel = ServicioSeguridad.PuestosPermisoServicio(Lista_PermisoRol, Lista_PermisoMod);

                //Bucle para agregar los nodos principales ROL, PUETOS
                foreach (PermisoEmpleadoViewModel rol in listaModel.Rol)
                {
                    nodes.Add(new TreeViewNode { id = rol.PermisoID.ToString(), parent = "#", text = rol.Modulo });
                }
                //Bucle para agregar los nodos hijos //cATALOGOS//Modulos
                foreach (PermisoEmpleadoViewModel modulo in listaModel.Modulo)
                {
                    //nodes.Add(new TreeViewNode { id = modulo.PermisoID.ToString() + "-" + modulo.Nombre.ToString(), parent = modulo.RolID.ToString(), text = modulo.Modulo });
                    nodes.Add(new TreeViewNode { id = modulo.PermisoID.ToString() + "-" + modulo.Modulo.ToString(), parent = modulo.RolID.ToString(), text = modulo.Modulo });
                }
                //Serializar en cadena Json
                ViewBag.Json = (new JavaScriptSerializer()).Serialize(nodes);
                
            }
            catch (Exception)
            {
                throw;
            }
            
            return View();
        }

        [HttpPost]
        public ActionResult Index(string selectedItems)
        {
            List<TreeViewNode> items = (new JavaScriptSerializer()).Deserialize<List<TreeViewNode>>(selectedItems);
            return RedirectToAction("Index");
        }


        public JsonResult RolPermiso()
        {
            AccesoViewModel acceso = null;

            try
            {

                //var valorPer = (int)PermisoEmpleadoServicio.Acceso.Lista_Permiso;
                //var valorMod = (int)PermisoEmpleadoServicio.Acceso.Lista_Modulo;
                //var valorPue = (int)PermisoEmpleadoServicio.Acceso.Lista_Puesto;

                var valorPer = (int)PermisoEmpleadoServicio.Acceso.Lista_Permiso;
                var valorMod = (int)PermisoEmpleadoServicio.Acceso.Lista_Modulo;
                var valorPue = (int)PermisoEmpleadoServicio.Acceso.Lista_Puesto;


                acceso = ServicioSeguridad.AccesoServicio(valorPer, valorMod, valorPue);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(acceso);
        }


        //Lleva el control de quien inicie sesion
        public ActionResult ControlLogin()
        {
            List<PermisoEmpleadoViewModel> lista = null;
            PermisoEmpleadoViewModel permiso = new PermisoEmpleadoViewModel();
            try
            {
                int valorLista = (int)PermisoEmpleadoServicio.Acceso.Lista_AccesoLogin;
                lista = ServicioSeguridad.ControlAccesoServicio(permiso, valorLista);
            }
            catch (Exception)
            {
                throw;
            }
            return View(lista);
            //return View();
        }

        public ActionResult ControlIndicadorOperaciones()
        {
            List<PermisoEmpleadoViewModel> lista = null;
            lista = new List<PermisoEmpleadoViewModel>();
            return View(lista);
        }

        //LLeva control, cuando se selecciona la sucursal en los indicadores de operaciones
        [HttpPost]
        public ActionResult ControlIndicadorOperaciones(PermisoEmpleadoViewModel permiso)
        {
            List<PermisoEmpleadoViewModel> lista = null;
            try
            {
                int valorLista = (int)PermisoEmpleadoServicio.Acceso.Lista_AccesoOperacion;
                lista = ServicioSeguridad.ControlAccesoServicio(permiso, valorLista);
            }
            catch (Exception)
            {
                throw;
            }
            return PartialView("gridControlIndOperacion",lista);

        }
        
        //Año, Mes y sucursal 
        public JsonResult Control()
        {
            List<PermisoEmpleadoViewModel> lista = null;

            AccesoViewModel listaControlModel = new AccesoViewModel();

            try
            {
                int Lista_Anio = (int)PermisoEmpleadoServicio.Acceso.Lista_Anio;
                int Lista_Mes = (int)PermisoEmpleadoServicio.Acceso.Lista_Mes;
                int Lista_Sucursal = (int)PermisoEmpleadoServicio.Acceso.Lista_Sucursal;


                listaControlModel = ServicioSeguridad.ControlServicio(Lista_Anio, Lista_Mes, Lista_Sucursal);


            }
            catch (Exception)
            {
                throw;
            }
            
            return Json(listaControlModel, JsonRequestBehavior.AllowGet);
        }
        
        //Agregar permiso por medio del puesto
        public JsonResult AddAccess(PermisoEmpleadoViewModel permisoModel)
                {

                    MensajeTransaccion mensaje = null;
                    try
                    {
                        mensaje = ServicioSeguridad.AltaPermisoServicio(permisoModel);
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    return Json(mensaje.MensajeRespuesta);
                    //return View();
                }

        //Año, Mes y sucursal 
        public ActionResult PermisoOperaciones()
        {
            
            List<TreeViewNode> nodes = new List<TreeViewNode>();
            List<PermisoEmpleadoViewModel> lista = null;

            AccesoViewModel listaModel = new AccesoViewModel();

            try
            {
                //acceso.Rol = ListadoPermisoEmpleado.ListaTreeEmpDAO(emp);
                //acceso.Modulo = ListadoPermisoEmpleado.ListaTreeEmpSucDAO(modulo);

                int Lista_PermisoMod = (int)PermisoEmpleadoServicio.Acceso.Lista_EmpSuc;
                int Lista_PermisoEmp = (int)PermisoEmpleadoServicio.Acceso.Lista_Emp;

                listaModel = ServicioSeguridad.TreePuestosPermisoServicio(Lista_PermisoMod, Lista_PermisoEmp);

                //Bucle para agregar los nodos principales ROL, PUETOS
                foreach (PermisoEmpleadoViewModel rol in listaModel.Rol)
                {
                    nodes.Add(new TreeViewNode { id = rol.Usuario.ToString(), parent = "#", text = rol.Usuario });
                }
                //Bucle para agregar los nodos hijos //cATALOGOS//Modulos
                foreach (PermisoEmpleadoViewModel modulo in listaModel.Modulo)
                {
                    //nodes.Add(new TreeViewNode { id = modulo.PermisoID.ToString() + "-" + modulo.Nombre.ToString(), parent = modulo.RolID.ToString(), text = modulo.Modulo });
                    nodes.Add(new TreeViewNode { id = modulo.SucursalID.ToString() + "-" + modulo.Sucursal.ToString(), parent = modulo.Usuario.ToString(), text = modulo.Sucursal });
                }
                //Serializar en cadena Json
                ViewBag.Json = (new JavaScriptSerializer()).Serialize(nodes);

            }
            catch (Exception ex)
            {
                throw ex;
            }


            return View();
        }

        //PUESTO, EMPLEADO 
        public JsonResult PuestoOperacionesInd()
        {
            //AccesoViewModel acceso = null;
            List<PermisoEmpleadoViewModel> acceso = null;
            try
            {
                var valorPto = (int)PermisoEmpleadoServicio.Acceso.Lista_PuestoDBDIN;

                acceso = new List<PermisoEmpleadoViewModel>();
                acceso = ServicioSeguridad.PuestoPermisoServicio(valorPto);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(acceso);
        }
        
        public JsonResult EmpleadoOperacionesInd(string puestoID)
        {
            //AccesoViewModel acceso = null;
            List<PermisoEmpleadoViewModel> acceso = null;
            try
            {
                var valorEdo = (int)PermisoEmpleadoServicio.Acceso.Lista_EmpleadoDBDIN;
                //var valorSal = (int)PermisoEmpleadoServicio.Acceso.Lista_SucSucursalDBDIN;
                acceso = new List<PermisoEmpleadoViewModel>();
                acceso = ServicioSeguridad.EmpleadoPermisoServicio(valorEdo, puestoID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(acceso);
        }

        public JsonResult AltaEmpSuc(PermisoEmpleadoViewModel permisoModel)
        {

            MensajeTransaccion mensaje = null;
            try
            {
                mensaje = ServicioSeguridad.AltaEmpSucServicio(permisoModel);
            }
            catch (Exception)
            {
                throw;
            }
            return Json(mensaje.MensajeRespuesta);
            //return View();
        }
        //Elimina al usuario con las sucursales, (permisos para que usuario pueda ver las sucursales en la pantalla principal)
        public JsonResult DeletePermiso(UsuarioViewModel usr)
        {

            MensajeTransaccion mensaje = null;
            try
            {
                int valor  = (int)PermisoEmpleadoServicio.Acceso.Eliminar_Permiso;
                mensaje = ServicioSeguridad.DeletePermisoServicio(usr, valor);
            }
            catch (Exception)
            {
                throw;
            }
            return Json(mensaje.MensajeRespuesta);
            //return View();
        }
        //Elimina los permisos por puesto, el puesto del empleado no puede ver todas las vistas
        public JsonResult DeletePermisoLog(UsuarioViewModel usr)
        {

            MensajeTransaccion mensaje = null;
            try
            {
                int valor = (int)PermisoEmpleadoServicio.Acceso.Elimina_Login;
                mensaje = ServicioSeguridad.DeleteLogServicio(usr, valor);
            }
            catch (Exception)
            {
                throw;
            }
            return Json(mensaje.MensajeRespuesta);
            //return View();
        }


    }
}