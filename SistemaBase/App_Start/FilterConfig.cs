﻿using System.Web;
using System.Web.Mvc;

namespace SistemaBase
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new Filters.VerificaSession());
            filters.Add(new Filters.DeleteFileAttribute());
            filters.Add(new Filters.VerificaSession());
        }
    }
}
