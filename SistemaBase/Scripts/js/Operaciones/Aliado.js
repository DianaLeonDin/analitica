﻿
//Matriz de indicadores de gestion por aliado.
function ViewAliado() {

    //DIAS HABILES
    var diasArray = [];
    //archivo js
    var diasArray = diasjs();

    var DHabiles = $("#txtDiasHabilesAliado").val(diasArray[0]);
    var DTrans = $("#txtDiasTranscAliado").val(diasArray[1]);
    //dias restantes
    $("#txtDiasRestantesAliado").val(diasArray[0] - diasArray[1])


    //SUCURSAL
    $.ajax({
        url: "../MatrizGestion/Sucursal",
        //type: 'POST',
        method: "POST",
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        success: function (SucursalAliado) {

            console.log(SucursalAliado);

            $.each(SucursalAliado, function (i, SucursalAliado) {
                $("#SucAliado").append('<option value="' + SucursalAliado.Value + '">' + SucursalAliado.Text + '</option>');
            });

        },
    });





    //ALIADO POR SUCURSAL
    $("#SucAliado").change(function () {
        //LIMPIAR EL COMBO DE ALIADO PARA CUANDO VUELVA  A ELEGIR
        $("#AliadoAliado").empty();

        //var NomSuc = $("#SucAliado").val();
        document.getElementById('TimeAliado').style.display = 'inline';

        //Sacar el nombre de la sucursal
        var combo = document.getElementById("SucAliado");
        var NomSuc = combo.options[combo.selectedIndex].text;
        //CVe sucursal para conectar al servidor de empeños y buscar lis aliados
        var CveSuc = combo.options[combo.selectedIndex].value;

        $.ajax({
            url: "../MatrizGestion/Aliado",
            type: 'POST',
            dataType: 'json',
            data: { NomSuc, CveSuc},
            success: function (AliadoAliado) {
                dHabilAliado();

                $.each(AliadoAliado, function (i, AliadoAliado) {
                    $("#AliadoAliado").append('<option value="' + AliadoAliado.Value + '">' + AliadoAliado.Text + '</option>');
                });
                document.getElementById('TimeAliado').style.display = 'none';
            },
        })
    });

    //CARGAR INDICADORES
    $("#AliadoAliado").change(function () {
        // var nombre = $("#AliadoAliado").val();      
        document.getElementById('containerAliado').style.display = 'inline';

        limpiarAliado();

        var checkBox = document.getElementById("MesAnteriorAliado");
        //Validar si esta marcado el checkbox
        //Si no esta marcado buscar del mes actual
        //Si esta marcado buscar el mes anterior


        //Cve sucursal para saber la conexion
        //Sacar el texto de un combo
        var CveSuc = document.getElementById("SucAliado").value;

        //var NomSuc = combo.options[combo.selectedIndex].text;
        var combo = document.getElementById("SucAliado");
        var NomSuc = combo.options[combo.selectedIndex].text;
        //var CveUser = document.getElementById("AliadoAliado").value;

        var Cusr = document.getElementById("AliadoAliado").value;
        //IndicadorAliado

        //Sacar el nombre de un combo
        //var combo = document.getElementById("SucAliado");
        //var CveSuc = combo.options[combo.selectedIndex].text;
        var date = new Date();
        var actual = new Date(date.getFullYear(), date.getMonth() + 1, 0);
        //FECHA SE LE QUITA UN MES ANTERIOR (LA FECHA EMPIEZA ES "eNERO-0, FEBRERO-2..")
        //Y SE LE SUMA 1 PARA YA TENER LA FECHA CORRECTA
        var anterior = new Date(date.getFullYear(), (date.getMonth() - 1) + 1, 0);

        var Anio = anterior.getFullYear();
        var Mes = anterior.getMonth() + 1;



        if (checkBox.checked == false) {
            $.ajax({
                url: "../MatrizGestion/IndicadorAliado",
                type: 'POST',
                dataType: 'json',
                data: { CveSuc, Cusr, NomSuc},
                success: function (indicador) {
                    //alert(indicador);

                    $("#txtPEmpRealAliado").val(formatNumber(indicador.PromedioSem));
                    //CAPITAL PRESTADO
                    if (indicador.Cartera != 0) {
                        $("#txtCPresAliadoReal").val(formatNumber(indicador.Cartera));
                        //tendencia
                        $("#txtCPTendenciaAliado").val(formatNumber(tendenciaAliado(indicador.Cartera)));

                    } else {
                        $("#txtCPresAliadoReal").val((0).toFixed(2));
                        //tendencia
                        $("#txtCPTendenciaAliado").val((0).toFixed(2));
                    }
                    //CLIENTES NUEVOS        
                    if (indicador.TotalCteNvo != 0) {
                        //$("#txtClieNuevAliadoReal").val(formatNumber(indicador.TotalCteNvo));
                        $("#txtClieNuevAliadoReal").val(indicador.TotalCteNvo);
                        //tendencia
                        $("#txtCNTendenciaAliado").val(formatNumber(tendenciaAliado(indicador.TotalCteNvo)));
                    } else {
                        $("#txtClieNuevAliadoReal").val(0);
                        //tendencia
                        $("#txtCNTendenciaAliado").val((0).toFixed(2));
                    }
                    //INGRESO EMPEÑOS
                    if (indicador.ingreso != 0) {
                        $("#txtIERealAliado").val(formatNumber(indicador.ingreso));
                        //tendencia
                        $("#txtIETendenciaAliado").val(formatNumber(tendenciaAliado(indicador.ingreso)));
                    } else {
                        $("#txtIERealAliado").val((0).toFixed(2));
                        //tendencia
                        $("#txtIETendenciaAliado").val((0).toFixed(2));
                    }
                    //INGRESO VENTA APARATOS, CELULARES Y JOYERIA , TOTALES (EMPEÑO-VENTA)

                    if (indicador.IngresoVenta != null) {
                        //CARGAR LO REAL --Ingreso de venta (Aparato, joyeria y celulares)
                        indicador.IngresoVenta.forEach(IngVta)
                        function IngVta(item, index) {

                            //if (item.Tipo.trim() == "Aparatos") {
                            //    //var realIngVteApa = $("#txtIngVtaApaReal").val(item.Ingreso.toFixed(2));
                            //    var resIVA = formatNumber(item.Ingreso);
                            //    if (resIVA != 0) {
                            //        var realIV = $("#txtIVApaAliadoReal").val(resIVA);
                            //        //tendencia
                            //        $("#txtIVApaAliadoTendencia").val(formatNumber(tendenciaAliado(item.Ingreso)));
                            //    } else {
                            //        var realIV = $("#txtIVApaAliadoReal").val(0);
                            //    }
                            //}
                            //if (item.Tipo.trim() == "Celulares") {

                            //    //var realIngVteCel = $("#txtIngVtaCelReal").val(item.Ingreso.toFixed(2));
                            //    var resIVC = formatNumber(item.Ingreso);
                            //    if (resIVC != 0) {
                            //        var reaIC = $("#txtIVCelAliadoReal").val(resIVC);
                            //        //tendencia
                            //        $("#txtIVCelAliadoTendencia").val(formatNumber(tendenciaAliado(item.Ingreso)));
                            //    } else {
                            //        var reaIC = $("#txtIVCelAliadoReal").val(0);
                            //    }

                            //}
                            //if (item.Tipo.trim() == "Joyeria") {

                            //    //var realIngVteJoy = $("#txtIngVtaJoyReal").val(item.Ingreso.toFixed(2));
                            //    var resIVJ = formatNumber(item.Ingreso);
                            //    if (resIVJ != 0) {
                            //        var reaIJ = $("#txtIVJoyAliadoReal").val(resIVJ);
                            //        //tendencia
                            //        $("#txtIVJoyAliadoTendencia").val(formatNumber(tendenciaAliado(item.Ingreso)));
                            //    } else {
                            //        var reaIJ = $("#txtIVJoyAliadoReal").val(0);
                            //    }
                            //}
                            //Ingreso ----- Ingreso aparatos, celulares y joyeria
                            if (item.Tipo.trim() == "Total") {
                                //var realIngVteJoy = $("#txtIngVtaJoyReal").val(item.Ingreso.toFixed(2));
                                var resITACJ = formatNumber(item.Ingreso);
                                if (resITACJ != 0) {
                                    var resITACJ = $("#txtIACJAliadoReal").val(resITACJ);
                                    //tendencia
                                    $("#txtIACJAliadoTendencia").val(formatNumber(tendenciaAliado(item.Ingreso)));
                                } else {
                                    var resITACJ = $("#txtIACJAliadoReal").val(0);
                                }
                            }

                        }

                        } 
                    //else {

                    //    //var realIV = $("#txtIVApaAliadoReal").val((0).toFixed(2));
                    //    //var reaIC = $("#txtIVCelAliadoReal").val((0).toFixed(2));
                    //    //var reaIJ = $("#txtIVJoyAliadoReal").val((0).toFixed(2));
                    //    $("#txtIACJAliadoReal").val("0.00");
                    //}

                    //Suma de Ingresos totalempeños y ventas
                    var ingresoE = $("#txtIERealAliado").val();
                    var IngresoACJ = $("#txtIACJAliadoReal").val();

                    if (ingresoE == "") {
                        ingresoE = "0.00";
                    }
                    if (IngresoACJ=="") {
                        IngresoACJ = "0.00"
                    }
                    var suma = parseFloat(ingresoE.replace(/,/g, "")) + parseFloat(IngresoACJ.replace(/,/g, ""));

                    //REAL de Ingreso total (empeños - venta)
                    $("#txtITAliadoReal").val(formatNumber(suma));
                    $("#txtITAliadoTendencia").val(formatNumber(tendenciaAliado(suma)));


                    //if ($("#txtIVApaAliadoReal").val() == "") {
                    //    $("#txtIVApaAliadoReal").val((0).toFixed(2));
                    //    $("#txtIVApaAliadoTendencia").val((0).toFixed(2));
                    //}
                    //if ($("#txtIVCelAliadoReal").val() == "") {
                    //    $("#txtIVCelAliadoReal").val((0).toFixed(2));
                    //    $("#txtIVCelAliadoTendencia").val((0).toFixed(2));
                    //}
                    //if ($("#txtIVJoyAliadoReal").val() == "") {
                    //    $("#txtIVJoyAliadoReal").val((0).toFixed(2));
                    //    $("#txtIVJoyAliadoTendencia").val((0).toFixed(2));
                    //}
                    //if ($("#txtITAliadoReal").val() == "") {
                    //    $("#txtITAliadoReal").val((0).toFixed(2));
                    //    $("#txtITAliadoTendencia").val((0).toFixed(2));
                    //} 
                    if ($("#txtIACJAliadoReal").val() == "") {
                        $("#txtIACJAliadoReal").val((0).toFixed(2));
                        $("#txtIACJAliadoTendencia").val((0).toFixed(2));
                    }
                    ////INGRESO APARTADOS , CELULARES Y JOYERIA 
                    //var RealApa = parseFloat($("#txtIVApaAliadoReal").val().replace(/,/g, ""));
                    //var RealCel = parseFloat($("#txtIVCelAliadoReal").val().replace(/,/g, ""));
                    //var RealJoy = parseFloat($("#txtIVJoyAliadoReal").val().replace(/,/g, ""));
                    //var suma = RealApa + RealCel + RealJoy;
                    ////$("#txtIACJAliadoReal").val(formatNumber(suma));


                    //if (suma != 0) {
                    //    var respuesta = $("#txtIACJAliadoReal").val(formatNumber(parseFloat(suma)));
                    //    $("#txtIACJAliadoTendencia").val(formatNumber(tendenciaAliado(suma)));
                    //} else {
                    //    var respuesta = parseFloat($("#txtIACJAliadoReal").val(suma.toFixed(2)));
                    //    $("#txtIACJAliadoTendencia").val(suma.toFixed(2));
                    //}


                    //meta(indicador.ValorMeta);
                    indicador.ValorMeta.forEach(metaItem);
                    function metaItem(item, index) {

                        if (item.NomIndicador.trim() == $("#MetaPPAliado").val()) {
                            $("#txtPPBaseAliado").val(formatNumber(item.meta));

                            //CUMPLIMIENTO
                            real = $("#txtPEmpRealAliado").val();
                            meta = $("#txtPPBaseAliado").val();

                            $('#txtPPCumplimientoAliado').removeClass('ColorCumplimiento');

                            var cumplimiento = avanceAliado(real, meta);

                            if (cumplimiento != 0) {
                                $("#txtPPCumplimientoAliado").val(parseInt(cumplimiento) + "%");
                            } else {
                                $("#txtPPCumplimientoAliado").val("0 %");
                            }

                            if (cumplimiento < 100) {
                                $("#txtPPCumplimientoAliado").addClass("ColorCumplimiento");
                            }

                        }
                        if (item.NomIndicador.trim() == $("#MetaCPAliado").val()) {
                            $("#txtCPBaseAliado").val(formatNumber(item.meta));


                            real = $("#txtCPresAliadoReal").val();
                            meta = $("#txtCPBaseAliado").val();
                            tendencia = $("#txtCPTendenciaAliado").val();
                            //AVANCE
                            avance = avanceAliado(real, meta);
                            if (avance != 0) {
                                $("#txtCPAvanceAliado").val(parseInt(avance) + " % ");
                            } else {
                                $("#txtCPAvanceAliado").val("0 %");
                            }

                            //CUMPLIMIENTO
                            //var cumplimiento = $("#txtCPCumplimientoAliado").val(cumplimientoAliado(real, item.meta));
                            $('#txtCPCumplimientoAliado').removeClass('ColorCumplimiento');

                            var cumplimiento = cumplimientoAliado(meta, tendencia);


                            if (cumplimiento != 0) {
                                $("#txtCPCumplimientoAliado").val(parseInt(cumplimiento) + "%");
                            } else {
                                $("#txtCPCumplimientoAliado").val("0 %");
                            }

                            if (cumplimiento < 100) {
                                $("#txtCPCumplimientoAliado").addClass("ColorCumplimiento");
                            }
                        }
                        if (item.NomIndicador.trim() == $("#MetaCNAliado").val()) {
                            $("#txtCNBaseAliado").val(item.meta);

                            real = $("#txtClieNuevAliadoReal").val();
                            meta = $("#txtCNBaseAliado").val();
                            tendencia = $("#txtCNTendenciaAliado").val();
                            //AVANCE
                            var avance = avanceAliado(real, meta);
                            if (avance != 0) {
                                $("#txtCNAvanceAliado").val(parseInt(avance) + " % ");
                            } else {
                                $("#txtCNAvanceAliado").val("0 % ");
                            }

                            //CUMPLIMIENTO
                            $('#txtCNCumplimientoAliado').removeClass('ColorCumplimiento');

                            var cumplimiento = cumplimientoAliado(meta, tendencia);

                            if (cumplimiento != 0) {
                                $("#txtCNCumplimientoAliado").val(parseInt(cumplimiento) + "%");
                            } else {
                                $("#txtCNCumplimientoAliado").val("0 %");
                            }

                            if (cumplimiento < 100) {
                                $("#txtCNCumplimientoAliado").addClass("ColorCumplimiento");
                            }
                        }
                        if (item.NomIndicador.trim() == $("#MetaIEAliado").val()) {
                            $("#txtIEBaseAliado").val(formatNumber(item.meta));

                            real = $("#txtIERealAliado").val();
                            meta = $("#txtIEBaseAliado").val();
                            tendencia = $("#txtIETendenciaAliado").val();
                            //AVANCE
                            avance = avanceAliado(real, meta);

                            if (avance != 0) {
                                $("#txtIEAvanceAliado").val(parseInt(avance) + " % ");
                            } else { $("#txtIEAvanceAliado").val("0 % "); }

                            //CUMPLIMIENTO
                            $('#txtIECumplimientoAliado').removeClass('ColorCumplimiento');
                            var cumplimiento = cumplimientoAliado(meta, tendencia);

                            if (cumplimiento != 0) {
                                $("#txtIECumplimientoAliado").val(parseInt(cumplimiento) + "%");
                            } else {
                                $("#txtIECumplimientoAliado").val("0 %");
                            }


                            if (cumplimiento < 100) {
                                $("#txtIECumplimientoAliado").addClass("ColorCumplimiento");
                            }

                        }
                        if (item.NomIndicador.trim() == $("#MetaIVAAliado").val()) {
                            $("#txtIVApaAliadoBase").val(formatNumber(item.meta));

                            real = $("#txtIVApaAliadoReal").val();
                            meta = $("#txtIVApaAliadoBase").val();
                            tendencia = $("#txtIVApaAliadoTendencia").val();
                            //AVANCE
                            avance = avanceAliado(real, meta);
                            if (avance != 0) {
                                $("#txtIVApaAliadoAvance").val(parseInt(avance) + " % ");
                            } else { $("#txtIVApaAliadoAvance").val("0 % "); }

                            //CUMPLIMIENTO
                            $('#txtIVApaAliadoCumplimiento').removeClass('ColorCumplimiento');
                            var cumplimiento = cumplimientoAliado(meta, tendencia);

                            if (cumplimiento != 0) {
                                $("#txtIVApaAliadoCumplimiento").val(parseInt(cumplimiento) + "%");
                            } else { $("#txtIVApaAliadoCumplimiento").val("0 %"); }


                            if (cumplimiento < 100) {
                                $("#txtIVApaAliadoCumplimiento").addClass("ColorCumplimiento");
                            }

                        }
                        if (item.NomIndicador.trim() == $("#MetaIVCAliado").val()) {
                            $("#txtIVCelAliadoBase").val(formatNumber(item.meta));
                            real = $("#txtIVCelAliadoReal").val();
                            meta = $("#txtIVCelAliadoBase").val();
                            tendencia = $("#txtIVCelAliadoTendencia").val();
                            //AVANCE
                            avance = avanceAliado(real, meta);
                            if (avance != 0) {
                                $("#txtIVCelAliadoAvance").val(parseInt(avance) + " % ");
                            } else { $("#txtIVCelAliadoAvance").val("0 % "); }

                            //CUMPLIMIENTO
                            $('#txtIVCelAliadoCumplimiento').removeClass('ColorCumplimiento');

                            var cumplimiento = cumplimientoAliado(meta, tendencia);
                            if (cumplimiento != 0) {
                                $("#txtIVCelAliadoCumplimiento").val(parseInt(cumplimiento) + "%");
                            } else { $("#txtIVCelAliadoCumplimiento").val("0 %"); }


                            if (cumplimiento < 100) {
                                $("#txtIVCelAliadoCumplimiento").addClass("ColorCumplimiento");
                            }

                        }
                        if (item.NomIndicador.trim() == $("#MetaIVJAliado").val()) {
                            $("#txtIVJoyAliadoBase").val(formatNumber(item.meta));
                            real = $("#txtIVJoyAliadoReal").val();
                            meta = $("#txtIVJoyAliadoBase").val();
                            tendencia = $("#txtIVJoyAliadoTendencia").val();

                            //AVANCE
                            avance = avanceAliado(real, meta);
                            if (avance != 0) {
                                $("#txtIVJoyAliadoAvance").val(parseInt(avance) + " % ");
                            } else { $("#txtIVJoyAliadoAvance").val("0 % "); }

                            //CUMPLIMIENTO
                            $('#txtIVJoyAliadoCumplimiento').removeClass('ColorCumplimiento');
                            var cumplimiento = cumplimientoAliado(meta, tendencia);

                            if (cumplimiento != 0) {
                                $("#txtIVJoyAliadoCumplimiento").val(parseInt(cumplimiento) + "%");
                            } else { $("#txtIVJoyAliadoCumplimiento").val("0 %"); }

                            if (cumplimiento < 100) {
                                $("#txtIVJoyAliadoCumplimiento").addClass("ColorCumplimiento");
                            }

                        }
                        if (item.NomIndicador.trim() == $("#MetaIACJAliado").val()) {
                            $("#txtIACJTotalAliadoBase").val(formatNumber(item.meta));

                            real = $("#txtIACJAliadoReal").val();
                            meta = $("#txtIACJTotalAliadoBase").val();
                            tendencia = $("#txtIACJAliadoTendencia").val();
                            //AVANCE
                            avance = avanceAliado(real, meta);
                            if (avance != 0) {
                                $("#txtIACJAliadoAvance").val(parseInt(avance) + " % ");
                            } else { $("#txtIACJAliadoAvance").val("0 % "); }

                            //CUMPLIMIENTO
                            $('#txtIACJAliadoCumplimiento').removeClass('ColorCumplimiento');

                            var cumplimiento = cumplimientoAliado(meta, tendencia);
                            if (cumplimiento != 0) {
                                $("#txtIACJAliadoCumplimiento").val(parseInt(cumplimiento) + "%");
                            } else { $("#txtIACJAliadoCumplimiento").val("0 %"); }


                            if (cumplimiento < 100) {
                                $("#txtIACJAliadoCumplimiento").addClass("ColorCumplimiento");
                            }

                        }
                        if (item.NomIndicador.trim() == $("#MetaITAliado").val()) {
                            $("#txtITotalAliadoBase").val(formatNumber(item.meta));

                            real = $("#txtITAliadoReal").val();
                            meta = $("#txtITotalAliadoBase").val();
                            tendencia = $("#txtITAliadoTendencia").val();
                            //AVANCE
                            avance = avanceAliado(real, meta);
                            if (avance != 0) {
                                $("#txtITAliadoAvance").val(parseInt(avance) + " % ");
                            } else { $("#txtITAliadoAvance").val("0 % "); }

                            //CUMPLIMIENTO
                            $('#txtITAliadoCumplimiento').removeClass('ColorCumplimiento');

                            var cumplimiento = cumplimientoAliado(meta, tendencia);
                            if (cumplimiento != 0) { $("#txtITAliadoCumplimiento").val(parseInt(cumplimiento) + "%"); } else { $("#txtITAliadoCumplimiento").val("0 %"); }


                            if (cumplimiento < 100) {
                                $("#txtITAliadoCumplimiento").addClass("ColorCumplimiento");
                            }

                        }
                    }
                    //Si no tienen valor ponerles 0

                    if ($("#txtPPBaseAliado").val() == "") {
                        //CUMPLIMIENTO
                        $("#txtPPCumplimientoAliado").val("0 %");
                        $("#txtPPCumplimientoAliado").addClass("ColorCumplimiento");
                        //cumplimiento

                    }
                    if ($("#txtCPBaseAliado").val() == "") {
                        $("#txtCPAvanceAliado").val("0 %");
                        //TENDENCIA
                        $("#txtCPCumplimientoAliado").val("0 %");
                        $("#txtCPCumplimientoAliado").addClass("ColorCumplimiento");
                    }
                    if ($("#txtCNBaseAliado").val() == "") {
                        $("#txtCNAvanceAliado").val("0 %");
                        //TENDENCIA
                        $("#txtCNCumplimientoAliado").val("0 %");
                        $("#txtCNCumplimientoAliado").addClass("ColorCumplimiento");
                    }
                    if ($("#txtIEBaseAliado").val() == "") {
                        $("#txtIEAvanceAliado").val("0 %");
                        //TENDENCIA
                        $("#txtIECumplimientoAliado").val("0 %");
                        $("#txtIECumplimientoAliado").addClass("ColorCumplimiento");
                    }
                    if ($("#txtIVApaAliadoBase").val() == "") {
                        $("#txtIVApaAliadoAvance").val("0 %");
                        //TENDENCIA
                        $("#txtIVApaAliadoCumplimiento").val("0 %");
                        $("#txtIVApaAliadoCumplimiento").addClass("ColorCumplimiento");
                    }
                    if ($("#txtIVCelAliadoBase").val() == "") {
                        $("#txtIVCelAliadoAvance").val("0 %");
                        //TENDENCIA
                        $("#txtIVCelAliadoCumplimiento").val("0 %");
                        $("#txtIVCelAliadoCumplimiento").addClass("ColorCumplimiento");
                    }
                    if ($("#txtIVJoyAliadoBase").val() == "") {
                        $("#txtIVJoyAliadoAvance").val("0 %");
                        //TENDENCIA
                        $("#txtIVJoyAliadoCumplimiento").val("0 %");
                        $("#txtIVJoyAliadoCumplimiento").addClass("ColorCumplimiento");
                    }
                    if ($("#txtIACJTotalAliadoBase").val() == "") {
                        $("#txtIACJAliadoAvance").val("0 %");
                        //TENDENCIA
                        $("#txtIACJAliadoCumplimiento").val("0 %");
                        $("#txtIACJAliadoCumplimiento").addClass("ColorCumplimiento");
                    }
                    if ($("#txtITotalAliadoBase").val() == "") {
                        $("#txtITAliadoAvance").val("0 %");
                        //TENDENCIA
                        $("#txtITAliadoCumplimiento").val("0 %");
                        $("#txtITAliadoCumplimiento").addClass("ColorCumplimiento");
                    }


                    document.getElementById('containerAliado').style.display = 'none';
                }
            })
        }
        else {

            $.ajax({
                type: 'POST',
                // url: '@Url.Action("MesAnterior")',
                url: "../MatrizGestion/IndicadorMesAnteriorAliado",
                dataType: 'json',
                data: { Anio, Mes, CveSuc, Cusr, NomSuc },
                success: function (indicador) {
                    // alert(indicador);
                                $("#txtPEmpRealAliado").val(formatNumber(indicador.PromedioSem));
                                //CAPITAL PRESTADO
                                if (indicador.Cartera != 0) {
                                    $("#txtCPresAliadoReal").val(formatNumber(indicador.Cartera));
                                    //tendencia
                                    $("#txtCPTendenciaAliado").val(formatNumber(tendenciaAliado(indicador.Cartera)));

                                } else {
                                    $("#txtCPresAliadoReal").val((0).toFixed(2));
                                    //tendencia
                                    $("#txtCPTendenciaAliado").val((0).toFixed(2));
                                }
                                //CLIENTES NUEVOS
                                if (indicador.TotalCteNvo != 0) {
                                    //$("#txtClieNuevAliadoReal").val(formatNumber(indicador.TotalCteNvo));
                                    $("#txtClieNuevAliadoReal").val(indicador.TotalCteNvo);
                                    //tendencia
                                    $("#txtCNTendenciaAliado").val(formatNumber(tendenciaAliado(indicador.TotalCteNvo)));
                                } else {
                                    $("#txtClieNuevAliadoReal").val(0);
                                    //tendencia
                                    $("#txtCNTendenciaAliado").val((0).toFixed(2));
                                }
                                //INGRESO EMPEÑOS

                                if (indicador.ingreso != 0) {
                                    $("#txtIERealAliado").val(formatNumber(indicador.ingreso));
                                    //tendencia
                                    $("#txtIETendenciaAliado").val(formatNumber(tendenciaAliado(indicador.ingreso)));
                                } else {
                                    $("#txtIERealAliado").val((0).toFixed(2));
                                    //tendencia
                                    $("#txtIETendenciaAliado").val((0).toFixed(2));
                                }

                                //INGRESO VENTA APARATOS, CELULARES Y JOYERIA , TOTALES (EMPEÑO-VENTA)

                                if (indicador.IngresoVenta != null) {
                                    //CARGAR LO REAL --Ingreso de venta (Aparato, joyeria y celulares)
                                    indicador.IngresoVenta.forEach(IngVta)
                                    function IngVta(item, index) {

                                        //if (item.Tipo.trim() == "Aparatos") {

                                        //    //var realIngVteApa = $("#txtIngVtaApaReal").val(item.Ingreso.toFixed(2));
                                        //    var resIVA = formatNumber(item.Ingreso);
                                        //    if (resIVA != 0) {
                                        //        var realIV = $("#txtIVApaAliadoReal").val(resIVA);
                                        //        //tendencia
                                        //        $("#txtIVApaAliadoTendencia").val(formatNumber(tendenciaAliado(item.Ingreso)));
                                        //    } else {
                                        //        var realIV = $("#txtIVApaAliadoReal").val(0);
                                        //    }
                                        //}
                                        //if (item.Tipo.trim() == "Celulares") {

                                        //    //var realIngVteCel = $("#txtIngVtaCelReal").val(item.Ingreso.toFixed(2));
                                        //    var resIVC = formatNumber(item.Ingreso);
                                        //    if (resIVC != 0) {
                                        //        var reaIC = $("#txtIVCelAliadoReal").val(resIVC);
                                        //        //tendencia
                                        //        $("#txtIVCelAliadoTendencia").val(formatNumber(tendenciaAliado(item.Ingreso)));
                                        //    } else {
                                        //        var reaIC = $("#txtIVCelAliadoReal").val(0);
                                        //    }

                                        //}
                                        //if (item.Tipo.trim() == "Joyeria") {

                                        //    //var realIngVteJoy = $("#txtIngVtaJoyReal").val(item.Ingreso.toFixed(2));
                                        //    var resIVJ = formatNumber(item.Ingreso);
                                        //    if (resIVJ != 0) {
                                        //        var reaIJ = $("#txtIVJoyAliadoReal").val(resIVJ);
                                        //        //tendencia
                                        //        $("#txtIVJoyAliadoTendencia").val(formatNumber(tendenciaAliado(item.Ingreso)));
                                        //    } else {
                                        //        var reaIJ = $("#txtIVJoyAliadoReal").val(0);
                                        //    }
                                        //}
                                        //Total (empeños- venta)
                                        if (item.Tipo.trim() == "Total") {
                                            //var realIngVteJoy = $("#txtIngVtaJoyReal").val(item.Ingreso.toFixed(2));
                                            var resITACJ = formatNumber(item.Ingreso);
                                            if (resITACJ != 0) {
                                                var resITACJ = $("#txtIACJAliadoReal").val(resITACJ);
                                                //tendencia
                                                $("#txtIACJAliadoTendencia").val(formatNumber(tendenciaAliado(item.Ingreso)));
                                            } else {
                                                var resITACJ = $("#txtIACJAliadoReal").val(0);
                                            }
                                        }

                                        //if (item.Tipo.trim() == "Total") {

                                        //    //var realIngVteJoy = $("#txtIngVtaJoyReal").val(item.Ingreso.toFixed(2));
                                        //    var resITACJ = formatNumber(item.Ingreso);
                                        //    if (resITACJ != 0) {
                                        //        var resITACJ = $("#txtITAliadoReal").val(resITACJ);
                                        //        //tendencia
                                        //        $("#txtITAliadoTendencia").val(formatNumber(tendenciaAliado(item.Ingreso)));
                                        //    } else {
                                        //        var resITACJ = $("#txtITAliadoReal").val(0);
                                        //    }
                                        //}

                                        


                                    }
                                }
                                
                                   
                                var ingresoE = $("#txtIERealAliado").val();
                                var IngresoACJ = $("#txtIACJAliadoReal").val();

                                if (ingresoE == "") {
                                    ingresoE = "0.00";
                                }
                                if (IngresoACJ == "") {
                                    var IngresoACJ = $("#txtIACJAliadoReal").val("0.00");
                                    IngresoACJ = "0.00"
                                }
                                var suma = parseFloat(ingresoE.replace(/,/g, "")) + parseFloat(IngresoACJ.replace(/,/g, ""));

                    //REAL de Ingreso total (empeños - venta)
                                $("#txtITAliadoReal").val(formatNumber(suma));
                                $("#txtITAliadoTendencia").val(formatNumber(tendenciaAliado(suma)));



                              

                    // meta(indicador.ValorMeta);

                                indicador.ValorMeta.forEach(metaItem);
                                function metaItem(item, index) {

                                    if (item.NomIndicador.trim() == $("#MetaPPAliado").val()) {
                                        $("#txtPPBaseAliado").val(formatNumber(item.meta));

                                        //CUMPLIMIENTO
                                        real = $("#txtPEmpRealAliado").val();
                                        meta = $("#txtPPBaseAliado").val();

                                        $('#txtPPCumplimientoAliado').removeClass('ColorCumplimiento');

                                        var cumplimiento = avanceAliado(real, meta);

                                        if (cumplimiento != 0) {
                                            $("#txtPPCumplimientoAliado").val(parseInt(cumplimiento) + "%");
                                        } else {
                                            $("#txtPPCumplimientoAliado").val("0 %");
                                        }

                                        if (cumplimiento < 100) {
                                            $("#txtPPCumplimientoAliado").addClass("ColorCumplimiento");
                                        }

                                    }
                                    if (item.NomIndicador.trim() == $("#MetaCPAliado").val()) {
                                        $("#txtCPBaseAliado").val(formatNumber(item.meta));


                                        real = $("#txtCPresAliadoReal").val();
                                        meta = $("#txtCPBaseAliado").val();
                                        tendencia = $("#txtCPTendenciaAliado").val();
                                        //AVANCE
                                        avance = avanceAliado(real, meta);
                                        if (avance != 0) {
                                            $("#txtCPAvanceAliado").val(parseInt(avance) + " % ");
                                        } else {
                                            $("#txtCPAvanceAliado").val("0 %");
                                        }

                                        //CUMPLIMIENTO
                                        //var cumplimiento = $("#txtCPCumplimientoAliado").val(cumplimientoAliado(real, item.meta));
                                        $('#txtCPCumplimientoAliado').removeClass('ColorCumplimiento');

                                        var cumplimiento = cumplimientoAliado(meta, tendencia);


                                        if (cumplimiento != 0) {
                                            $("#txtCPCumplimientoAliado").val(parseInt(cumplimiento) + "%");
                                        } else {
                                            $("#txtCPCumplimientoAliado").val("0 %");
                                        }

                                        if (cumplimiento < 100) {
                                            $("#txtCPCumplimientoAliado").addClass("ColorCumplimiento");
                                        }
                                    }
                                    if (item.NomIndicador.trim() == $("#MetaCNAliado").val()) {
                                        $("#txtCNBaseAliado").val(item.meta);

                                        real = $("#txtClieNuevAliadoReal").val();
                                        meta = $("#txtCNBaseAliado").val();
                                        tendencia = $("#txtCNTendenciaAliado").val();
                                        //AVANCE
                                        var avance = avanceAliado(real, meta);
                                        if (avance != 0) {
                                            $("#txtCNAvanceAliado").val(parseInt(avance) + " % ");
                                        } else {
                                            $("#txtCNAvanceAliado").val("0 % ");
                                        }

                                        //CUMPLIMIENTO
                                        $('#txtCNCumplimientoAliado').removeClass('ColorCumplimiento');

                                        var cumplimiento = cumplimientoAliado(meta, tendencia);

                                        if (cumplimiento != 0) {
                                            $("#txtCNCumplimientoAliado").val(parseInt(cumplimiento) + "%");
                                        } else {
                                            $("#txtCNCumplimientoAliado").val("0 %");
                                        }

                                        if (cumplimiento < 100) {
                                            $("#txtCNCumplimientoAliado").addClass("ColorCumplimiento");
                                        }
                                    }
                                    if (item.NomIndicador.trim() == $("#MetaIEAliado").val()) {
                                        $("#txtIEBaseAliado").val(formatNumber(item.meta));

                                        real = $("#txtIERealAliado").val();
                                        meta = $("#txtIEBaseAliado").val();
                                        tendencia = $("#txtIETendenciaAliado").val();
                                        //AVANCE
                                        avance = avanceAliado(real, meta);

                                        if (avance != 0) {
                                            $("#txtIEAvanceAliado").val(parseInt(avance) + " % ");
                                        } else { $("#txtIEAvanceAliado").val("0 % "); }

                                        //CUMPLIMIENTO
                                        $('#txtIECumplimientoAliado').removeClass('ColorCumplimiento');
                                        var cumplimiento = cumplimientoAliado(meta, tendencia);

                                        if (cumplimiento != 0) {
                                            $("#txtIECumplimientoAliado").val(parseInt(cumplimiento) + "%");
                                        } else {
                                            $("#txtIECumplimientoAliado").val("0 %");
                                        }


                                        if (cumplimiento < 100) {
                                            $("#txtIECumplimientoAliado").addClass("ColorCumplimiento");
                                        }

                                    }
                                    if (item.NomIndicador.trim() == $("#MetaIVAAliado").val()) {
                                        $("#txtIVApaAliadoBase").val(formatNumber(item.meta));

                                        real = $("#txtIVApaAliadoReal").val();
                                        meta = $("#txtIVApaAliadoBase").val();
                                        tendencia = $("#txtIVApaAliadoTendencia").val();
                                        //AVANCE
                                        avance = avanceAliado(real, meta);
                                        if (avance != 0) {
                                            $("#txtIVApaAliadoAvance").val(parseInt(avance) + " % ");
                                        } else { $("#txtIVApaAliadoAvance").val("0 % "); }

                                        //CUMPLIMIENTO
                                        $('#txtIVApaAliadoCumplimiento').removeClass('ColorCumplimiento');
                                        var cumplimiento = cumplimientoAliado(meta, tendencia);

                                        if (cumplimiento != 0) {
                                            $("#txtIVApaAliadoCumplimiento").val(parseInt(cumplimiento) + "%");
                                        } else { $("#txtIVApaAliadoCumplimiento").val("0 %"); }


                                        if (cumplimiento < 100) {
                                            $("#txtIVApaAliadoCumplimiento").addClass("ColorCumplimiento");
                                        }

                                    }
                                    if (item.NomIndicador.trim() == $("#MetaIVCAliado").val()) {
                                        $("#txtIVCelAliadoBase").val(formatNumber(item.meta));
                                        real = $("#txtIVCelAliadoReal").val();
                                        meta = $("#txtIVCelAliadoBase").val();
                                        tendencia = $("#txtIVCelAliadoTendencia").val();
                                        //AVANCE
                                        avance = avanceAliado(real, meta);
                                        if (avance != 0) {
                                            $("#txtIVCelAliadoAvance").val(parseInt(avance) + " % ");
                                        } else { $("#txtIVCelAliadoAvance").val("0 % "); }

                                        //CUMPLIMIENTO
                                        $('#txtIVCelAliadoCumplimiento').removeClass('ColorCumplimiento');

                                        var cumplimiento = cumplimientoAliado(meta, tendencia);
                                        if (cumplimiento != 0) {
                                            $("#txtIVCelAliadoCumplimiento").val(parseInt(cumplimiento) + "%");
                                        } else { $("#txtIVCelAliadoCumplimiento").val("0 %"); }


                                        if (cumplimiento < 100) {
                                            $("#txtIVCelAliadoCumplimiento").addClass("ColorCumplimiento");
                                        }

                                    }
                                    if (item.NomIndicador.trim() == $("#MetaIVJAliado").val()) {
                                        $("#txtIVJoyAliadoBase").val(formatNumber(item.meta));
                                        real = $("#txtIVJoyAliadoReal").val();
                                        meta = $("#txtIVJoyAliadoBase").val();
                                        tendencia = $("#txtIVJoyAliadoTendencia").val();

                                        //AVANCE
                                        avance = avanceAliado(real, meta);
                                        if (avance != 0) {
                                            $("#txtIVJoyAliadoAvance").val(parseInt(avance) + " % ");
                                        } else { $("#txtIVJoyAliadoAvance").val("0 % "); }

                                        //CUMPLIMIENTO
                                        $('#txtIVJoyAliadoCumplimiento').removeClass('ColorCumplimiento');
                                        var cumplimiento = cumplimientoAliado(meta, tendencia);

                                        if (cumplimiento != 0) {
                                            $("#txtIVJoyAliadoCumplimiento").val(parseInt(cumplimiento) + "%");
                                        } else { $("#txtIVJoyAliadoCumplimiento").val("0 %"); }

                                        if (cumplimiento < 100) {
                                            $("#txtIVJoyAliadoCumplimiento").addClass("ColorCumplimiento");
                                        }

                                    }
                                    if (item.NomIndicador.trim() == $("#MetaIACJAliado").val()) {
                                        $("#txtIACJTotalAliadoBase").val(formatNumber(item.meta));

                                        real = $("#txtIACJAliadoReal").val();
                                        meta = $("#txtIACJTotalAliadoBase").val();
                                        tendencia = $("#txtIACJAliadoTendencia").val();
                                        //AVANCE
                                        avance = avanceAliado(real, meta);
                                        if (avance != 0) {
                                            $("#txtIACJAliadoAvance").val(parseInt(avance) + " % ");
                                        } else { $("#txtIACJAliadoAvance").val("0 % "); }

                                        //CUMPLIMIENTO
                                        $('#txtIACJAliadoCumplimiento').removeClass('ColorCumplimiento');

                                        var cumplimiento = cumplimientoAliado(meta, tendencia);
                                        if (cumplimiento != 0) {
                                            $("#txtIACJAliadoCumplimiento").val(parseInt(cumplimiento) + "%");
                                        } else { $("#txtIACJAliadoCumplimiento").val("0 %"); }


                                        if (cumplimiento < 100) {
                                            $("#txtIACJAliadoCumplimiento").addClass("ColorCumplimiento");
                                        }

                                    }
                                    if (item.NomIndicador.trim() == $("#MetaITAliado").val()) {
                                        $("#txtITotalAliadoBase").val(formatNumber(item.meta));

                                        real = $("#txtITAliadoReal").val();
                                        meta = $("#txtITotalAliadoBase").val();
                                        tendencia = $("#txtITAliadoTendencia").val();
                                        //AVANCE
                                        avance = avanceAliado(real, meta);
                                        if (avance != 0) {
                                            $("#txtITAliadoAvance").val(parseInt(avance) + " % ");
                                        } else { $("#txtITAliadoAvance").val("0 % "); }

                                        //CUMPLIMIENTO
                                        $('#txtITAliadoCumplimiento').removeClass('ColorCumplimiento');

                                        var cumplimiento = cumplimientoAliado(meta, tendencia);
                                        if (cumplimiento != 0) { $("#txtITAliadoCumplimiento").val(parseInt(cumplimiento) + "%"); } else { $("#txtITAliadoCumplimiento").val("0 %"); }


                                        if (cumplimiento < 100) {
                                            $("#txtITAliadoCumplimiento").addClass("ColorCumplimiento");
                                        }

                                    }
                                }
                    //Si no tienen valor ponerles 0

                                if ($("#txtPPBaseAliado").val() == "") {
                                    //CUMPLIMIENTO
                                    $("#txtPPCumplimientoAliado").val("0 %");
                                    $("#txtPPCumplimientoAliado").addClass("ColorCumplimiento");
                                    //cumplimiento

                                }
                                if ($("#txtCPBaseAliado").val() == "") {
                                    $("#txtCPAvanceAliado").val("0 %");
                                    //TENDENCIA
                                    $("#txtCPCumplimientoAliado").val("0 %");
                                    $("#txtCPCumplimientoAliado").addClass("ColorCumplimiento");
                                }
                                if ($("#txtCNBaseAliado").val() == "") {
                                    $("#txtCNAvanceAliado").val("0 %");
                                    //TENDENCIA
                                    $("#txtCNCumplimientoAliado").val("0 %");
                                    $("#txtCNCumplimientoAliado").addClass("ColorCumplimiento");
                                }
                                if ($("#txtIEBaseAliado").val() == "") {
                                    $("#txtIEAvanceAliado").val("0 %");
                                    //TENDENCIA
                                    $("#txtIECumplimientoAliado").val("0 %");
                                    $("#txtIECumplimientoAliado").addClass("ColorCumplimiento");
                                }
                                if ($("#txtIVApaAliadoBase").val() == "") {
                                    $("#txtIVApaAliadoAvance").val("0 %");
                                    //TENDENCIA
                                    $("#txtIVApaAliadoCumplimiento").val("0 %");
                                    $("#txtIVApaAliadoCumplimiento").addClass("ColorCumplimiento");
                                }
                                if ($("#txtIVCelAliadoBase").val() == "") {
                                    $("#txtIVCelAliadoAvance").val("0 %");
                                    //TENDENCIA
                                    $("#txtIVCelAliadoCumplimiento").val("0 %");
                                    $("#txtIVCelAliadoCumplimiento").addClass("ColorCumplimiento");
                                }
                                if ($("#txtIVJoyAliadoBase").val() == "") {
                                    $("#txtIVJoyAliadoAvance").val("0 %");
                                    //TENDENCIA
                                    $("#txtIVJoyAliadoCumplimiento").val("0 %");
                                    $("#txtIVJoyAliadoCumplimiento").addClass("ColorCumplimiento");
                                }
                                if ($("#txtIACJTotalAliadoBase").val() == "") {
                                    $("#txtIACJAliadoAvance").val("0 %");
                                    //TENDENCIA
                                    $("#txtIACJAliadoCumplimiento").val("0 %");
                                    $("#txtIACJAliadoCumplimiento").addClass("ColorCumplimiento");
                                }
                                if ($("#txtITotalAliadoBase").val() == "") {
                                    $("#txtITAliadoAvance").val("0 %");
                                    //TENDENCIA
                                    $("#txtITAliadoCumplimiento").val("0 %");
                                    $("#txtITAliadoCumplimiento").addClass("ColorCumplimiento");
                                }

                                document.getElementById('containerAliado').style.display = 'none';

                }
            })
        }

        })
    //});

}

function dHabilAliado() {
    //Dias habiles
    var combo = document.getElementById("SucAliado");
    var SucursalID = combo.options[combo.selectedIndex].text;
    //alert(selected);

    $.ajax({
        type: 'POST',
        //url: '@Url.Action("ConsultaDiaHabil", "Operaciones")',
        url: "../Operaciones/ConsultaDiaHabil",
        dataType: 'json',
        data: { SucursalID },
        success: function (Sucursal) {
            //$("#txtDiasHabilesAliado").val(Sucursal.Cantidad);
            $("#txtDiasHabilesAliado").val(Sucursal.DiasHabiles);
            $("#txtDiasTranscAliado").val(Sucursal.DiasTranscurridos);

            $("#txtDiasRestantesAliado").val(parseInt($("#txtDiasHabilesAliado").val()) - parseInt($("#txtDiasTranscAliado").val()))
            //RESTANTES
            //diasRestanteAliado();



           
        },
    });
}

//CARGAR DIAS RESTANTES
function diasRestanteAliado() {
    var habiles = $("#txtDiasHabilesAliado").val();
    var transcurrido = $("#txtDiasTranscAliado").val();
    //archivo js

    var DiasRestantes = diasRestantejs(habiles, transcurrido);
    $("#txtDiasRestantesAliado").val(DiasRestantes);
}

function formatNumber(num) {
    if (!num || num == 'NaN') return '-';
    if (num == 'Infinity') return '&#x221e;';
    num = num.toString().replace(/\$|\,/g, '');
    if (isNaN(num))
        num = "0";
    sign = (num == (num = Math.abs(num)));
    num = Math.floor(num * 100 + 0.50000000001);
    cents = num % 100;
    num = Math.floor(num / 100).toString();
    if (cents < 10)
        cents = "0" + cents;
    for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3) ; i++)
        num = num.substring(0, num.length - (4 * i + 3)) + ',' + num.substring(num.length - (4 * i + 3));
    return (((sign) ? '' : '-') + num + '.' + cents);
}


//Cargar Indicadores 
function CargarIndicadores() {
    $.ajax({
        url: "../MatrizGestion/IndicadorAliado",
        type: 'POST',
        dataType: 'json',
        data: { NomSuc },
        success: function (AliadoAliado) {
            $.each(AliadoAliado, function (i, AliadoAliado) {
                $("#AliadoAliado").append('<option value="' + AliadoAliado.Value + '">' + AliadoAliado.Text + '</option>');
            });
        },
    })
}


function ManteriorAliado() {

    var checkBox = document.getElementById("MesAnteriorAliado");

    document.getElementById('TimeAliado').style.display = 'inline';

    var combo;
    var NomSuc;
    

    $("#AliadoAliado").empty();
    limpiarAliado();

    var Cusr = document.getElementById("AliadoAliado").value;
    //Sacar el nombre de la sucursal
    combo = document.getElementById("SucAliado");
    NomSuc = combo.options[combo.selectedIndex].text;
    //CVe sucursal para conectar al servidor de empeños y buscar lis aliados
    var CveSuc = combo.options[combo.selectedIndex].value;


    // si el checkbox(Mes anterior es marcado), despliega el valor en el textbox
    if (checkBox.checked == true) {

        var date = new Date();
        var actual = new Date(date.getFullYear(), date.getMonth() + 1, 0);
        //FECHA SE LE QUITA UN MES ANTERIOR (LA FECHA EMPIEZA ES "eNERO-0, FEBRERO-2..")
        //Y SE LE SUMA 1 PARA YA TENER LA FECHA CORRECTA
        var anterior = new Date(date.getFullYear(), (date.getMonth() - 1) + 1, 0);

        var Anio = anterior.getFullYear();
        var Mes = anterior.getMonth() + 1;

        //var CveSuc = $("#SucAliado").val();
        DiasHMAAliado(Anio, Mes);
        //var NomSuc = $("#SucAliado").val();

        $.ajax({
            url: "../MatrizGestion/AliadoMesAnterior",
            type: 'POST',
            dataType: 'json',
            data: { NomSuc, CveSuc, Anio, Mes },
            success: function (AliadoAliado) {

                $.each(AliadoAliado, function (i, AliadoAliado) {
                    $("#AliadoAliado").append('<option value="' + AliadoAliado.Value + '">' + AliadoAliado.Text + '</option>');
                });
                document.getElementById('TimeAliado').style.display = 'none';
            },
        })

    } else {
        
        //DiasHMAAliado(Anio, Mes);
        dHabilAliado();
        //alert("no");
        $.ajax({
            url: "../MatrizGestion/Aliado",
            type: 'POST',
            dataType: 'json',
            data: { NomSuc, CveSuc },
            success: function (AliadoAliado) {

                $.each(AliadoAliado, function (i, AliadoAliado) {
                    $("#AliadoAliado").append('<option value="' + AliadoAliado.Value + '">' + AliadoAliado.Text + '</option>');
                });
                document.getElementById('TimeAliado').style.display = 'none';
            },
        })
    }
}

//function meta(Response) {

//    Response.forEach(metaItem);
//    function metaItem(item, index) {

//        if (item.NomIndicador.trim() == $("#MetaPPAliado").val()) {
//            $("#txtPPBaseAliado").val(formatNumber(item.meta));
        
//            //CUMPLIMIENTO
//            real = $("#txtPEmpRealAliado").val();
//            meta = $("#txtPPBaseAliado").val();

//            $('#txtPPCumplimientoAliado').removeClass('ColorCumplimiento');

//            var cumplimiento = avanceAliado(real, meta);
            
//            if (cumplimiento != 0)
//            {
//                $("#txtPPCumplimientoAliado").val(parseInt(cumplimiento) + "%");
//            } else {
//                $("#txtPPCumplimientoAliado").val("0 %");
//            }

//            if (cumplimiento < 100) {
//                $("#txtPPCumplimientoAliado").addClass("ColorCumplimiento");
//            }
                        
//        }
//        if (item.NomIndicador.trim() == $("#MetaCPAliado").val()) {
//            $("#txtCPBaseAliado").val(formatNumber(item.meta));


//            real = $("#txtCPresAliadoReal").val();
//            meta = $("#txtCPBaseAliado").val();
//            tendencia = $("#txtCPTendenciaAliado").val();
//            //AVANCE
//            avance = avanceAliado(real, meta);
//            if (avance != 0) {
//                $("#txtCPAvanceAliado").val(parseInt(avance) + " % ");
//            } else {
//                $("#txtCPAvanceAliado").val( "0 %");
//            }
            
//            //CUMPLIMIENTO
//            //var cumplimiento = $("#txtCPCumplimientoAliado").val(cumplimientoAliado(real, item.meta));
//            $('#txtCPCumplimientoAliado').removeClass('ColorCumplimiento');

//            var cumplimiento = cumplimientoAliado(meta, tendencia);
           
            
//            if (cumplimiento != 0) {
//                $("#txtCPCumplimientoAliado").val(parseInt(cumplimiento) + "%");
//            } else {
//                $("#txtCPCumplimientoAliado").val("0 %");
//            }

//            if (cumplimiento < 100) {
//                $("#txtCPCumplimientoAliado").addClass("ColorCumplimiento");
//            }
//        }
//        if (item.NomIndicador.trim() == $("#MetaCNAliado").val()) {
//            $("#txtCNBaseAliado").val(item.meta);
          
//            real = $("#txtClieNuevAliadoReal").val();
//            meta = $("#txtCNBaseAliado").val();
//            tendencia = $("#txtCNTendenciaAliado").val();
//            //AVANCE
//            var avance = avanceAliado(real, meta);
//            if (avance != 0)
//            {
//                $("#txtCNAvanceAliado").val(parseInt(avance) + " % ");
//            } else {
//                $("#txtCNAvanceAliado").val("0 % ");
//            }
            
//            //CUMPLIMIENTO
//            $('#txtCNCumplimientoAliado').removeClass('ColorCumplimiento');

//            var cumplimiento = cumplimientoAliado(meta, tendencia);

//            if (cumplimiento != 0)
//            {
//                $("#txtCNCumplimientoAliado").val(parseInt(cumplimiento) + "%");
//            } else {
//                $("#txtCNCumplimientoAliado").val("0 %");
//            }

//            if (cumplimiento < 100) {
//                    $("#txtCNCumplimientoAliado").addClass("ColorCumplimiento");
//            }
//        }
//        if (item.NomIndicador.trim() == $("#MetaIEAliado").val()) {
//            $("#txtIEBaseAliado").val(formatNumber(item.meta));

//            real = $("#txtIERealAliado").val();
//            meta = $("#txtIEBaseAliado").val();
//            tendencia = $("#txtIETendenciaAliado").val();
//            //AVANCE
//            avance = avanceAliado(real, meta);

//            if(avance !=0){
//                $("#txtIEAvanceAliado").val(parseInt(avance) + " % ");
//            } else { $("#txtIEAvanceAliado").val("0 % "); }
            
//            //CUMPLIMIENTO
//            $('#txtIECumplimientoAliado').removeClass('ColorCumplimiento');
//            var cumplimiento = cumplimientoAliado(meta, tendencia);

//            if (cumplimiento != 0) {
//                $("#txtIECumplimientoAliado").val(parseInt(cumplimiento) + "%");
//            } else {
//                $("#txtIECumplimientoAliado").val("0 %");
//            }
           

//            if (cumplimiento < 100) {
//                $("#txtIECumplimientoAliado").addClass("ColorCumplimiento");
//            }
            
//        }
//        if (item.NomIndicador.trim() == $("#MetaIVAAliado").val()) {
//            $("#txtIVApaAliadoBase").val(formatNumber(item.meta));

//            real = $("#txtIVApaAliadoReal").val();
//            meta = $("#txtIVApaAliadoBase").val();
//            tendencia = $("#txtIVApaAliadoTendencia").val();
//            //AVANCE
//            avance = avanceAliado(real,meta);
//            if (avance!= 0) {
//                $("#txtIVApaAliadoAvance").val(parseInt(avance) + " % ");
//            } else { $("#txtIVApaAliadoAvance").val( "0 % "); }
            
//            //CUMPLIMIENTO
//            $('#txtIVApaAliadoCumplimiento').removeClass('ColorCumplimiento');
//            var cumplimiento = cumplimientoAliado(meta, tendencia);

//            if(cumplimiento != 0){
//                $("#txtIVApaAliadoCumplimiento").val(parseInt(cumplimiento) + "%");
//            } else { $("#txtIVApaAliadoCumplimiento").val("0 %"); }
            

//            if (cumplimiento < 100) {
//                $("#txtIVApaAliadoCumplimiento").addClass("ColorCumplimiento");
//            }

//        }
//        if (item.NomIndicador.trim() == $("#MetaIVCAliado").val()) {
//            $("#txtIVCelAliadoBase").val(formatNumber(item.meta));
//            real = $("#txtIVCelAliadoReal").val();
//            meta = $("#txtIVCelAliadoBase").val();
//            tendencia = $("#txtIVCelAliadoTendencia").val();
//            //AVANCE
//            avance = avanceAliado(real, meta);
//            if (avance != 0) {
//                $("#txtIVCelAliadoAvance").val(parseInt(avance) + " % ");
//            } else { $("#txtIVCelAliadoAvance").val("0 % "); }
            
//            //CUMPLIMIENTO
//            $('#txtIVCelAliadoCumplimiento').removeClass('ColorCumplimiento');

//            var cumplimiento = cumplimientoAliado(meta, tendencia);
//            if(cumplimiento !=0){
//                $("#txtIVCelAliadoCumplimiento").val(parseInt(cumplimiento) + "%");
//            } else { $("#txtIVCelAliadoCumplimiento").val("0 %"); }
            

//            if (cumplimiento < 100) {
//                $("#txtIVCelAliadoCumplimiento").addClass("ColorCumplimiento");
//            }

//        }
//        if (item.NomIndicador.trim() == $("#MetaIVJAliado").val()) {
//            $("#txtIVJoyAliadoBase").val(formatNumber(item.meta));
//            real = $("#txtIVJoyAliadoReal").val();
//            meta = $("#txtIVJoyAliadoBase").val();
//            tendencia = $("#txtIVJoyAliadoTendencia").val();

//            //AVANCE
//            avance = avanceAliado(real,meta);
//            if(avance!=0){
//                $("#txtIVJoyAliadoAvance").val(parseInt(avance) + " % ");
//            } else { $("#txtIVJoyAliadoAvance").val("0 % "); }
            
//            //CUMPLIMIENTO
//            $('#txtIVJoyAliadoCumplimiento').removeClass('ColorCumplimiento');
//            var cumplimiento = cumplimientoAliado(meta, tendencia);

//            if (cumplimiento != 0) {
//                $("#txtIVJoyAliadoCumplimiento").val(parseInt(cumplimiento) + "%");
//            } else { $("#txtIVJoyAliadoCumplimiento").val("0 %"); }
           
//            if (cumplimiento < 100) {
//                $("#txtIVJoyAliadoCumplimiento").addClass("ColorCumplimiento");
//            }

//        }
//        if (item.NomIndicador.trim() == $("#MetaIACJAliado").val()) {
//            $("#txtIACJTotalAliadoBase").val(formatNumber(item.meta));
            
//            real = $("#txtIACJAliadoReal").val();
//            meta = $("#txtIACJTotalAliadoBase").val();
//            tendencia = $("#txtIACJAliadoTendencia").val();
//            //AVANCE
//            avance = avanceAliado(real, meta);
//            if (avance != 0) {
//                $("#txtIACJAliadoAvance").val(parseInt(avance) + " % ");
//            } else { $("#txtIACJAliadoAvance").val("0 % "); }
            
//            //CUMPLIMIENTO
//            $('#txtIACJAliadoCumplimiento').removeClass('ColorCumplimiento');

//            var cumplimiento = cumplimientoAliado(meta, tendencia);
//            if(cumplimiento!=0){
//                $("#txtIACJAliadoCumplimiento").val(parseInt(cumplimiento) + "%");
//            } else { $("#txtIACJAliadoCumplimiento").val("0 %"); }
           

//            if (cumplimiento < 100) {
//                $("#txtIACJAliadoCumplimiento").addClass("ColorCumplimiento");
//            }

//        }
//        if (item.NomIndicador.trim() == $("#MetaITAliado").val()) {
//            $("#txtITotalAliadoBase").val(formatNumber(item.meta));

//            real = $("#txtITAliadoReal").val();
//            meta = $("#txtITotalAliadoBase").val();
//            tendencia = $("#txtITAliadoTendencia").val();
//            //AVANCE
//            avance = avanceAliado(real,meta);
//            if (avance != 0) {
//                $("#txtITAliadoAvance").val(parseInt(avance) + " % ");
//            } else { $("#txtITAliadoAvance").val("0 % "); }
            
//            //CUMPLIMIENTO
//            $('#txtITAliadoCumplimiento').removeClass('ColorCumplimiento');

//            var cumplimiento = cumplimientoAliado(meta, tendencia);
//            if (cumplimiento != 0) { $("#txtITAliadoCumplimiento").val(parseInt(cumplimiento) + "%"); } else { $("#txtITAliadoCumplimiento").val("0 %"); }
            

//            if (cumplimiento < 100) {
//                $("#txtITAliadoCumplimiento").addClass("ColorCumplimiento");
//            }

//        }
//    }
//    //Si no tienen valor ponerles 0

//    if ($("#txtPPBaseAliado").val() == ""  ) {
//        //CUMPLIMIENTO
//        $("#txtPPCumplimientoAliado").val("0 %");
//        $("#txtPPCumplimientoAliado").addClass("ColorCumplimiento");
//    //cumplimiento

//    }
//    if ($("#txtCPBaseAliado").val() == "") {
//        $("#txtCPAvanceAliado").val("0 %");
//        //TENDENCIA
//        $("#txtCPCumplimientoAliado").val("0 %");
//        $("#txtCPCumplimientoAliado").addClass("ColorCumplimiento");
//    }
//    if ($("#txtCNBaseAliado").val() == "") {
//        $("#txtCNAvanceAliado").val("0 %");
//        //TENDENCIA
//        $("#txtCNCumplimientoAliado").val("0 %");
//        $("#txtCNCumplimientoAliado").addClass("ColorCumplimiento");
//    }
//    if ($("#txtIEBaseAliado").val() == "") {
//        $("#txtIEAvanceAliado").val("0 %");
//        //TENDENCIA
//        $("#txtIECumplimientoAliado").val("0 %");
//        $("#txtIECumplimientoAliado").addClass("ColorCumplimiento");
//    }
//    if ($("#txtIVApaAliadoBase").val() == "") {
//        $("#txtIVApaAliadoAvance").val("0 %");
//        //TENDENCIA
//        $("#txtIVApaAliadoCumplimiento").val("0 %");
//        $("#txtIVApaAliadoCumplimiento").addClass("ColorCumplimiento");
//    }
//    if ($("#txtIVCelAliadoBase").val() == "") {
//        $("#txtIVCelAliadoAvance").val("0 %");
//        //TENDENCIA
//        $("#txtIVCelAliadoCumplimiento").val("0 %");
//        $("#txtIVCelAliadoCumplimiento").addClass("ColorCumplimiento");
//    }
//    if ($("#txtIVJoyAliadoBase").val() == "") {
//        $("#txtIVJoyAliadoAvance").val("0 %");
//        //TENDENCIA
//        $("#txtIVJoyAliadoCumplimiento").val("0 %");
//        $("#txtIVJoyAliadoCumplimiento").addClass("ColorCumplimiento");
//    }
//    if ($("#txtIACJTotalAliadoBase").val() == "") {
//        $("#txtIACJAliadoAvance").val("0 %");
//        //TENDENCIA
//        $("#txtIACJAliadoCumplimiento").val("0 %");
//        $("#txtIACJAliadoCumplimiento").addClass("ColorCumplimiento");
//    }
//    if ($("#txtITotalAliadoBase").val() == "") {
//        $("#txtITAliadoAvance").val("0 %");
//        //TENDENCIA
//        $("#txtITAliadoCumplimiento").val("0 %");
//        $("#txtITAliadoCumplimiento").addClass("ColorCumplimiento");
//    }
//}

function DiasHMAAliado(Anio, Mes) {
    //sucursal seleccionado)
    var combo = document.getElementById("SucAliado");
    var SucursalID = combo.options[combo.selectedIndex].text;
    //alert(selected);

    $.ajax({
        type: 'POST',
        url: "../Operaciones/ConsultaDHabilMes",
        //url: '@Url.Action("ConsultaDHabilMes", "Operaciones")',
        dataType: 'json',
        data: { SucursalID, Anio, Mes },
        success: function (diasH) {
            $("#txtDiasHabilesAliado").val(diasH.DiasHabiles);
            $("#txtDiasTranscAliado").val(diasH.DiasHabiles);

            //RESTANTES
            //diasRestante();
            //CARGAR LA BASE -- CAPITAL PRESTADO
            //baseMes(SucursalID);

            //Dias restantes
            $("#txtDiasRestantesAliado").val(0);
            
        },
    });

}

function limpiarAliado() {
    //META
    txtCNBaseAliado
    $("#txtPPBaseAliado").val("");
    $("#txtCPBaseAliado").val("");
    $("#txtCNBaseAliado").val("");
    $("#txtIEBaseAliado").val("");
    $("#txtIVApaAliadoBase").val("");
    $("#txtIVCelAliadoBase").val("");
    $("#txtIVJoyAliadoBase").val("");
    $("#txtIngTotalBase").val("");
    $("#txtIngTotalBase").val("");

    //REAL--listo
    $("#txtPEmpRealAliado").val("");
    $("#txtCPresAliadoReal").val("");
    $("#txtClieNuevAliadoReal").val("");
    $("#txtIERealAliado").val("");
    $("#txtIVApaAliadoReal").val("");
    $("#txtIVCelAliadoReal").val("");
    $("#txtIVJoyAliadoReal").val("");
    $("#txtIACJAliadoReal").val("");
    $("#txtITAliadoReal").val("");

    //AVANCE
    $("#txtCPAvanceAliado").val("");
    $("#txtCNAvanceAliado").val("");
    $("#txtIEAvanceAliado").val("");
    $("#txtIVApaAliadoAvance").val("");
    $("#txtIVCelAliadoAvance").val("");
    $("#txtIVJoyAliadoAvance").val("");
    $("#txtIACJAliadoAvance").val("");
    $("#txtITAliadoAvance").val("");
        
    //TENDENCIA
    $("#txtCPTendenciaAliado").val("");
    $("#txtCNTendenciaAliado").val("");
    $("#txtIETendenciaAliado").val("");
    $("#txtIVApaAliadoTendencia").val("");
    $("#txtIVCelAliadoTendencia").val("");
    $("#txtIVJoyAliadoTendencia").val("");
    $("#txtIACJAliadoTendencia").val("");
    $("#txtITAliadoTendencia").val("");   

    //CUMPLIMIENTO
    $("#txtPPCumplimientoAliado").val("");
    $("#txtCPCumplimientoAliado").val("");
    $("#txtCNCumplimientoAliado").val("");
    $("#txtIECumplimientoAliado").val("");
    $("#txtIVApaAliadoCumplimiento").val("");
    $("#txtIVCelAliadoCumplimiento").val("");
    $("#txtIVJoyAliadoCumplimiento").val("");
    $("#txtIACJAliadoCumplimiento").val("");
    $("#txtITAliadoCumplimiento").val("");


    $("#txtPPCumplimientoAliado").removeClass('ColorCumplimiento');
    $("#txtCPCumplimientoAliado").removeClass('ColorCumplimiento');
    $("#txtCNCumplimientoAliado").removeClass('ColorCumplimiento');
    $("#txtIECumplimientoAliado").removeClass('ColorCumplimiento');
    $("#txtIVApaAliadoCumplimiento").removeClass('ColorCumplimiento');
    $("#txtIVCelAliadoCumplimiento").removeClass('ColorCumplimiento');
    $("#txtIVJoyAliadoCumplimiento").removeClass('ColorCumplimiento');
    $("#txtIACJAliadoCumplimiento").removeClass('ColorCumplimiento');
    $("#txtITAliadoCumplimiento").removeClass('ColorCumplimiento');
   
}


//Dia habil
//Dias transcurridos
//dias reastantes
//cargar operaciones
//ver mes anterior

//Cumplimiento promedio  prestamo
function cumplimientoPPAliado() {

    $('#txtPPCumplimientoAliado').removeClass('ColorCumplimiento');

    var real = $("#txtPEmpRealAliado").val();
    var base = $("#txtPPBaseAliado").val();

    if (real == "" | base == "") {
        $("#txtPPCumplimientoAliado").val("");
    } else {
        real = real.replace(/,/g, "")
        base = base.replace(/,/g, "")

        var cumplimiento = parseInt((parseFloat(real) / parseFloat(base)) * 100)
        $("#txtPPCumplimientoAliado").val(cumplimiento + " % ");
        //PINTAR EL NEGATIVO TEXTBOX
        if (cumplimiento < 100) {
            $("#txtPPCumplimientoAliado").addClass("ColorCumplimiento");
        }
    }

}


// Capital prestado avance , tendencia y cumplimiento
function avanceCAPAliado() {

    var base = document.getElementById("txtCPBaseAliado").value;
    var real = document.getElementById("txtCPresAliadoReal").value;


    if (base == "" | real == "") {
        $("#txtCPAvanceAliado").val(0);

    }
    else {
        //BASE
        //var avance = Math.round((parseFloat(real) / parseFloat(base)) * 100)

        base = base.replace(/,/g, "")
        real = real.replace(/,/g, "")

        //$("#txtMetCarteraAvance").val(avance.toFixed(2) + " % ")
        var avance = Math.round((parseInt(real) / parseInt(base)) * 100)
        $("#txtCPAvanceAliado").val(avance + " % ")
    }
}
function tendenciaCPAliado() {

    var real = document.getElementById("txtCPresAliadoReal").value;
    var habiles = document.getElementById("txtDiasHabilesAliado").value;
    var transcurrido = document.getElementById("txtDiasTranscAliado").value;

    if (real == "" | habiles == "" | transcurrido == "") {
        $("#txtCPTendenciaAliado").val("");

    } else {
        real = real.replace(/,/g, "")
        habiles = habiles.replace(/,/g, "")
        transcurrido = transcurrido.replace(/,/g, "")

        var tendencia = (real / transcurrido) * habiles
        //$("#txtCapPresTendencia").val(new Intl.NumberFormat("en-MX").format(tendencia));
        $("#txtCPTendenciaAliado").val(formatNumber(tendencia));
        //var tendencia = (parseFloat(Sucursal.Cartera) / parseFloat(transcurrido)) * parseFloat(habiles)
        //$("#txtMetCarteraTendencia").val(new Intl.NumberFormat("en-MX").format(tendencia));

    }
}
function cumplimientoCPAliado() {

    $('#txtCPCumplimientoAliado').removeClass('ColorCumplimiento');

    var tendencia = $("#txtCPTendenciaAliado").val();
    var base = $("#txtCPBaseAliado").val();


    if (tendencia == "" | base == "") {
        $("#txtCPCumplimientoAliado").val(0);
    } else {
        tendencia = tendencia.replace(/,/g, "")
        base = base.replace(/,/g, "")

        var cumplimiento = parseInt((parseFloat(tendencia) / parseFloat(base)) * 100)
        $("#txtCPCumplimientoAliado").val(cumplimiento + " % ")


    }

    //PINTAR EL NEGATIVO TEXTBOX
    if (cumplimiento < 100) {
        $("#txtCPCumplimientoAliado").addClass("ColorCumplimiento");
    }

}

// Clientes nuevos avance , tendencia y cumplimiento
function avanceCNAliado() {

    var base = document.getElementById("txtCNBaseAliado").value;
    var real = document.getElementById("txtClieNuevAliadoReal").value;

    // var base = $("#txtCNBaseAliado").value();
    //var real = $("#txtClieNuevAliadoReal").value;;

    if (base == "" | real == "" | real == "-") {
        $("#txtCNAvanceAliado").val(0);

    }
    else {
        //BASE
        //var avance = Math.round((parseFloat(real) / parseFloat(base)) * 100)

        base = base.replace(/,/g, "")
        real = real.replace(/,/g, "")

        //$("#txtMetCarteraAvance").val(avance.toFixed(2) + " % ")
        var avance = Math.round((parseInt(real) / parseInt(base)) * 100)
        $("#txtCNAvanceAliado").val(avance + " % ")
    }
}
function tendenciaCNAliado() {

    //var real = $("#txtClieNuevReal").val();
    //var habiles = $("#txtDiasHabiles").val();
    //var transcurrido = $("#txtDiasTransc").val();

    var real = document.getElementById("txtClieNuevAliadoReal").value;
    var habiles = document.getElementById("txtDiasHabilesAliado").value;
    var transcurrido = $("#txtDiasTranscAliado").val();

    if (real == "" | habiles == "" | real == "-") {
        $("#txtCNTendenciaAliado").val(0);

    } else {
        //real = real.replace(/,/g, "")
        //habiles = habiles.replace(/,/g, "")
        //transcurrido = transcurrido.replace(/,/g, "")

        real = real.replace(",", "");
        habiles = habiles.replace(",", "");
        transcurrido = transcurrido.replace(",", "");

        var tendencia = Math.round((real / transcurrido) * habiles)
        $("#txtCNTendenciaAliado").val(formatNumber(tendencia));

    }

}
function cumplimientoCNAliado() {
    $('#txtCNCumplimientoAliado').removeClass('ColorCumplimiento');

    var tendencia = $("#txtCNTendenciaAliado").val();
    var base = $("#txtCPBaseAliado").val();
    var cumplimiento;

    if (tendencia == "" | base == "" | base == "-" | tendencia == "0") {
        $("#txtCNCumplimientoAliado").val(0);

    } else {
        tendencia = tendencia.replace(/,/g, "")
        base = base.replace(/,/g, "")

        cumplimiento = (parseFloat(tendencia) / parseFloat(base)) * 100
        $("#txtCNCumplimientoAliado").val(parseInt(cumplimiento) + " % ")

    }

    //PINTAR EL NEGATIVO TEXTBOX
    if (cumplimiento < 100) {
        $("#txtCNCumplimientoAliado").addClass("ColorCumplimiento");
    }
}




function tendenciaAliado(real) {

    var habiles = document.getElementById("txtDiasHabilesAliado").value;
    var transcurrido = $("#txtDiasTranscAliado").val();

    if (real == "" | habiles == "" | real == "-") {
        tendencia = 0;

    } else {

        tendencia = (real / transcurrido) * habiles
        //$("#txtCPTendenciaProducto").val(formatNumber(tendencia));        
    }
    return tendencia;
}

function avanceAliado(real, base) {
    //var base = document.getElementById("txtCNBaseAliado").value;
    //var real = document.getElementById("txtClieNuevAliadoReal").value;

    if (base == "" | real == "" | real == "-") {
        //$("#txtCNAvanceAliado").val(0);
        avance = (0).toFixed(2);
    }
    else {
        //BASE
        //var avance = Math.round((parseFloat(real) / parseFloat(base)) * 100)

        base = base.replace(/,/g, "")
        real = real.replace(/,/g, "")

        //$("#txtMetCarteraAvance").val(avance.toFixed(2) + " % ")
        avance = (parseFloat(real) / parseFloat(base)) * 100
       // $("#txtCNAvanceAliado").val(avance + " % ")
    }
    return avance;
}

function cumplimientoAliado(base, tendencia) {

        tendencia = tendencia.replace(/,/g, "")
        base = base.replace(/,/g, "")

        cumplimiento = (parseFloat(tendencia) / parseFloat(base)) * 100
        cumplimiento = parseInt(cumplimiento);


        return cumplimiento;
}