﻿function cargarTablasIRP() {
    //IRP
    $.ajax({
        type: 'POST',
        url: '@Url.Action("IRP")',
        dataType: 'json',
        //data: {  },
        success: function (json) {

            $("#IRP").dataTable().fnDestroy();

            //LlenarTabla(json);
            let $dt = $('#IRP');

            let dt = $dt.DataTable({
                data: json,
                columns: [
                      { data: "Ann" },
                      { data: "Ene" },
                      { data: "Feb" },
                      { data: "Mar" },
                      { data: "Abr" },
                      { data: "May" },
                      { data: "Jun" },
                      { data: "Jul" },
                      { data: "Ago" },
                      { data: "Sep" },
                      { data: "Oct" },
                      { data: "Nov" },
                      { data: "Dic" }
                ],
                "language": {
                    "sProcessing": "Procesando...",
                    "sLengthMenu": "Mostrar _MENU_ registros",
                    "sZeroRecords": "No se encontraron resultados",
                    "sEmptyTable": "Ningún dato disponible en esta tabla",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "info": "",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }

            });

        }
    })

    //IRP ALTA
    $.ajax({
        type: 'POST',
        url: '@Url.Action("IRPAlta")',
        dataType: 'json',
        //data: {  },
        success: function (json) {

            $("#IRPALTA").dataTable().fnDestroy();

            //LlenarTabla(json);
            let $dt = $('#IRPALTA');

            let dt = $dt.DataTable({
                data: json,
                columns: [
                      { data: "Ann" },
                      { data: "Ene" },
                      { data: "Feb" },
                      { data: "Mar" },
                      { data: "Abr" },
                      { data: "May" },
                      { data: "Jun" },
                      { data: "Jul" },
                      { data: "Ago" },
                      { data: "Sep" },
                      { data: "Oct" },
                      { data: "Nov" },
                      { data: "Dic" }
                ],
                "language": {
                    "sProcessing": "Procesando...",
                    "sLengthMenu": "Mostrar _MENU_ registros",
                    "sZeroRecords": "No se encontraron resultados",
                    "sEmptyTable": "Ningún dato disponible en esta tabla",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "info": "",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }

            });

        }
    })

    //IRPBAJA
    $.ajax({
        type: 'POST',
        url: '@Url.Action("IRPBaja")',
        dataType: 'json',
        //data: {  },
        success: function (json) {

            $("#IRPBAJA").dataTable().fnDestroy();

            //LlenarTabla(json);
            let $dt = $('#IRPBAJA');

            let dt = $dt.DataTable({
                data: json,
                columns: [
                      { data: "Ann" },
                      { data: "Ene" },
                      { data: "Feb" },
                      { data: "Mar" },
                      { data: "Abr" },
                      { data: "May" },
                      { data: "Jun" },
                      { data: "Jul" },
                      { data: "Ago" },
                      { data: "Sep" },
                      { data: "Oct" },
                      { data: "Nov" },
                      { data: "Dic" }
                ],
                "language": {
                    "sProcessing": "Procesando...",
                    "sLengthMenu": "Mostrar _MENU_ registros",
                    "sZeroRecords": "No se encontraron resultados",
                    "sEmptyTable": "Ningún dato disponible en esta tabla",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "info": "",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }

            });

        }
    })


    //IRPACUMULADO
    $.ajax({
        type: 'POST',
        url: '@Url.Action("IRPAcumulado")',
        dataType: 'json',
        //data: {  },
        success: function (json) {

            $("#IRPACUMULADO").dataTable().fnDestroy();

            //LlenarTabla(json);
            let $dt = $('#IRPACUMULADO');

            let dt = $dt.DataTable({
                data: json,
                columns: [
                      { data: "Ann" },
                      { data: "Ene" },
                      { data: "Feb" },
                      { data: "Mar" },
                      { data: "Abr" },
                      { data: "May" },
                      { data: "Jun" },
                      { data: "Jul" },
                      { data: "Ago" },
                      { data: "Sep" },
                      { data: "Oct" },
                      { data: "Nov" },
                      { data: "Dic" }
                ],
                "language": {
                    "sProcessing": "Procesando...",
                    "sLengthMenu": "Mostrar _MENU_ registros",
                    "sZeroRecords": "No se encontraron resultados",
                    "sEmptyTable": "Ningún dato disponible en esta tabla",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "info": "",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }

            });

        }
    })
}