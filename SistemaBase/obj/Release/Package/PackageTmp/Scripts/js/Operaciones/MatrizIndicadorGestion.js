﻿
//Cargar todas las sucursales
//function cargarSucursaljs() {
//    //var url = '@Url.Action("Sucursal", "MatrizGestion")';
//    var urls = '/MatrizGestion/Sucursal';
   
//    $.ajax({
//        type: 'POST',
//        url: urls,
//        dataType: 'json',
//        success: function (Sucursal) {            
//            $.each(Sucursal, function (i, Sucursal) {
//                $("#NomSuc").append('<option value="' + Sucursal.Value + '">' + Sucursal.Text + '</option>');
//            });
//        },
//    }); return false;
//}

//funcion limpiar textbox de la matriz de gestion empeños, cuando se realice el cambio de la sucursal
function limpText() {
    //Numero folios     
    $("#txtFolios").val("");
    $("#txtNumCte").val("");
    //BASE
    $("#txtPromEmpBase").val("");
    $("#txtPorcEnaBase").val("");
    //$("#txtCartApaBase").val("");
    //$("#txtCartCelBase").val("");
    //$("#txtCartJoyBase").val("");
    $("#txtCapPresBase").val("");
    $("#txtClieNuevBase").val("");
    $("#txtMetIngreBase").val("");
    $("#txtIngVtaApaBase").val("");
    $("#txtIngVtaCelBase").val("");
    $("#txtIngVtaJoyBase").val("");
    $("#txtIngTotalBase").val("");
    $("#txtCartTotalBase").val("");
    $("#txtCartTotalBase").val("");
    $("#txtIngACJBase").val("");
    $("#txtCartTotalReal").val("");
    $("#txtClieNuevVtaBase").val("");
    
        
    //REAL
    $("#txtPromEmpReal").val("");
    $("#txtPorcEnaReal").val("");
    //$("#txtCartApaReal").val("");
    //$("#txtCartCelReal").val("");
    //$("#txtCartJoyReal").val("");
    $("#txtCapPresReal").val("");
    $("#txtClieNuevReal").val("");
    $("#txtMetIngreReal").val("");
    $("#txtIngVtaApaReal").val("");
    $("#txtIngVtaCelReal").val("");
    $("#txtIngVtaJoyReal").val("");
    $("#txtIngTotalReal").val("");
    $("#txtCartTotalReal").val("");
    $("#txtCartTotalReal").val("");

    $("#txtCartApaReal").val("");
    $("#txtCartCelReal").val("");
    $("#txtCartJoyReal").val("");
    $("#txtIngACJReal").val("");
    $("#txtClieNuevVtaReal").val("");
    
    

    //AVANCE
    //$("#txtCartApaAvance").val("");
    //$("#txtCartCelAvance").val("");
    //$("#txtCartJoyAvance").val("");
    $("#txtCapPresAvance").val("");
    $("#txtClieNuevAvance").val("");
    $("#txtMetIngreAvance").val("");
    $("#txtIngVtaApaAvance").val("");
    $("#txtIngVtaCelAvance").val("");
    $("#txtIngVtaJoyAvance").val("");
    $("#txtIngTotalAvance").val("");
    $("#txtCartTotalAvance").val("");
    $("#txtIngACJAvance").val("");
    $("#txtClieNuevVtaAvance").val("");
    

    //TENDENCIA
    //$("#txtCartApaTendencia").val("");
    //$("#txtCartCelTendencia").val("");
    //$("#txtCartJoyTendencia").val("");
    $("#txtCapPresTendencia").val("");
    $("#txtClieNuevTendencia").val("");
    $("#txtMetIngreTendencia").val("");
    $("#txtIngVtaApaTendencia").val("");
    $("#txtIngVtaCelTendencia").val("");
    $("#txtIngVtaJoyTendencia").val("");
    $("#txtIngTotalTendencia").val("");
    $("#txtCartTotalTendencia").val("");
    $("#txtIngACJTendencia").val(""); 
    $("#txtClieNuevVtaTendencia").val("");

    //CUMPLIMIENTO
    $("#txtPromEmpCumplimiento").val("");
    $("#txtPorcEnaCumplimiento").val("");
    //$("#txtCartApaCumplimiento").val("");
    //$("#txtCartCelCumplimiento").val("");
    //$("#txtCartJoyCumplimiento").val("");
    $("#txtCapPresCumplimiento").val("");
    $("#txtClieNuevCumplimiento").val("");
    $("#txtMetIngreCumplimiento").val("");
    $("#txtIngVtaApaCumplimiento").val("");
    $("#txtIngVtaCelCumplimiento").val("");
    $("#txtIngVtaJoyCumplimiento").val("");
    $("#txtIngTotalCumplimiento").val("");
    $("#txtCartTotalCumplimiento").val("");
    $("#txtIngACJCumplimiento").val("");
    $("#txtClieNuevVtaCumplimiento").val("");

    //Quitar color negativo del cumplimiento
    $("#txtPromEmpCumplimiento").removeClass('ColorCumplimiento');
    $("#txtPorcEnaCumplimiento").removeClass('ColorCumplimiento');
    //$("#txtCartApaCumplimiento").removeClass('ColorCumplimiento');
    //$("#txtCartCelCumplimiento").removeClass('ColorCumplimiento');
    //$("#txtCartJoyCumplimiento").removeClass('ColorCumplimiento');
    $("#txtCapPresCumplimiento").removeClass('ColorCumplimiento');
    $("#txtClieNuevCumplimiento").removeClass('ColorCumplimiento');
    $("#txtMetIngreCumplimiento").removeClass('ColorCumplimiento');
    $("#txtIngVtaApaCumplimiento").removeClass('ColorCumplimiento');
    $("#txtIngVtaCelCumplimiento").removeClass('ColorCumplimiento');
    $("#txtIngVtaJoyCumplimiento").removeClass('ColorCumplimiento');
    $("#txtIngTotalCumplimiento").removeClass('ColorCumplimiento');
    $("#txtCartTotalCumplimiento").removeClass('ColorCumplimiento');
    $("#txtIngACJCumplimiento").removeClass('ColorCumplimiento');
    $("#txtClieNuevVtaCumplimiento").removeClass('ColorCumplimiento');



    //QUITAR LOS ID DE LAS METAS Y LA SUCURSAL QUE SE PINTAN DINAMICAMENTE 
    $("#IDMetaPorMax").remove();
    $("#IDSucursalPorMax").remove();

}

//INPUTS DIAS HABILES Y DIAS TRANSCURRIDOS
function diasjs() {

    var diasArray = [];
    var date = new Date();
    //DIAS HABILES
    var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
    var habiles = lastDay.getDate();
    diasArray.push(habiles)
    //DIAS HABILES TRANSCURRIDOS (EL DIA DE HOY ,DIA ACTUAL )
    //dias habiles(FUNCIONA AL CARGAR AL ABRIR EL FORMULARIO, SIN NECESIDAD DE CINECTARSE A LA SUCURSAL)
    var transcurridos = date.getDate();
    diasArray.push(transcurridos)
    //diasRestante();
    ///ARREGLO CON LOS DIAS TRANSCURRIDOS Y LOS DIAS HABILES
    return diasArray;
    
}

//Restar dias habiles a los dias transcurridos 
function diasRestantejs(habiles, transcurrido) {
    var DiasRestantes = parseFloat(habiles) - parseFloat(transcurrido)
    return DiasRestantes;
}

// PORCENTAJE MAXIMO DE ENAJENACION 
function cumplimientoPorcMaxEnajs(real, base) {

    var cumplimiento = Math.round(parseInt(real) / parseInt(base) * 100).toFixed(2);
    return cumplimiento;
}

function AddComPromPres() {
    var comentario = $("#ComPromPres").val();
    var BaseID = $("#IDBPromPres").val();
}


//$("#AddComPromPres").click(function () {
//    var comentario = $("#ComPromPres").val();
//    var BaseID = $("#IDBPromPres").val();


//    alert(comentario + BaseID);

//            $.ajax({
//                type: "POST",
//                //url: url,
//                url : "../MatrizGestion/AddComentario",
//                data: parametros,
//                contentType: false, //importante enviar este parametro en false
//                processData: false, //importante enviar este parametro en false
//                data: { comentario, BaseID },
//                success: function (data) {

//                    //$('#mensajeModal').modal('show');
//                    //$('#resultado').html(data);
//                },
//                error: function (r) {

//                    alert("Error del servidor");
//                }
//            });

//})




////Datos informativos 
////$(document).ready(function () {

//    //sobreescribimos el metodo submit para que envie la solicitud por ajax
//    $("#ModalPP").submit(function (e) {

//        //esto evita que se haga la petición común, es decir evita que se refresque la pagina
//        e.preventDefault();

//        //ruta la cual recibira nuestro archivo
//        //url = "@Url.Action('AddComentario', 'MatrizGestion')"
//        url = "../MatrizGestion/AddComentario";
//        //var url = '@Url.Action("Sucursal", "MatrizGestion")';

//        //FormData es necesario para el envio de archivo,
//        //y de la siguiente manera capturamos todos los elementos del formulario
//        var parametros=new FormData($(this)[0])

//        //realizamos la petición ajax con la función de jquery
//        $.ajax({
//            type: "POST",
//            url: url,
//            data: parametros,
//            contentType: false, //importante enviar este parametro en false
//            processData: false, //importante enviar este parametro en false
//            success: function (data) {

//                $('#mensajeModal').modal('show');
//                $('#resultado').html(data);
//            },
//            error: function (r) {

//                alert("Error del servidor");
//            }
//        });

//    })

////})

