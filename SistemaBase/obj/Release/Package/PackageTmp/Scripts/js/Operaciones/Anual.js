﻿function ViewAnual(){

    //DIAS HABILES
    var diasArray = [];
    //archivo js
    var diasArray = diasjs();

    var DHabiles = $("#txtDiasHabilesAnual").val(diasArray[0]);
    var DTrans = $("#txtDiasTranscAnual").val(diasArray[1]);
    //dias restantes
    $("#txtDiasRestantesAnual").val(diasArray[0] - diasArray[1])


    //SUCURSAL
    $.ajax({
        url: "../MatrizGestion/Sucursal",
        method: "POST",
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        success: function (SucursalAliado) {

            $.each(SucursalAliado, function (i, SucursalAliado) {
                $("#NomSucAnual").append('<option value="' + SucursalAliado.Value + '">' + SucursalAliado.Text + '</option>');
            });

        },
    });

    //AL SELECCIONAR UNA SUCURSAL
    $("#NomSucAnual").change(function () {
        //LIMPIAR EL COMBO DE ALIADO PARA CUANDO VUELVA  A ELEGIR
        //$("#AliadoAliado").empty();

        //var NomSuc = $("#SucAliado").val();
        //document.getElementById('TimeAliado').style.display = 'inline';

        //Sacar el nombre de la sucursal
        var combo = document.getElementById("NomSucAnual");
        var NomSuc = combo.options[combo.selectedIndex].text;
        //CVe sucursal para conectar al servidor de empeños y buscar lis aliados
        var CveSuc = combo.options[combo.selectedIndex].value;

        $.ajax({            
            type: 'POST',
            dataType: 'json',
            url: "../MatrizGestion/AnioSucursal",
            data: { NomSuc, CveSuc},
            success: function (AliadoAliado) {
               
                $.each(AliadoAliado, function (i, anio) {
                    $("#anioAnual").append('<option value="' + anio.Value + '">' + anio.Text + '</option>');
                });
                document.getElementById('TimeAliado').style.display = 'none';

            },
        })
    });


    //AL SELECCIONAR UN AÑO
    $("#anioAnual").change(function () {
        //LIMPIAR EL COMBO DE ALIADO PARA CUANDO VUELVA  A ELEGIR
        //$("#AliadoAliado").empty();

        //var NomSuc = $("#SucAliado").val();
        //document.getElementById('TimeAliado').style.display = 'inline';

        //Sacar el nombre de la sucursal
        var combo = document.getElementById("NomSucAnual");
        var NomSuc = combo.options[combo.selectedIndex].text;
        //CVe sucursal para conectar al servidor de empeños y buscar lis aliados
        var CveSuc = combo.options[combo.selectedIndex].value;

        //Sacar el nombre de la sucursal
        var ann = document.getElementById("anioAnual");
        var AnnEmp = ann.options[ann.selectedIndex].text;
        //CVe sucursal para conectar al servidor de empeños y buscar lis aliados
       // var AnnEmp = ann.options[ann.selectedIndex].value;
        limpiarAnual();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "../MatrizGestion/IndAnual",
            data: { NomSuc, CveSuc, AnnEmp},
            success: function (Ind) {
                //REAL PRESTAMO PROMEDIO
                $("#txtPromEmpRealAnn").val(formatNumber(Ind.PromedioSem));

                //Numero de empeños
                $("#txtFoliosAnn").val(Ind.FolioCapPrest);

                //REAL PORCENTAJE MAXIMO DE ENAJENACION
                $("#txtPorcEnRealAnn").val(Ind.Folios);

                //REAL CARTERA (Aparato, joyeria y celulares)
                Ind.CarteraView.forEach(cartera);
                function cartera(item, index) {

                    if (item.Tipo.trim() == "Aparatos") {

                        var resCA = formatNumber(item.CapitalVigente);
                        var reaCAP = $("#txtCartApaRealAnn").val(resCA);

                    }
                    if (item.Tipo.trim() == "Celulares") {
                        var resCC = formatNumber(item.CapitalVigente);
                        var reaCCl = $("#txtCartCelRealAnn").val(resCC);

                    }
                    if (item.Tipo.trim() == "Joyeria") {

                        var resCJ = formatNumber(item.CapitalVigente);
                        var reaCJo = $("#txtCartJoyRealAnn").val(resCJ);

                    }
                }
                //REAL CARTERA TOTAL
                $("#txtCartTotalRealAnn").val(formatNumber(Ind.CarteraTotal));

                //REAL CAPITAL PRESTADO
                $("#txtCapPresRealAnn").val(formatNumber(Ind.Cartera));
                //REAL CLIETES NUEVOS EMPEÑOS Y Promedio de préstamo clientes nuevos
                $("#txtNumCteEmpAnn").val(formatNumber(Ind.TotalCteNvo));
                $("#txtClieNuevRealAnn").val(Ind.NumClientes);

                //REAL CLIENTES NUEVOS VENTAS
                $("#txtClieNuevVtaRealAnn").val(formatNumber(Ind.NumClientesVta));

                //REAL INGRESO EMPEÑO 
                $("#txtMetIngreRealAnn").val(formatNumber(Ind.ingreso));

                //REAL Ingreso venta (aparatos, celulares y joyeria)
                Ind.IngresoVenta.forEach(checkar);
                function checkar(item, index) {

                    if (item.Tipo.trim() == "Aparatos") {
                        var realIV = $("#txtIngVtaApaRealAnn").val(formatNumber(item.Ingreso));
                    }
                    if (item.Tipo.trim() == "Celulares") {
                        var reaIC = $("#txtIngVtaCelRealAnn").val(formatNumber(item.Ingreso));
                    }
                    if (item.Tipo.trim() == "Joyeria") {
                        var reaIJ = $("#txtIngVtaJoyRealAnn").val(formatNumber(item.Ingreso));
                    }
                }

                var realIV = $("#txtIngVtaApaRealAnn").val().replace(/,/g, "");
                var reaIC = $("#txtIngVtaCelRealAnn").val().replace(/,/g, "");
                var reaIJ = $("#txtIngVtaJoyRealAnn").val().replace(/,/g, "");

                var suma = parseFloat(realIV) + parseFloat(reaIC) + parseFloat(reaIJ)

                //REAL INGRESO APARATPS, CELULAR Y JOYERIA 
                $("#txtIngACJRealAnn").val(formatNumber(suma));

                var ing = $("#txtMetIngreRealAnn").val().replace(/,/g, "");
                //REAL INGRESO TOTAL (empeños - venta)
                $("#txtIngTotalRealAnn").val(formatNumber(suma +parseFloat(ing)));
                
                //document.getElementById('TimeAliado').style.display = 'none';
                metaAnn(Ind.ValorMeta);
            },
        })
    });
}

function metaAnn(valor) {

    valor.forEach(metaItem);
    function metaItem(item, index) {

        if (item.NomIndicador.trim() == $("#MetaAnnPresProm").val()) {
            //Meta
            $("#txtPresPromMetaAnn").val(formatNumber(item.meta));
            //Real
            var meta = $("#txtPromEmpRealAnn").val();
            var cumplimiento = CumplimientoAnn(item.meta, meta);            
          
            if (cumplimiento != 0) {
                $("#txtPromEmpCumptoAnn").val(parseInt(cumplimiento) + "%");
            } else {
                $("#txtPromEmpCumptoAnn").val("0 %");
            }
            if (cumplimiento < 100) {
                $("#txtPromEmpCumptoAnn").addClass("ColorCumplimiento");
            }
        }
        if (item.NomIndicador.trim() == $("#MetaAnnPorcEnaje").val()) {
            $("#txtPorcEnMetaAnn").val(formatNumber(item.meta));
        }
        if (item.NomIndicador.trim() == $("#MetaAnnPorcEnaje").val()) {
            $("#txtPorcEnMetaAnn").val(formatNumber(item.meta));
        }
        if (item.NomIndicador.trim() == $("#MetaCarTotalAnn").val()) {
            $("#txtCartTotalMetaAnn").val(formatNumber(item.meta));
            var real = $("#txtCartTotalMetaAnn").val(formatNumber(item.meta));
            //AVANCE
            avance = avanceAliado(real, item.meta);
            if (avance != 0) {
                $("#txtCPAvanceAliado").val(parseInt(avance) + " % ");
            } else {
                $("#txtCPAvanceAliado").val("0 %");
            }
        }
        if (item.NomIndicador.trim() == $("#MetaCapPrestado").val()) {
            $("#txtCapPresMeta").val(formatNumber(item.meta));
        }
        if (item.NomIndicador.trim() == $("#MetaClieNvoEmpAnn").val()) {
            $("#txtClieNuevMetaAnn").val(formatNumber(item.meta));
        }
        if (item.NomIndicador.trim() == $("#MetaBaseClieNvoVtaAnn").val()) {
            $("#txtClieNuevVtaMetaAnn").val(formatNumber(item.meta));
        }
        if (item.NomIndicador.trim() == $("#MetaMetIngresoAnn").val()) {
            $("#txtMetIngreMEtaAnn").val(formatNumber(item.meta));
        }
        if (item.NomIndicador.trim() == $("#MetaIngVtaApaAnn").val()) {
            $("#txtIngVtaApaMetaAnn").val(formatNumber(item.meta));
        }
        if (item.NomIndicador.trim() == $("#MetaIngVtaCelAnn").val()) {
            $("#txtIngVtaCelMetaAnn").val(formatNumber(item.meta));
        }
        if (item.NomIndicador.trim() == $("#BaseIngVtaJoyAnn").val()) {
            $("#txtIngVtaJoyMetaAnn").val(formatNumber(item.meta));
        }

        if (item.NomIndicador.trim() == $("#MetaIngACJAnn").val()) {
            $("#txtIngACJMetaAnn").val(formatNumber(item.meta));
        }

        if (item.NomIndicador.trim() == $("#MetaTotIngresoAnn").val()) {
            $("#txtIngTotalMetaAnn").val(formatNumber(item.meta));
        }







        if (item.NomIndicador.trim() == $("#MetaCPAliado").val()) {
            $("#txtCPBaseAliado").val(formatNumber(item.meta));


            real = $("#txtCPresAliadoReal").val();
            meta = $("#txtCPBaseAliado").val();
            tendencia = $("#txtCPTendenciaAliado").val();
            //AVANCE
            avance = avanceAliado(real, meta);
            if (avance != 0) {
                $("#txtCPAvanceAliado").val(parseInt(avance) + " % ");
            } else {
                $("#txtCPAvanceAliado").val("0 %");
            }

            //CUMPLIMIENTO
            //var cumplimiento = $("#txtCPCumplimientoAliado").val(cumplimientoAliado(real, item.meta));
            $('#txtCPCumplimientoAliado').removeClass('ColorCumplimiento');

            var cumplimiento = cumplimientoAliado(meta, tendencia);


            if (cumplimiento != 0) {
                $("#txtCPCumplimientoAliado").val(parseInt(cumplimiento) + "%");
            } else {
                $("#txtCPCumplimientoAliado").val("0 %");
            }

            if (cumplimiento < 100) {
                $("#txtCPCumplimientoAliado").addClass("ColorCumplimiento");
            }
        }
        if (item.NomIndicador.trim() == $("#MetaCNAliado").val()) {
            $("#txtCNBaseAliado").val(item.meta);

            real = $("#txtClieNuevAliadoReal").val();
            meta = $("#txtCNBaseAliado").val();
            tendencia = $("#txtCNTendenciaAliado").val();
            //AVANCE
            var avance = avanceAliado(real, meta);
            if (avance != 0) {
                $("#txtCNAvanceAliado").val(parseInt(avance) + " % ");
            } else {
                $("#txtCNAvanceAliado").val("0 % ");
            }

            //CUMPLIMIENTO
            $('#txtCNCumplimientoAliado').removeClass('ColorCumplimiento');

            var cumplimiento = cumplimientoAliado(meta, tendencia);

            if (cumplimiento != 0) {
                $("#txtCNCumplimientoAliado").val(parseInt(cumplimiento) + "%");
            } else {
                $("#txtCNCumplimientoAliado").val("0 %");
            }

            if (cumplimiento < 100) {
                $("#txtCNCumplimientoAliado").addClass("ColorCumplimiento");
            }
        }
        if (item.NomIndicador.trim() == $("#MetaIEAliado").val()) {
            $("#txtIEBaseAliado").val(formatNumber(item.meta));

            real = $("#txtIERealAliado").val();
            meta = $("#txtIEBaseAliado").val();
            tendencia = $("#txtIETendenciaAliado").val();
            //AVANCE
            avance = avanceAliado(real, meta);

            if (avance != 0) {
                $("#txtIEAvanceAliado").val(parseInt(avance) + " % ");
            } else { $("#txtIEAvanceAliado").val("0 % "); }

            //CUMPLIMIENTO
            $('#txtIECumplimientoAliado').removeClass('ColorCumplimiento');
            var cumplimiento = cumplimientoAliado(meta, tendencia);

            if (cumplimiento != 0) {
                $("#txtIECumplimientoAliado").val(parseInt(cumplimiento) + "%");
            } else {
                $("#txtIECumplimientoAliado").val("0 %");
            }


            if (cumplimiento < 100) {
                $("#txtIECumplimientoAliado").addClass("ColorCumplimiento");
            }

        }
        if (item.NomIndicador.trim() == $("#MetaIVAAliado").val()) {
            $("#txtIVApaAliadoBase").val(formatNumber(item.meta));

            real = $("#txtIVApaAliadoReal").val();
            meta = $("#txtIVApaAliadoBase").val();
            tendencia = $("#txtIVApaAliadoTendencia").val();
            //AVANCE
            avance = avanceAliado(real, meta);
            if (avance != 0) {
                $("#txtIVApaAliadoAvance").val(parseInt(avance) + " % ");
            } else { $("#txtIVApaAliadoAvance").val("0 % "); }

            //CUMPLIMIENTO
            $('#txtIVApaAliadoCumplimiento').removeClass('ColorCumplimiento');
            var cumplimiento = cumplimientoAliado(meta, tendencia);

            if (cumplimiento != 0) {
                $("#txtIVApaAliadoCumplimiento").val(parseInt(cumplimiento) + "%");
            } else { $("#txtIVApaAliadoCumplimiento").val("0 %"); }


            if (cumplimiento < 100) {
                $("#txtIVApaAliadoCumplimiento").addClass("ColorCumplimiento");
            }

        }
        if (item.NomIndicador.trim() == $("#MetaIVCAliado").val()) {
            $("#txtIVCelAliadoBase").val(formatNumber(item.meta));
            real = $("#txtIVCelAliadoReal").val();
            meta = $("#txtIVCelAliadoBase").val();
            tendencia = $("#txtIVCelAliadoTendencia").val();
            //AVANCE
            avance = avanceAliado(real, meta);
            if (avance != 0) {
                $("#txtIVCelAliadoAvance").val(parseInt(avance) + " % ");
            } else { $("#txtIVCelAliadoAvance").val("0 % "); }

            //CUMPLIMIENTO
            $('#txtIVCelAliadoCumplimiento').removeClass('ColorCumplimiento');

            var cumplimiento = cumplimientoAliado(meta, tendencia);
            if (cumplimiento != 0) {
                $("#txtIVCelAliadoCumplimiento").val(parseInt(cumplimiento) + "%");
            } else { $("#txtIVCelAliadoCumplimiento").val("0 %"); }


            if (cumplimiento < 100) {
                $("#txtIVCelAliadoCumplimiento").addClass("ColorCumplimiento");
            }

        }
        if (item.NomIndicador.trim() == $("#MetaIVJAliado").val()) {
            $("#txtIVJoyAliadoBase").val(formatNumber(item.meta));
            real = $("#txtIVJoyAliadoReal").val();
            meta = $("#txtIVJoyAliadoBase").val();
            tendencia = $("#txtIVJoyAliadoTendencia").val();

            //AVANCE
            avance = avanceAliado(real, meta);
            if (avance != 0) {
                $("#txtIVJoyAliadoAvance").val(parseInt(avance) + " % ");
            } else { $("#txtIVJoyAliadoAvance").val("0 % "); }

            //CUMPLIMIENTO
            $('#txtIVJoyAliadoCumplimiento').removeClass('ColorCumplimiento');
            var cumplimiento = cumplimientoAliado(meta, tendencia);

            if (cumplimiento != 0) {
                $("#txtIVJoyAliadoCumplimiento").val(parseInt(cumplimiento) + "%");
            } else { $("#txtIVJoyAliadoCumplimiento").val("0 %"); }

            if (cumplimiento < 100) {
                $("#txtIVJoyAliadoCumplimiento").addClass("ColorCumplimiento");
            }

        }
        if (item.NomIndicador.trim() == $("#MetaIACJAliado").val()) {
            $("#txtIACJTotalAliadoBase").val(formatNumber(item.meta));

            real = $("#txtIACJAliadoReal").val();
            meta = $("#txtIACJTotalAliadoBase").val();
            tendencia = $("#txtIACJAliadoTendencia").val();
            //AVANCE
            avance = avanceAliado(real, meta);
            if (avance != 0) {
                $("#txtIACJAliadoAvance").val(parseInt(avance) + " % ");
            } else { $("#txtIACJAliadoAvance").val("0 % "); }

            //CUMPLIMIENTO
            $('#txtIACJAliadoCumplimiento').removeClass('ColorCumplimiento');

            var cumplimiento = cumplimientoAliado(meta, tendencia);
            if (cumplimiento != 0) {
                $("#txtIACJAliadoCumplimiento").val(parseInt(cumplimiento) + "%");
            } else { $("#txtIACJAliadoCumplimiento").val("0 %"); }


            if (cumplimiento < 100) {
                $("#txtIACJAliadoCumplimiento").addClass("ColorCumplimiento");
            }

        }
        if (item.NomIndicador.trim() == $("#MetaITAliado").val()) {
            $("#txtITotalAliadoBase").val(formatNumber(item.meta));

            real = $("#txtITAliadoReal").val();
            meta = $("#txtITotalAliadoBase").val();
            tendencia = $("#txtITAliadoTendencia").val();
            //AVANCE
            avance = avanceAliado(real, meta);
            if (avance != 0) {
                $("#txtITAliadoAvance").val(parseInt(avance) + " % ");
            } else { $("#txtITAliadoAvance").val("0 % "); }

            //CUMPLIMIENTO
            $('#txtITAliadoCumplimiento').removeClass('ColorCumplimiento');

            var cumplimiento = cumplimientoAliado(meta, tendencia);
            if (cumplimiento != 0) { $("#txtITAliadoCumplimiento").val(parseInt(cumplimiento) + "%"); } else { $("#txtITAliadoCumplimiento").val("0 %"); }


            if (cumplimiento < 100) {
                $("#txtITAliadoCumplimiento").addClass("ColorCumplimiento");
            }

        }
    }
}

function limpiarAnual() {
    //META    
    $("#txtFoliosAnn").val("");
    $("#txtPromEmpRealAnn").val("");
    $("#txtPorcEnRealAnn").val("");
    $("#txtCartApaRealAnn").val("");
    $("#txtCartCelRealAnn").val("");
    $("#txtCartJoyRealAnn").val("");
    $("#txtCartTotalRealAnn").val("");
    $("#txtCapPresRealAnn").val("");
    $("#txtNumCteEmpAnn").val("");
    $("#txtClieNuevRealAnn").val("");
    $("#txtClieNuevVtaRealAnn").val("");
    $("#txtMetIngreRealAnn").val("");
    $("#txtIngVtaApaRealAnn").val("");
    $("#txtIngVtaCelRealAnn").val("");
    $("#txtIngVtaJoyRealAnn").val("");
    $("#txtIngACJRealAnn").val("");
    $("#txtIngTotalRealAnn").val("");


}

function AvanceAnn(real, base) {
    if (base == "" | real == "" | real == "-") {
        avance = (0).toFixed(2);
    }
    else {

        base = base.replace(/,/g, "");
        real = real.replace(/,/g, "");

        avance = (parseFloat(real) / parseFloat(base)) * 100
    }
    return avance;
}

function TendenciaAnn() {

}
function CumplimientoAnn(base, tendencia) {
    tendencia = tendencia.replace(/,/g, "")
    base = base.replace(/,/g, "")

    cumplimiento = (parseFloat(tendencia) / parseFloat(base)) * 100
    cumplimiento = parseInt(cumplimiento);


    return cumplimiento;
}
