﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ENTIDADES.Ticket;
using Utilidades.Utilidades;
namespace Validacion.Validacion
{
    public class TicketValidacion
    {

        public TicketModel Validar(TicketModel ticketMod)
        {
            TicketModel ticket = null;

            try {
                ticket = new TicketModel();
                ticket.codigoRespuesta = Utilidades.Utilidades.Constantes.CodigoExito[0];
                ticket.mensajeRespuesta = Utilidades.Utilidades.Constantes.CodigoExito[1];

                if (ticketMod.TicketID==0)
                {
                    ticket.codigoRespuesta = "Error-0001";
                    ticket.mensajeRespuesta = "El campo Ticket ID debe tener valor.";
                }
                if (ticket.Em_Cve_Empleado == "")
                {
                    ticket.codigoRespuesta = "Error-0002";
                    ticket.mensajeRespuesta = "El campo Aliado debe tener valor.";
                }
                if (ticket.DeptoAsignado == "")
                {
                    ticket.codigoRespuesta = "Error-0003";
                    ticket.mensajeRespuesta = "El campo Departamento debe tener valor.";
                }
                if (ticket.Comentario=="")
                {
                    ticket.codigoRespuesta = "Error-0004";
                    ticket.mensajeRespuesta = "El campo Comentario debe tener valor.";
                }
                if (ticket.TipoReporteID == 0)
                {
                    ticket.codigoRespuesta = "Error-0005";
                    ticket.mensajeRespuesta = "El campo Comentario debe tener valor.";
                }
            }
            catch(Exception)
            {
                throw;
            }

            return ticket;
        }

    }
}
