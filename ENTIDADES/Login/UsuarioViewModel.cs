﻿using ENTIDADES.Base;
using ENTIDADES.Permisos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ENTIDADES.Login
{
    public class UsuarioViewModel:BaseModel
    {

        public string CveEmpleado { get; set; }
        public string CodigoAlterno { get; set; }
        public string Password { get; set; }
        public string NombreEmp { get; set; }
        public string CveDepartamentoEmp { get; set; }
        public string NomDepartamdentoEmp { get; set; }
        public string CvePuestoEmpleado { get; set; }
        public string NomPuestoEmp { get; set; }
        public string CveSucursal { get; set; }
        public string NomSucursal { get; set; }
        public string EmReporta { get; set; }
        public string NumIMSS { get; set; }
        public  List<Modulo> listaModulo { get; set; }

        public string ValidatePassword { get; set; }
        public string ActPassword { get; set; }
        //Puesto para perfil de acceso , 
        public int Em_Puesto { get; set; }

    }
}
