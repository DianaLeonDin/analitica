﻿using ENTIDADES.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ENTIDADES.Login
{
    public class UsuarioOperViewModel:BaseModel
    {
        // datos del usuario
        public string Operador { get; set; }
        public string Perfil { get; set; }
        public string Clave { get; set; }
        public string ClaveN { get; set; }
        public string NombreUsuario { get; set; }
        public string EMail { get; set; }
        public string Sucursal { get; set; }
        // datos del perfil
        public string NombrePerfil { get; set; }
        public string Iniciales { get; set; }
        public string Categoria { get; set; }
        public short Nivel { get; set; }
        public string Departamento { get; set; }
    }
}
