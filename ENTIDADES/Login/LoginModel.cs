﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ENTIDADES.Login
{
    public class LoginModel
    {
        public string Operador { get; set; }
        public int Perfil { get; set; }
        public string Clave { get; set; }

        public string Nombre { get; set; }
    }
}
