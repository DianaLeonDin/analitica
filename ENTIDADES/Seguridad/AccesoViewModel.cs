﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ENTIDADES.Seguridad
{
    public class AccesoViewModel
    {
        public List<PermisoEmpleadoViewModel> Rol { get; set; }
        public List<PermisoEmpleadoViewModel> Modulo { get; set; }

        //CONTROL DE ACCESO , AÑO MES Y SUCURSAL DE LOS QUE HAN CONSULTADO LA INFORMACION EN LOS INDICADORES DE OPERACIONES
        public List<PermisoEmpleadoViewModel> anio { get; set; }
        public List<PermisoEmpleadoViewModel> mes { get; set; }
        public List<PermisoEmpleadoViewModel> sucursal { get; set; }

        //Puesto, empleado y sucursal
        public List<PermisoEmpleadoViewModel> Puesto { get; set; }
        public List<PermisoEmpleadoViewModel> Empleado { get; set; }
        //public List<PermisoEmpleadoViewModel> sucursal { get; set; }

    }
}
