﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ENTIDADES.Seguridad
{
    public class PermisoEmpleadoViewModel
    {
        public int RolID { get; set; }
        public int PermisoID { get; set; }
        public string Nombre { get; set; }
        public string Modulo { get; set; }
        public string Descripcion { get; set; }
        public string Pe_Cve_Puesto_Empleado { get; set; }
        
        public string Usuario { get; set; }
        public DateTime Fecha { get; set; }
        public string SucursalID { get; set; }
        public string Sucursal { get; set; }
        public int Total { get; set; }
        public string FechaVar { get; set; }


        public int anio { get; set; }
        public string mes { get; set; }
        public int nummes { get; set; }
        public int comentarios { get; set; }
        public int visitas { get; set; }
    }
}
