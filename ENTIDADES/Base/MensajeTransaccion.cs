﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ENTIDADES.Base
{
    public class MensajeTransaccion
    {
        public string MensajeRespuesta { get; set; }
        public string CodigoRespuesta { get; set; }

        // public int Consecutivo { get; set; }
        public string Consecutivo { get; set; }

    }
}
