﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ENTIDADES.Base
{
    public class BaseModel
    {
        public String codigoRespuesta { get; set; }
        public String mensajeRespuesta { get; set; }
    }
}
