﻿using ENTIDADES.Base;
using ENTIDADES.Login;
using ENTIDADES.Permisos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERVICIO.Seguridad
{
    public interface IOperadorServicio
    {


        UsuarioViewModel OperadoresCon(Empleado usuario);
        MensajeTransaccion ActualizarContraseña(UsuarioViewModel usuarioResponse);

        List<Modulo> ListarAccesoModulo(PUESTOOPERACION puestoOperacion);

    }
}
