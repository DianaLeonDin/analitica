﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ENTIDADES.Mantenimiento
{
    public class OperacionesViewModel
    {
        //Catalogo operaciones --> matriz de indicadores de gestion -->Base
        public int BaseID { get; set; }
        public string Nombre { get; set; }
        public decimal Valor { get; set; }
        //public string Frecuencia { get; set; }
        public string SucursalID { get; set; }
        public string AliadoID { get; set; }
        public int Mes { get; set; }
        public int Anio { get; set; }

        public string mesAnio { get; set; }

        public string NomMes { get; set; }

        ////public DateTime FechaInhabil { get; set; }
        public string FechaInhabil { get; set; }


        //Cargar .os dias habiles
        public int DiasHabiles { get; set; }
        public int DiasTranscurridos { get; set; }
        public int Folio { get; set; }

        public string Descripcion { get; set; }

        public int Cantidad { get; set; }


        public List<OperacionesViewModel> SucMeta { get; set; }

       
       // public string[] SucMeta { get; set; }

        public string Meta { get; set; }


    }
}
