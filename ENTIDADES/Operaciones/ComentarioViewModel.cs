﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ENTIDADES.Operaciones
{
    public class ComentarioViewModel
    {

        public string Descripcion { get; set; }
        public int BaseID { get; set; }
        public string UsuarioID { get; set; }
        public string SucursalID { get; set; }
        public DateTime Fecha { get; set; }
        public string Nombre { get; set; }

    }
}
