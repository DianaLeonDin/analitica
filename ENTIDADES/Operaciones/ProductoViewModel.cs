﻿using ENTIDADES.Mantenimiento;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ENTIDADES.Operaciones
{
    public class ProductoViewModel
    {
        public List<MatrizGestionViewModel> Productos { get; set; }

        //Promedio monto empeño
        public decimal PromedioSem { get; set; }
        public decimal Cartera { get; set; }
        //META INGRESO
        public decimal ingreso { get; set; }
        //Clientes nuevos
        public int TotalCteNvo { get; set; }
        public string producto { get; set; }
        public List<IngresoVentaViewModel> IngresoVenta { get; set; }


                  //Productos lista
        public List<MatrizGestionViewModel> ProductosVM { get; set; }

    }
}
