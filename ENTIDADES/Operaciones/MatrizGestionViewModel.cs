﻿using ENTIDADES.Mantenimiento;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ENTIDADES.Operaciones
{
    public class MatrizGestionViewModel
    {
        //Scursales
        public int AnnEmp { get; set; }
        public string NomSuc { get; set; }
        public string CveSuc { get; set; }

        //%METAS CARTERA
        public decimal Cartera { get; set; }
        public string Producto { get; set; }

        public int Cve { get; set; }
        //
        public int CveUser { get; set; }
        public string Cusr { get; set; }
        public string nombre { get; set; }
        public string puesto { get; set; }

        //META INGRESO
        public decimal ingreso { get; set; }
        public decimal total { get; set; }

        //Clientes nuevos empeños
        public decimal TotalCteNvo { get; set; }
        public int NumClientes { get; set; }

        //Clientes nuevos ventas
        public int NumClientesVta { get; set; }

        //Promedio monto empeño
        public decimal PromedioSem { get; set; }
        public decimal FolioCapPrest { get; set; }

        //Porcentaje maximo enajenacion
        public decimal Folios { get; set; }

        //Igreso venta (celular, aparato joyeria)
        public string tipo { get; set; }

        public List<IngresoVentaViewModel> IngresoVenta { get; set; }

        //Ingreso total 

        public decimal IngTotal { get; set; }

        //Cartera (Aparatos, celular y joyeria)
        public List<CarteraViewModel> CarteraView { get; set; }

        //MES ANTERIOS
        public int Mes { get; set; }
        public int Anio { get; set; }
        public int CarteraTotal { get; set; }

        //Para las metas de los aliados
        public List<MetaOperacionesViewModel> ValorMeta { get; set; }

        //Productos listado de reales 
        
        public List<ProductoViewModel> ListarMetaProducto { get; set; }

        public List<ComentarioViewModel> comentarios { get; set; }


    }
}


public class AliadoCombo
{
    public string Text { get; set; }
    //public string Value { get; set; }
    public string Value { get; set; }
}