﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ENTIDADES.Operaciones
{
    public class PromedioPrestamoViewModel
    {
        public string NomSuc { get; set; }
        public int AnnEmp { get; set; }

        public string NomUser { get; set; }
        public string Status { get; set; }
        public int MesEmp { get; set; }
        public string Mes { get; set; }
        public string Tipo { get; set; }

        public string Producto { get; set; }

        public string Promedio { get; set; }




    }
}
