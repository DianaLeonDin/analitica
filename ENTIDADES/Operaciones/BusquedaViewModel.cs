﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ENTIDADES.Operaciones
{
    public class BusquedaViewModel
    {

        public List<MatrizGestionViewModel> BusquedasAliado { get; set; }
        public List<MatrizGestionViewModel> BusquedaTipoProducto { get; set; }
        public List<MatrizGestionViewModel> BusquedasFamilia { get; set; }

    }
}
