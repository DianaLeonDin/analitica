//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ENTIDADES.RH
{
    using System;
    using System.Collections.Generic;
    
    public partial class VsdTipoDocumento
    {
        public int TipoDocumentoID { get; set; }
        public string NombretipoDoc { get; set; }
        public int EstatusID { get; set; }
        public int OperAlta { get; set; }
        public System.DateTime FechaAlta { get; set; }
        public int OperUltMod { get; set; }
        public System.DateTime FechaUltMod { get; set; }
    }
}
