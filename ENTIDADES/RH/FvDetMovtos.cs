//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ENTIDADES.RH
{
    using System;
    using System.Collections.Generic;
    
    public partial class FvDetMovtos
    {
        public int IdMovto { get; set; }
        public int Ann { get; set; }
        public int Mes { get; set; }
        public double SaldoAnt { get; set; }
        public double AplicarFV { get; set; }
        public double SaldoAct { get; set; }
    }
}
